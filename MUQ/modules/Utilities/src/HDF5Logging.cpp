#include "MUQ/Utilities/HDF5Logging.h"

#include <assert.h>

#include <boost/filesystem/path.hpp>

#include <MUQ/Utilities/LogConfig.h>
#include <MUQ/Utilities/HDF5Wrapper.h>

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;

int  HDF5Logging:: logWriteFrequency = 100;
std::map<std::string,
         std::tuple<std::shared_ptr<Eigen::MatrixXd>, std::shared_ptr<int>,
                    std::shared_ptr<int> > > HDF5Logging:: dataBuffer;

void HDF5LogEntry::MergeWithPrefix(string const prefix, std::shared_ptr<HDF5LogEntry> dataToPrefix)
{
  for (auto& anEntry : dataToPrefix->loggedData) {
    this->loggedData[prefix + string("/") + anEntry.first] = anEntry.second;
  }
}

void HDF5Logging::Configure(boost::property_tree::ptree const& properties)
{
  assert(dataBuffer.empty() && "Can't resize existing buffers.");

  logWriteFrequency = properties.get<int>("HDF5.LogWriteFrequency", 1);
  LOG(INFO) << "HDF5Logging every " << logWriteFrequency << " entries.";
}

#if MUQ_PYTHON == 1
void HDF5Logging::PyConfigure(boost::python::dict const& properties)
{
  Configure(PythonDictToPtree(properties));
}

#endif // if MUQ_PYTHON == 1

void HDF5Logging::WriteEntry(std::string                   pathPrefix,
                             std::shared_ptr<HDF5LogEntry> entry,
                             int                           entryNumber,
                             int                           totalEntries)
{
  assert(totalEntries % logWriteFrequency == 0);

  //loop over all the entries to write them
  for (auto& anEntry : entry->loggedData) {
    boost::filesystem::path pathAddition(pathPrefix);
    pathAddition /= anEntry.first;
    string fullPath = pathAddition.string(); //concatenate the path, but add a / if necessary

    //first, figure out if the entry is new by checking if we have a cache for it
    if (dataBuffer.find(fullPath) == dataBuffer.end()) {
      LOG(INFO) << "Creating log cache for " << fullPath << " size " << anEntry.second.rows() << ", caching " <<
      logWriteFrequency << " of " << totalEntries;
      //if not, we have to create it

      Eigen::MatrixXd mat =  MatrixXd::Constant(
        anEntry.second.rows(), totalEntries, std::numeric_limits<double>::quiet_NaN());
      HDF5Wrapper::WriteMatrix(fullPath, mat);

      auto zeroMat =
        std::make_shared<MatrixXd>(Eigen::MatrixXd::Constant(anEntry.second.rows(), logWriteFrequency,
                                                             std::numeric_limits<double>::quiet_NaN()));
      //zeroMat->setZero();
      auto startIndex      = make_shared<int>(entryNumber - entryNumber % logWriteFrequency);
      auto totalEntriesPtr = make_shared<int>(totalEntries);
      dataBuffer[fullPath] = std::tuple<std::shared_ptr<Eigen::MatrixXd>, std::shared_ptr<int>, std::shared_ptr<int> >(
        zeroMat,
        startIndex,
        totalEntriesPtr);
    }

    //everything exists, grab the current cache.
    std::shared_ptr<Eigen::MatrixXd> entryCache;
    std::shared_ptr<int> cacheStart;
    std::shared_ptr<int> cacheTotalEntries;
    std::tie(entryCache, cacheStart, cacheTotalEntries) = dataBuffer[fullPath];

    assert(entryCache->rows() == anEntry.second.rows()); // check that its size matches the cache
    assert(*cacheTotalEntries == totalEntries);

    //First, if this entry is before where the cache starts, just write it directly
    if (entryNumber < *cacheStart) {
      HDF5Wrapper::WritePartialMatrix(fullPath, anEntry.second, 0, entryNumber);
    } else if (entryNumber < *cacheStart + logWriteFrequency) { //if it's in the
                                                                // cache, add it
      entryCache->col(entryNumber - *cacheStart) = anEntry.second;
    } else {                                                    //else it's after, so
                                                                // write out cache and
                                                                // shift it
      HDF5Wrapper::WritePartialMatrix(fullPath, *entryCache, 0, *cacheStart);
      *entryCache = Eigen::MatrixXd::Constant(entryCache->rows(),
                                              entryCache->cols(),
                                              std::numeric_limits<double>::quiet_NaN()); //reset
                                                                                         // to
                                                                                         // NaN
      *cacheStart = entryNumber - entryNumber % logWriteFrequency;                       //move
                                                                                         // cache
      entryCache->col(entryNumber - *cacheStart) = anEntry.second;                       //add
                                                                                         // this
                                                                                         // entry
    }

    //now that we've written into the cache, consider moving it if it's now full
    if (*cacheStart + logWriteFrequency - entryNumber == 1) { //cache is full, so write it out
      HDF5Wrapper::WritePartialMatrix(fullPath, *entryCache, 0, *cacheStart);
      entryCache->setZero();
      *cacheStart = *cacheStart + logWriteFrequency;
    }
  }
}

void HDF5Logging::FlushBuffer()
{
  for (auto aBuffer : dataBuffer) {
    std::shared_ptr<Eigen::MatrixXd> entryCache;
    std::shared_ptr<int> cacheStart;
    std::shared_ptr<int> cacheTotalEntries;
    std::tie(entryCache, cacheStart, cacheTotalEntries) = aBuffer.second;
    if (*cacheStart < *cacheTotalEntries) { //write if the cache is within the allowable size limit
      HDF5Wrapper::WritePartialMatrix(aBuffer.first, *entryCache, 0, *cacheStart);
    }
  }
}

void HDF5Logging::WipeBuffers()
{
  FlushBuffer();
  dataBuffer.clear();
}


