
#include "MUQ/Utilities/Hmatrix/ElementNode.h"

using namespace muq::Utilities;


template<unsigned int dim>
void ElementNode<dim>::SetConvexHull(const Eigen::Matrix<double, dim, Eigen::Dynamic>& ElePos,
                                     const std::vector<unsigned int>& Elements)
{
  if ((child1.get() != nullptr) && (child2.get() != nullptr)) {
    // build the child convex hulls
    child1->SetConvexHull(ElePos, Elements);
    child2->SetConvexHull(ElePos, Elements);

    // combine the child convex hulls
    CHull = child1->CHull + child2->CHull;
  } else {
    // we need to build the convex hull from scratch

    //first, collect all the points into a vector

    std::vector < Eigen::Matrix < double, dim, 1 >> allpts(EndInd - StartInd);
    for (unsigned int i = 0; i < EndInd - StartInd; ++i) {
      allpts[i] = ElePos.col(Elements[i + StartInd]);
    }

    // build the convex hull from the collected points
    CHull.BuildHull(allpts);
  }
}

namespace muq {
namespace Utilities {
template class ElementNode<1>;
template class ElementNode<2>;
template class ElementNode<3>;
}
}

