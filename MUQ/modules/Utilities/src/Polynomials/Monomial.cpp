#include "MUQ/Utilities/Polynomials/Monomial.h"

#include <boost/serialization/access.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>

using namespace muq::Utilities;

double Monomial::alpha(double x, unsigned int k)
{
  return -x;
}

double Monomial::beta(double x, unsigned int k)
{
  return 0.0;
}

double Monomial::phi0(double x)
{
  return 1.0;
}

double Monomial::phi1(double x)
{
  return x;
}

double Monomial::gradient_static(unsigned int order, double x)
{
  //this definition provided by http://functions.wolfram.com/Polynomials/HermiteH/20/01/01/
  if (order == 0) {
    //order 0 is a constant
    return 0;
  } else {
    return static_cast<double>(order) * evaluate_static(order - 1, x);
  }
}

double Monomial::secondDerivative_static(unsigned int order, double x){
  if(order<2){
    return 0;
  }else{
    return static_cast<double>(order) * gradient_static(order-1,x);
  }
}

Eigen::VectorXd Monomial::GetMonomialCoeffs(const int order){
  
  Eigen::VectorXd monoCoeffs = Eigen::VectorXd::Zero(order+1);
  monoCoeffs(order) = 1.0;
  return monoCoeffs;
}

template<class Archive>
void Monomial::serialize(Archive& ar, const unsigned int version)
{
  //nothing to serialize except for the parent and the type of the object
  ar& boost::serialization::base_object<Static1DPolynomial<Monomial>>(*this);
}


BOOST_CLASS_EXPORT_IMPLEMENT(muq::Utilities::Static1DPolynomial<Monomial>)
BOOST_CLASS_EXPORT_IMPLEMENT(muq::Utilities::Monomial)

template void Monomial::serialize(boost::archive::text_oarchive& ar, unsigned int version);
template void Monomial::serialize(boost::archive::text_iarchive& ar, const unsigned int version);

template void Static1DPolynomial<Monomial>::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void Static1DPolynomial<Monomial>::serialize(boost::archive::text_iarchive& ar, const unsigned int version);

