
#include "MUQ/Utilities/PointWrapper.h"

#include <iostream>

#include <boost/serialization/export.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>


#include "MUQ/Utilities/EigenUtils.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;

PointWrapper::PointWrapper() {}

PointWrapper::PointWrapper(VectorXd const& point)
{
  //copy it locally
  this->point = point;
}

PointWrapper::~PointWrapper() {}

bool PointWrapper::operator==(const PointWrapper& b) const
{
  return MatrixEqual(this->point, b.point);
}

bool PointWrapper::operator<(const PointWrapper& b) const
{
  assert(this->point.rows() == b.point.rows());

  //make sure they aren't equal first, sort of inefficient to handle this way, but simple
  if (*this == b) {
    return false;
  }

  //then check the coordinates in order for something that's <
  for (unsigned int i = 0; i < this->point.rows(); i++) { //check each in order
    if (this->point(i) < b.point(i)) {                    //if definitely less, return true
      return true;
    } else if (this->point(i) > b.point(i)) {             //if definitely greater, return true
      return false;
    }

    //keep looking only if they're "equal"
  }

  return false;
}

template<class Archive>
void PointWrapper::serialize(Archive& ar, const unsigned int version)
{
  ar& point;
}

template void PointWrapper::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void PointWrapper::serialize(boost::archive::text_iarchive& ar, const unsigned int version);


template<class Archive>
void PointComp::serialize(Archive& ar, const unsigned int version) {}

//BOOST_CLASS_EXPORT(muq::Utilities::PointComp)

template void PointComp::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void PointComp::serialize(boost::archive::text_iarchive& ar, const unsigned int version);

