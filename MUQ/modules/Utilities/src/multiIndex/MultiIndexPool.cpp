#include "MUQ/Utilities/multiIndex/MultiIndexPool.h"

using namespace std;
using namespace muq::Utilities;


unsigned int MultiIndexPool::AddMulti(std::shared_ptr<MultiIndex> newMulti)
{
  // first, check if the multiindex already exists
  auto iter = multi2all.find(newMulti);
  if(iter==multi2all.end()){
    allMultis.push_back(newMulti);
    all2active.push_back(std::vector<std::pair<unsigned int, unsigned int>>());
    
    int newInd = allMultis.size()-1;
    multi2all[newMulti] = newInd;
    
    return newInd;
  }else{
    return iter->second;
  }
}

void MultiIndexPool::SetActive(int globalInd, int setId, int activeInd)
{
  std::vector<std::pair<unsigned int, unsigned int>>& pairs = all2active.at(globalInd);
  for(auto pair : pairs){
    if(pair.first==setId)
      return;
  }
  
  pairs.push_back(make_pair(setId,activeInd));
}




