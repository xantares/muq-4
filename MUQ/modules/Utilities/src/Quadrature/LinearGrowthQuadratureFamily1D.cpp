#include "MUQ/Utilities/Quadrature/LinearGrowthQuadratureFamily1D.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

using namespace Eigen;
using namespace muq::Utilities;

LinearGrowthQuadratureFamily1D::LinearGrowthQuadratureFamily1D() {}

LinearGrowthQuadratureFamily1D::LinearGrowthQuadratureFamily1D(QuadratureFamily1D::Ptr inputRule,
                                                               unsigned int            inputGrowthInterval) :
  growthInterval(inputGrowthInterval), baseQuadrature1DRule(inputRule)
{
  //use an initialization list
}

LinearGrowthQuadratureFamily1D::~LinearGrowthQuadratureFamily1D()
{
  // TODO Auto-generated destructor stub
}

unsigned int LinearGrowthQuadratureFamily1D::GetPrecisePolyOrder(unsigned int const order) const
{
  assert(order > 0);

  //simply call the base with the new order
  return baseQuadrature1DRule->GetPrecisePolyOrder(1 + (order - 1) * growthInterval);
}

void LinearGrowthQuadratureFamily1D::ComputeNodesAndWeights(unsigned int const            order,
                                                            std::shared_ptr<RowVectorXd>& nodes,
                                                            std::shared_ptr<RowVectorXd>& weights) const
{
  assert(order > 0);

  //simply call the base with the new order
  nodes   = baseQuadrature1DRule->GetNodes(1 + (order - 1) * growthInterval);
  weights = baseQuadrature1DRule->GetWeights(1 + (order - 1) * growthInterval);
}

template<class Archive>
void LinearGrowthQuadratureFamily1D::serialize(Archive& ar, const unsigned int version)
{
  ar& boost::serialization::base_object<QuadratureFamily1D>(*this);
  ar& baseQuadrature1DRule;
  ar& growthInterval;
}

BOOST_CLASS_EXPORT(LinearGrowthQuadratureFamily1D)
