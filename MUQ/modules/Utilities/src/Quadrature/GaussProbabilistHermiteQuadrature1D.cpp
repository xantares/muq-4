#include "MUQ/Utilities/Quadrature/GaussProbabilistHermiteQuadrature1D.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/shared_ptr.hpp>

#include <boost/math/constants/constants.hpp>

using namespace Eigen;
using namespace muq::Utilities;

///Implements the monic coefficients;
RowVectorXd GaussProbabilistHermiteQuadrature1D::GetMonicCoeff(unsigned int const j) const
{
  assert(j < 65 && "Hermite are difficult to compute with and the quadrature is known to fall apart somewhere around here. May be unstable beyond this");

  RowVectorXd monicCoeff(2);

  monicCoeff << 0, j;
  return monicCoeff;
}

double GaussProbabilistHermiteQuadrature1D::IntegralOfWeight() const
{
  return boost::math::constants::root_two_pi<double>();
}

template<class Archive>
void GaussProbabilistHermiteQuadrature1D::serialize(Archive& ar, const unsigned int version)
{
  ar& boost::serialization::base_object<QuadratureFamily1D>(*this);
}

BOOST_CLASS_EXPORT(muq::Utilities::GaussProbabilistHermiteQuadrature1D) template void GaussProbabilistHermiteQuadrature1D::serialize(
  boost::archive::text_oarchive& ar,
  const unsigned int             version);
template void                                                              GaussProbabilistHermiteQuadrature1D::serialize(
  boost::archive::text_iarchive& ar,
  const unsigned int             version);
