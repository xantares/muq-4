#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>

using namespace Eigen;
using namespace muq::Utilities;

QuadratureFamily1D::QuadratureFamily1D() {}

QuadratureFamily1D::~QuadratureFamily1D() {}

std::shared_ptr<RowVectorXd> QuadratureFamily1D::GetNodes(unsigned int const order)
{
  //if it's not in the cache, grow the cache
  if ((this->nodes.size() < order + 1) || !(this->nodes[order]) || (this->nodes[order]->cols() == 0)) {
    AddOrderToCache(order);
  }

  //then return from the cache
  return this->nodes[order];
}

std::shared_ptr<RowVectorXd> QuadratureFamily1D::GetWeights(unsigned int const order)
{
  //if it's not in the cache, grow the cache
  if ((this->weights.size() < order + 1) || !(this->weights[order]) || (this->weights[order]->cols() == 0)) {
    AddOrderToCache(order);
  }


  return this->weights[order];
}

void QuadratureFamily1D::AddOrderToCache(unsigned int const order)
{
  //allocate space for the new nodes and weights
  std::shared_ptr<RowVectorXd> nodes(new RowVectorXd());
  std::shared_ptr<RowVectorXd> weights(new RowVectorXd());

  //Use the virtual method to compute the nodes and weights
  ComputeNodesAndWeights(order, nodes, weights);

  //If needed, resize the stl::vector and put these nodes in.
  if (this->nodes.size() < order + 1) {
    this->nodes.resize(order + 1);
  }

  this->nodes[order] = nodes;

  //If needed, resize the stl::vector and put these nodes in.
  {
    this->weights.resize(order + 1);
  }

  this->weights[order] = weights;
}

template<class Archive>
void QuadratureFamily1D::serialize(Archive& ar, const unsigned int version)
{
  //don't save the caches
}

BOOST_CLASS_EXPORT_IMPLEMENT(QuadratureFamily1D) template void QuadratureFamily1D::serialize(
  boost::archive::text_oarchive& ar,
  const unsigned int             version);
template void                                                  QuadratureFamily1D::serialize(
  boost::archive::text_iarchive& ar,
  const unsigned int             version);
