
#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/LogConfig.h"

#include <assert.h>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#if MUQ_MPI
# include <mpi.h>
# include <boost/mpi.hpp>
namespace mpi = boost::mpi;
#endif // if MUQ_MPI

using namespace std;
using namespace muq::Utilities;

hid_t  HDF5Wrapper::globalFile       = -1;
string HDF5Wrapper::filename         = "";
bool   HDF5Wrapper::useExternalLinks = false;

#ifdef MUQ_MPI
string hdf5RootName = "";

///A helper function, since we have to open hdf5 files in several places, which is tricky with mpi on
hid_t OpenWithMPI(std::string const& filename)
{
  boost::filesystem::path filepath(filename);
  hid_t file_id;
  MPI_Comm comm     = MPI_COMM_SELF;
  MPI_Info info     = MPI_INFO_NULL;
  hid_t    plist_id = H5Pcreate(H5P_FILE_ACCESS);

  H5Pset_fapl_mpio(plist_id, comm, info);
  if (boost::filesystem::exists(filepath)) {
    file_id = H5Fopen(filename.c_str(), H5F_ACC_RDWR, plist_id);
  } else {
    file_id = H5Fcreate(filename.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, plist_id);
  }

  H5Pclose(plist_id);
  return file_id;
}

#endif // ifdef MUQ_MPI


void HDF5Wrapper::OpenFile(std::string const& filename, bool useExternalLinks)
{
  LOG(INFO) << "Open HDF5 file root name " << filename;
  HDF5Wrapper::filename         = filename;
  HDF5Wrapper::useExternalLinks = useExternalLinks;

  string rankFilename = filename;

  #if MUQ_MPI
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);
  worldComm->barrier();
  hdf5RootName = filename;
  rankFilename = HDF5Wrapper::filename + "_rank_" + boost::lexical_cast<string>(worldComm->rank());


  //use helper function
  globalFile = OpenWithMPI(rankFilename);


#else // if MUQ_MPI

  boost::filesystem::path filepath(rankFilename);

  if (boost::filesystem::exists(filepath)) {
    globalFile = H5Fopen(rankFilename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
  } else {
    globalFile = H5Fcreate(rankFilename.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT);
  }
#endif // if MUQ_MPI

  assert(globalFile > 0);
}

void HDF5Wrapper::OpenFile(boost::property_tree::ptree const& properties)
{
  string filename      = properties.get<string>("HDF5.GlobalFilename");
  bool   externalLinks = properties.get("HDF5.UseExternalLinks", false);

  OpenFile(filename, externalLinks);
}

void HDF5Wrapper::FlushFile()
{
  if (globalFile > 0) { //only flush if safe to do so
    H5Fflush(globalFile, H5F_SCOPE_GLOBAL);
  }
}

herr_t CopyObjectToGlobalFile(hid_t o_id, const char *name, const H5O_info_t *info, void *op_data)
{
  string nameBuffer(name);
  string fullGroupName = "/" + nameBuffer;

  if (info->type == H5O_TYPE_DATASET) {
    if (!HDF5Wrapper::DoesDataSetExist(fullGroupName)) {
      H5Ocopy(o_id, name, HDF5Wrapper::globalFile, fullGroupName.c_str(), H5P_DEFAULT, H5P_DEFAULT);
    }
  } else if (info->type == H5O_TYPE_GROUP) {
    if (!HDF5Wrapper::DoesGroupExist(fullGroupName)) {
      H5Ocopy(o_id, name, HDF5Wrapper::globalFile, fullGroupName.c_str(), H5P_DEFAULT, H5P_DEFAULT);
    }
  }

  return 0;
}

struct LinkData {
  std::string targetFile;
};

herr_t LinkDatasetsToGlobalFile(hid_t o_id, const char *name, const H5O_info_t *info, void *op_data)
{
  string nameBuffer(name);
  string fullGroupName = "/" + nameBuffer;

  LinkData *linkData = static_cast<LinkData *>(op_data);

  if (info->type == H5O_TYPE_GROUP) {
    if (!HDF5Wrapper::DoesGroupExist(fullGroupName)) {
      H5Lcreate_external(linkData->targetFile.c_str(),
                         fullGroupName.c_str(), HDF5Wrapper::globalFile, fullGroupName.c_str(), H5P_DEFAULT,
                         H5P_DEFAULT);
    }
  }

  return 0;
}

void HDF5Wrapper::MergeFile(const std::string& otherFile)
{
  hid_t plist_id = H5Pcreate(H5P_FILE_ACCESS);

#ifdef MUQ_MPI
  MPI_Comm comm = MPI_COMM_SELF;
  MPI_Info info = MPI_INFO_NULL;
  H5Pset_fapl_mpio(plist_id, comm, info);
#endif // ifdef MUQ_MPI

  //open the other file with read only permissions
  hid_t partFile = H5Fopen(otherFile.c_str(), H5F_ACC_RDONLY, plist_id);

  // open the root group in the other file
  string rootGroupName = "/";
  hid_t  rootGroup     = H5Gopen2(partFile, rootGroupName.c_str(), H5P_DEFAULT);

  if (useExternalLinks) {
    // get the absolute path to the other file
    boost::filesystem::path partFilePath(otherFile);
    boost::filesystem::path absPath = boost::filesystem::absolute(partFilePath);

    std::shared_ptr<LinkData> linkData = std::make_shared<LinkData>();
    linkData->targetFile = absPath.string();

    assert(H5Ovisit(rootGroup, H5_INDEX_NAME, H5_ITER_NATIVE, &LinkDatasetsToGlobalFile,
                    static_cast<void *>(linkData.get())) >= 0);
  } else {
    assert(H5Ovisit(rootGroup, H5_INDEX_NAME, H5_ITER_NATIVE, &CopyObjectToGlobalFile, NULL) >= 0);
  }

  // close the group in the other file
  H5Gclose(rootGroup);

  // close the other file
  H5Fclose(partFile);

  // close the property list
  H5Pclose(plist_id);
}

#ifdef MUQ_MPI
void HDF5Wrapper::CloseAndMerge()
{
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);
  worldComm->barrier();

  if (globalFile > 0) {
    H5Fclose(globalFile);
    globalFile = 0; //mark as invalid
  }

  worldComm->barrier();

  if (worldComm->rank() == 0) {
    //open the global file
    globalFile = OpenWithMPI(filename);
    // loop over all the files from the other ranks
    for (int i = 0; i < worldComm->size(); ++i) {
      // merge the other file
      std::string rankFilename = HDF5Wrapper::filename + "_rank_" + boost::lexical_cast<string>(i);
      MergeFile(rankFilename);

      // if we actually copied the data, instead of linking to it, delete the rank file
      if (!useExternalLinks) {
        boost::filesystem::remove(rankFilename);
      }
    }

    //close global file
    H5Fclose(globalFile);
    globalFile = 0; //mark as invalid
  }

  worldComm->barrier();
  LOG(INFO) << "HDF5 files merged.";
}

#endif // ifdef MUQ_MPI

void HDF5Wrapper::CloseFile()
{
#ifdef MUQ_MPI
  CloseAndMerge();
#else // ifdef MUQ_MPI
  if (globalFile > 0) {
    H5Fclose(globalFile);
    globalFile = 0; //mark as invalid
  }
#endif // ifdef MUQ_MPI
  LOG(INFO) << "HDF5 files closed.";
}

bool HDF5Wrapper::DoesGroupExist(std::string const groupName)
{
  if ((groupName.compare("/") == 0) || (groupName.compare("") == 0) || (groupName.compare("/.") == 0)) {
    return true;
  }
  boost::filesystem::path groupPath(groupName);
  boost::filesystem::path parentPath = groupPath.parent_path();

  return DoesGroupExist(parentPath.string()) && (H5Lexists(globalFile, groupName.c_str(), H5P_DEFAULT) > 0);
}

bool HDF5Wrapper::DoesDataSetExist(std::string const datasetName)
{
  boost::filesystem::path datasetPath(datasetName);
  boost::filesystem::path parentPath = datasetPath.parent_path();

  return DoesGroupExist(parentPath.string()) && (H5Lexists(globalFile, datasetName.c_str(), H5P_DEFAULT) > 0);
}

void HDF5Wrapper::CreateGroup(std::string const groupName)
{
  if ((groupName.compare("/") == 0) || (groupName.compare("") == 0)) {
    return;
  }

  boost::filesystem::path groupPath(groupName);
  boost::filesystem::path parentPath = groupPath.parent_path();
  CreateGroup(parentPath.string()); //make sure parent exists

  if (!DoesGroupExist(groupName)) { //now the parents are all there, so create as needed
    hid_t newgroup = H5Gcreate2(globalFile, groupName.c_str(), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Gclose(newgroup);
  }
}

void HDF5Wrapper::WriteStringAttribute(std::string const& datasetName,
                                       std::string const& attributeName,
                                       std::string const& attribute)
{
  if (DoesDataSetExist(datasetName)) {
    H5LTset_attribute_string(globalFile, datasetName.c_str(), attributeName.c_str(), attribute.c_str());
    FlushFile();
  } else {
    LOG(INFO) << "Dataset " << datasetName << " does not exist for attribute writing " << attributeName << " = " <<
      attribute;
  }
}

void HDF5Wrapper::WritePartialMatrix(std::string const& datasetName, Eigen::MatrixXd const& matrix, int row, int col)
{
  assert(DoesDataSetExist(datasetName));

  hid_t dataset = H5Dopen2(globalFile, datasetName.c_str(), H5P_DEFAULT);

  hid_t fullspace = H5Dget_space(dataset);

  hsize_t start[2];


  hsize_t stride[2];


  hsize_t count[2];
  hsize_t block[2];

  start[0]  = row;
  start[1]  = col;
  stride[0] = 1;
  stride[1] = 1;
  count[0]  = 1;
  count[1]  = 1;
  block[0]  = matrix.rows();
  block[1]  = matrix.cols();

  H5Sselect_hyperslab(fullspace, H5S_SELECT_SET, start, stride, count, block);

  Eigen::MatrixXd transposedData = matrix.transpose();
  hsize_t limitedBlock           = H5Screate_simple(2, block, block);
  H5Dwrite(dataset, H5Dget_type(dataset), limitedBlock, fullspace, H5P_DEFAULT, transposedData.data());
  H5Sclose(limitedBlock);
  H5Sclose(fullspace);
  H5Dclose(dataset);

  WriteScalarAttribute(datasetName, "LargestRowWritten", static_cast<int>(row + matrix.rows() - 1));
  WriteScalarAttribute(datasetName, "LargestColWritten", static_cast<int>(col + matrix.cols() - 1));
}


Eigen::VectorXi HDF5Wrapper::GetDatasetSize(std::string const datasetName){
	if(DoesDataSetExist(datasetName)){
		
	    hid_t dataset = H5Dopen2(globalFile, datasetName.c_str(), H5P_DEFAULT);

        hid_t space_id = H5Dget_space(dataset);
		
		int rank;
		hsize_t  *dims;
	    hsize_t  *max_dims;
        
		rank     = H5Sget_simple_extent_ndims(space_id); 
		dims     = (hsize_t*)malloc(rank*sizeof(hsize_t));
		max_dims = (hsize_t*)malloc(rank*sizeof(hsize_t));
		H5Sget_simple_extent_dims(space_id, dims, max_dims);
				  
        H5Sclose(space_id);
		H5Dclose(dataset);
		
		Eigen::VectorXi output(rank);
		for(int i=0; i<rank; ++i)
		  output(i) = dims[i];
		
		free(dims);
		free(max_dims);
		
		return output;
	}else{
		return Eigen::VectorXi();	
	}
}
