
#include <gtest/gtest.h>

#include "MUQ/Utilities/LinearOperators/Operators/EigenLinOp.h"
#include "MUQ/Utilities/LinearOperators/Operators/GeneralSymmetricLinOp.h"
#include "MUQ/Utilities/LinearOperators/Operators/EigenLinOp.h"
#include "MUQ/Utilities/LinearOperators/Solvers/CG.h"
#include "MUQ/Utilities/LinearOperators/Solvers/BiCG.h"
#include "MUQ/Utilities/LinearOperators/Solvers/MinRes.h"

using namespace muq::Utilities;
using namespace std;


TEST(GeneralUtiltiesLinOp, EigenGeneralSolve_BiCG){
	
    Eigen::MatrixXd temp = Eigen::MatrixXd::Ones(10, 10) + 2 * Eigen::MatrixXd::Identity(10, 10);
    Eigen::MatrixXd A    = temp * temp.transpose();
    Eigen::VectorXd rhs  = Eigen::VectorXd::Ones(10);

    // create the linear operator
    auto Aop = make_shared <EigenLinOp>(A);

    // create the solver and solve the system
    BiCG solver;

    solver.compute(Aop);
    Eigen::VectorXd itSolution = solver.solve(rhs);

    // compute the direction solution using eigen
    Eigen::VectorXd trueSolution = A.lu().solve(rhs);

    // compare the direct and iterative solutions
    for (int i = 0; i < 10; ++i) {
      EXPECT_NEAR(trueSolution[i], itSolution[i], 1e-7);
    }
	
	
}

TEST(GeneralUtiltiesLinOp, EigenSymmetricSolve_MinRes)
{
  Eigen::MatrixXd temp = Eigen::MatrixXd::Ones(10, 10) + 2 * Eigen::MatrixXd::Identity(10, 10);
  Eigen::MatrixXd A    = temp * temp.transpose();
  Eigen::VectorXd rhs  = Eigen::VectorXd::Ones(10);

  // create the linear operator
  auto Aop = make_shared < GeneralSymmetricLinOp < Eigen::SelfAdjointView<Eigen::MatrixXd, Eigen::Lower >>>(
    A.selfadjointView<Eigen::Lower>(),
    A.rows());

  // create the solver and solve the system
  MinRes solver;

  solver.compute(Aop);
  Eigen::VectorXd itSolution = solver.solve(rhs);

  // compute the direction solution using eigen
  Eigen::VectorXd trueSolution = A.ldlt().solve(rhs);

  // compare the direct and iterative solutions
  for (int i = 0; i < 10; ++i) {
    EXPECT_NEAR(trueSolution[i], itSolution[i], 1e-7);
  }
}


TEST(GeneralUtiltiesLinOp, EigenSymmetricSolve_CG)
{
  Eigen::MatrixXd temp = Eigen::MatrixXd::Ones(10, 10) + 2 * Eigen::MatrixXd::Identity(10, 10);
  Eigen::MatrixXd A    = temp * temp.transpose();
  Eigen::VectorXd rhs  = Eigen::VectorXd::Ones(10);

  // create the linear operator
  auto Aop = make_shared < GeneralSymmetricLinOp < Eigen::SelfAdjointView<Eigen::MatrixXd, Eigen::Lower >>>(
    A.selfadjointView<Eigen::Lower>(),
    A.rows());

  // create the solver and solve the system
  CG solver;

  solver.compute(Aop);
  Eigen::VectorXd itSolution = solver.solve(rhs);

  // compute the direction solution using eigen
  Eigen::VectorXd trueSolution = A.ldlt().solve(rhs);

  // compare the direct and iterative solutions
  for (int i = 0; i < 10; ++i) {
    EXPECT_NEAR(trueSolution[i], itSolution[i], 1e-7);
  }
}
