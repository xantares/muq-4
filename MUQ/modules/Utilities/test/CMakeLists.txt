
##########################################
#  Objects to build


# Add in the google test sources for Utilities, pass them up
set(Test_GeneralUtility_Sources
  
    Utilities/test/TranslaterTest.cpp
    #Utilities/test/FileWrapperTest.cpp
    Utilities/test/EigenUtilsTest.cpp
    Utilities/test/MultiIndexSetTest.cpp
    Utilities/test/MultiIndexTest.cpp
    Utilities/test/RngTest.cpp
    Utilities/test/MatApproxTest.cpp
    #Utilities/test/MeshTest.cpp
    Utilities/test/JobManagerTest.cpp
    
    Utilities/test/TreeTests.cpp
    Utilities/test/LinOpTests.cpp

    Utilities/test/HDF5WrapperTest.cpp
    
    Utilities/test/SignalProcessingTests.cpp

    Utilities/test/HermitePolynomials1DTest.cpp
    Utilities/test/LegendrePolynomials1DTest.cpp
    Utilities/test/MonomialTest.cpp

    Utilities/test/QuadratureTestFunctions.cpp
    Utilities/test/GaussianQuadratureFamily1DTest.cpp
    Utilities/test/FullTensorQuadratureTest.cpp
    Utilities/test/GaussChebyshevQuadrature1DTest.cpp
    Utilities/test/ClenshawCurtisQuadratureTest.cpp
    
    PARENT_SCOPE
)




