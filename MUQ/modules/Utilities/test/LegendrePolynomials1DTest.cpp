#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"

#include "gtest/gtest.h"

using namespace muq::Utilities;

TEST(UtilitiesLegendrePolyTest, spotChecks1)
{
  LegendrePolynomials1DRecursive legendrePolyR;

  //A sample of points tested against mathematica
  EXPECT_NEAR(0.3375579333496094,   legendrePolyR.evaluate(5, 0.325),    1e-14);
  EXPECT_NEAR(-0.05346106275520913, legendrePolyR.evaluate(20, -.845),   1e-14);
  EXPECT_NEAR(-0.1119514835092105,  legendrePolyR.evaluate(50, .1264),   1e-14);
  EXPECT_NEAR(-.001892916076323403, legendrePolyR.evaluate(200, -.3598), 1e-14);
  EXPECT_NEAR(0.01954143166718206,  legendrePolyR.evaluate(1000, .4587), 1e-14);

  EXPECT_NEAR(4.97913965847362,     legendrePolyR.gradient(52, .1),      1e-13);
  
  EXPECT_DOUBLE_EQ(1.0, legendrePolyR.gradient(1, 0.5));
  EXPECT_DOUBLE_EQ(1.0, legendrePolyR.gradient(1, 0.75));
  EXPECT_DOUBLE_EQ(1.0, legendrePolyR.gradient(1, -0.5));
  
  EXPECT_DOUBLE_EQ(15.0/2.0*0.25-1.5, legendrePolyR.gradient(3, 0.5));
  EXPECT_DOUBLE_EQ(15.0/2.0*0.75*0.75-1.5, legendrePolyR.gradient(3, 0.75));
  EXPECT_DOUBLE_EQ(15.0/2.0*0.25-1.5, legendrePolyR.gradient(3, -0.5));
  
  EXPECT_EQ(0.0, legendrePolyR.gradient(0, -0.5));
  EXPECT_EQ(0.0, legendrePolyR.gradient(0, 0.0));
  EXPECT_EQ(0.0, legendrePolyR.gradient(0, 0.5));
}

TEST(UtilitiesLegendrePolyTest, SturmSolve){
  
  // create a cubic polynomial that caused us issues at one point, this is almost linear
  Eigen::VectorXd poly(5);
  poly << 9.754040499940952e-02,     2.784982188670484e-01,     5.468815192049838e-01,     9.575068354342976e-01,     9.648885351992765e-01;
  
  // compute the roots
  Eigen::VectorXd roots = LegendrePolynomials1DRecursive::Roots(poly, 1e-6);
  
  // compare the roots to the truth
  EXPECT_NEAR(-9.051904659059379e-01,roots(0),1e-4);
  EXPECT_NEAR(-5.226204925205424e-01,roots(1),1e-4);
  EXPECT_NEAR(1.268658154013623e-01,roots(2),1e-4);
  EXPECT_NEAR(7.338881792978924e-01,roots(3),1e-4);
}

TEST(UtilitiesLegendrePolyTest, spotChecks2)
{
  LegendrePolynomials1DRecursive legendrePolyR;
  
  double x = 0.5;
  unsigned maxOrder = 11;
  Eigen::VectorXd trueVals(maxOrder+1);
  for(int p=0; p<=maxOrder; ++p)
    trueVals(p) = legendrePolyR.evaluate(p,x);
  
  Eigen::VectorXd testVals = legendrePolyR.evaluateAll(maxOrder,x);
  
  for(int p=0; p<=maxOrder; ++p)
    EXPECT_DOUBLE_EQ(trueVals(p),testVals(p));
}