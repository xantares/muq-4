
#include <gtest/gtest.h>

#include "MUQ/Utilities/Trees/BinaryTree.h"
#include "MUQ/Utilities/Trees/MeanTerminalNode.h"


using namespace muq::Utilities;
using namespace std;


template<>
std::shared_ptr < DerivedSplitterRegister < >> KdSplitter<>::reg = std::make_shared < DerivedSplitterRegister < >>
                                                                   ("KdSplitter", KdSplitter<>::Create);

template<>
std::shared_ptr < DerivedSplitterRegister < >> CovSplitter<>::reg = std::make_shared < DerivedSplitterRegister < >>
                                                                    ("CovSplitter", CovSplitter<>::Create);

template<>
std::shared_ptr < DerivedStopperRegister < >> PtDepthStopper<>::reg = std::make_shared < DerivedStopperRegister < >>
                                                                      ("PtDepthStopper", PtDepthStopper<>::Create);

template<>
std::shared_ptr < DerivedTerminalRegister < Eigen::VectorXd >> MeanTerminalNode<>::reg = std::make_shared <
                                                                                         DerivedTerminalRegister <
                                                                                         Eigen::VectorXd >>
                                                                                         ("MeanTerminalNode",
MeanTerminalNode<>::Create);


template<>
std::shared_ptr < DerivedSplitterRegister < int >> KdSplitter<int>::reg = std::make_shared < DerivedSplitterRegister <
                                                                          int >>
                                                                          ("KdSplitter", KdSplitter<int>::Create);

template<>
std::shared_ptr < DerivedSplitterRegister < int >> CovSplitter<int>::reg = std::make_shared < DerivedSplitterRegister <
                                                                           int >>
                                                                           ("CovSplitter", CovSplitter<int>::Create);

template<>
std::shared_ptr < DerivedStopperRegister < int >> PtDepthStopper<int>::reg = std::make_shared < DerivedStopperRegister <
                                                                             int >>
                                                                             ("PtDepthStopper",
                                                                              PtDepthStopper<int>::Create);

template<>
std::shared_ptr < DerivedTerminalRegister < Eigen::VectorXd, int >> MeanTerminalNode<int>::reg = std::make_shared <
                                                                                                 DerivedTerminalRegister
                                                                                                 < Eigen::VectorXd,
int >> ("MeanTerminalNode", MeanTerminalNode<int>::Create);


TEST(Utilities_TreeTests, MeanCovTree)
{
  int dim  = 10;
  int npts = 1e4;

  // create a few points in a matrix
  Eigen::MatrixXd pts = Eigen::MatrixXd::Random(dim, npts);

  // create the parameter list
  boost::property_tree::ptree params;

  params.put("Trees.SplitterMethod", "CovSplitter");

  // create the binary tree
  BinaryTree<Eigen::VectorXd> tree(pts, params);

  Eigen::VectorXd newPt = Eigen::VectorXd::Random(dim);
  Eigen::VectorXd mu    = tree.RootNode->Eval(newPt);
}


TEST(Utilities_TreeTests, MeanKdTree)
{
  int dim  = 10;
  int npts = 1e4;

  // create a few points in a matrix
  Eigen::MatrixXd pts = Eigen::MatrixXd::Random(dim, npts);

  // create the parameter list
  boost::property_tree::ptree params;

  params.put("Trees.SplitterMethod", "KdSplitter");

  // create the binary tree
  BinaryTree<Eigen::VectorXd> tree(pts, params);

  Eigen::VectorXd newPt = Eigen::VectorXd::Random(dim);
  Eigen::VectorXd mu    = tree.RootNode->Eval(newPt);
}

TEST(Utilities_TreeTests, ExtraArgTest)
{
  int dim  = 10;
  int npts = 1e4;

  // create a few points in a matrix
  Eigen::MatrixXd pts = Eigen::MatrixXd::Random(dim, npts);

  // create the parameter list
  boost::property_tree::ptree params;

  params.put("Trees.SplitterMethod", "KdSplitter");

  // create the binary tree
  BinaryTree<Eigen::VectorXd, int> tree(pts, params, 1);

  Eigen::VectorXd newPt = Eigen::VectorXd::Random(dim);
  Eigen::VectorXd mu    = tree.RootNode->Eval(newPt);
}
