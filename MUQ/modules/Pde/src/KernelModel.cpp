
#include "MUQ/Pde/KernelModel.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Pde;
using namespace muq::Geostatistics;

KernelModel::KernelModel(vector<Eigen::VectorXd> const& pts, unsigned int numBasis,
                         shared_ptr<CovKernel> const& kernel) :
  LinearModel(kernel->BuildKL(pts, numBasis))
{}

KernelModel::KernelModel(vector<Eigen::VectorXd> const& pts, unsigned int numBasis,
                         Eigen::VectorXd const& mean,
                         shared_ptr<CovKernel> const& kernel) :
  LinearModel(mean, kernel->BuildKL(pts, numBasis))
{}

Eigen::MatrixXd KernelModel::GetCovariance() const
{
  return A * A.transpose();
}

void KernelModel::UpdateMean(Eigen::VectorXd const& mean)
{
  assert(mean.size() == outputSize);
  b = mean;

  bool *ptr;
  ptr  = (bool *)(&isAffine);
  *ptr = false;

  ptr = nullptr;
  delete[] ptr;
}

