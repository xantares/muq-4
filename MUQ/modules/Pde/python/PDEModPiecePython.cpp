#include "MUQ/Pde/python/PDEModPiecePython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Pde;

shared_ptr<PDEModPiece<SolverModPiece> > muq::Pde::NontransientConstructPDE(py::list const& inputSizes, shared_ptr<GenericEquationSystems> const& system, py::dict& param)
{
  const Eigen::VectorXi inSizes = GetEigenVector<Eigen::VectorXi>(inputSizes);

  boost::property_tree::ptree para = PythonDictToPtree(param);

  return ConstructPDE(inSizes, system, para);
}

shared_ptr<PDEModPiece<SolverModPiece> > muq::Pde::NontransientConstructPDEPoints(py::list const& inputSizes, py::list const& pnts, shared_ptr<GenericEquationSystems> const& system, py::dict& param)
{
  const Eigen::VectorXi inSizes = GetEigenVector<Eigen::VectorXi>(inputSizes);

  const vector<Eigen::VectorXd> points = PythonListToVector(pnts);
  boost::property_tree::ptree para          = PythonDictToPtree(param);

  return ConstructPDE(inSizes, points, system, para);
}

shared_ptr<PDEModPiece<OdeModPiece> > muq::Pde::TransientConstructPDE(py::list const& inputSizes, py::list const& obsTimes, shared_ptr<GenericEquationSystems> const& system, py::dict& param)
{
  const Eigen::VectorXi inSizes    = GetEigenVector<Eigen::VectorXi>(inputSizes);
  const Eigen::VectorXd times      = GetEigenVector<Eigen::VectorXd>(obsTimes);
  boost::property_tree::ptree para = PythonDictToPtree(param);

  return ConstructPDE(inSizes, times, system, para);
}

shared_ptr<PDEModPiece<OdeModPiece> > muq::Pde::TransientConstructPDEPoints(py::list const& inputSizes, py::list const& pnts, py::list const& obsTimes, shared_ptr<GenericEquationSystems> const& system, py::dict& param)
{
  const Eigen::VectorXi inSizes = GetEigenVector<Eigen::VectorXi>(inputSizes);

  const vector<Eigen::VectorXd> points = PythonListToVector(pnts);
  const Eigen::VectorXd times          = GetEigenVector<Eigen::VectorXd>(obsTimes);
  boost::property_tree::ptree para     = PythonDictToPtree(param);

  return ConstructPDE(inSizes, points, times, system, para);
}

void muq::Pde::ExportPDE()
{
  py::class_<PDEModPiece<SolverModPiece>,
                        shared_ptr<PDEModPiece<SolverModPiece> >,
                        py::bases<ModPiece>,
                        boost::noncopyable> exportNontransientPDE("NontransientPDEModPiece", py::no_init);

  exportNontransientPDE.def("__init__", py::make_constructor(NontransientConstructPDE));
  exportNontransientPDE.def("__init__", py::make_constructor(NontransientConstructPDEPoints));

  exportNontransientPDE.def("Evaluate", &PDEModPiece<SolverModPiece>::PyEvaluate);
  exportNontransientPDE.def("Gradient", &PDEModPiece<SolverModPiece>::PyGradient);
  exportNontransientPDE.def("Jacobian", &PDEModPiece<SolverModPiece>::PyJacobian);
  exportNontransientPDE.def("JacobianAction", &PDEModPiece<SolverModPiece>::PyJacobianAction);
  exportNontransientPDE.def("Hessian", &PDEModPiece<SolverModPiece>::PyHessian);
  exportNontransientPDE.def("PrintToExodus", &PDEModPiece<SolverModPiece>::PrintToExodusPy);

  py::implicitly_convertible<shared_ptr<PDEModPiece<SolverModPiece> >,
                                        shared_ptr<ModPiece> >();

  py::class_<PDEModPiece<OdeModPiece>,
                        shared_ptr<PDEModPiece<OdeModPiece> >,
                        py::bases<ModPiece>,
                        boost::noncopyable> exportTransientPDE("TransientPDEModPiece", py::no_init);

  exportTransientPDE.def("__init__", py::make_constructor(TransientConstructPDE));
  exportTransientPDE.def("__init__", py::make_constructor(TransientConstructPDEPoints));

  exportTransientPDE.def("Evaluate", &PDEModPiece<OdeModPiece>::PyEvaluate);
  exportTransientPDE.def("Gradient", &PDEModPiece<OdeModPiece>::PyGradient);
  exportTransientPDE.def("Jacobian", &PDEModPiece<OdeModPiece>::PyJacobian);
  exportTransientPDE.def("JacobianAction", &PDEModPiece<OdeModPiece>::PyJacobianAction);
  exportTransientPDE.def("Hessian", &PDEModPiece<OdeModPiece>::PyHessian);
  exportTransientPDE.def("PrintToExodus", &PDEModPiece<OdeModPiece>::PrintToExodusPy);

  py::implicitly_convertible<shared_ptr<PDEModPiece<OdeModPiece> >,
                                        shared_ptr<ModPiece> >();
}
