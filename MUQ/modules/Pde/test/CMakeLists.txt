##########################################
#  Objects to build


# files containing google tests
set(Test_pde_Sources
    Pde/test/MeshParameterTest.cpp

    Pde/test/KernelModelTest.cpp
    Pde/test/MeshKernelModelTest.cpp

    Pde/test/PDETest.cpp

    Pde/test/FieldIntegrator1DTest.cpp

    Pde/test/PointObserverTest.cpp
    Pde/test/SensorTest.cpp

    PARENT_SCOPE
)
