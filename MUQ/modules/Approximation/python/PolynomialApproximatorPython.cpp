#include "MUQ/Approximation/python/PolynomialApproximatorPython.h"
#include <MUQ/Approximation/Incremental/GPApproximator.h>

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

PolynomialApproximator::PolynomialApproximator(std::shared_ptr<muq::Modelling::ModPiece> const& fnIn, boost::python::dict const& para) :
  PolynomialApproximator(fnIn, PythonDictToPtree(para)) {}


double PolynomialApproximator::PyLocalRadius(py::list const& point) const
{
  // convert python list to Eigen::Vector
  Eigen::VectorXd eigenPoint = GetEigenVector<Eigen::VectorXd>(point);
  
  return LocalRadius(eigenPoint);
}

double PolynomialApproximator::PyErrorIndicator(boost::python::list const& point) const
{
  // convert python list to Eigen::Vector
  Eigen::VectorXd eigenPoint = GetEigenVector<Eigen::VectorXd>(point);
  
  return ErrorIndicator(eigenPoint);
}


void muq::Approximation::ExportPolynomialApproximator() 
{
  // export the PolynomialApproximator class
  py::class_<PolynomialApproximator, shared_ptr<PolynomialApproximator>, py::bases<OneInputJacobianModPiece>, boost::noncopyable> 
    exportApprox("PolynomialApproximator", py::init<shared_ptr<ModPiece> const&, py::dict const&>());

  // add to cache 
  exportApprox.def("AddToCache", &Approximator::PyAddToCache);

  // refinement
  exportApprox.def("RefineNear", &Approximator::PyRefineNear);

  // nearest neighbors 
  exportApprox.def("ResetNumNearestNeighbors", &PolynomialApproximator::ResetNumNearestNeighbors);
  exportApprox.def("NumNearestNeighbors", &PolynomialApproximator::NumNearestNeighbors);

  // radius of the local ball
  exportApprox.def("LocalRadius", &PolynomialApproximator::PyLocalRadius);
  
  // compute the error indicator
  exportApprox.def("ErrorIndicator", &PolynomialApproximator::PyErrorIndicator);

  // get the cache points 
  exportApprox.def("GetCachePoints", &Approximator::PyGetCachePoints);

  // get the model being approximated
  exportApprox.def("GetModel", &PolynomialApproximator::GetModel);

  // convert to parents
  py::implicitly_convertible<shared_ptr<PolynomialApproximator>, shared_ptr<OneInputJacobianModPiece> >();
}

GPApproximator::GPApproximator(std::shared_ptr<muq::Modelling::ModPiece> const& fnIn, boost::python::dict const& para) :
  GPApproximator(fnIn, PythonDictToPtree(para)) {}
  
void muq::Approximation::ExportGPApproximator() 
{
  // export the PolynomialApproximator class
  py::class_<GPApproximator, shared_ptr<GPApproximator>, py::bases<OneInputJacobianModPiece>, boost::noncopyable> 
    exportApprox("GPApproximator", py::init<shared_ptr<ModPiece> const&, py::dict const&>());

  // add to cache 
  exportApprox.def("AddToCache", &Approximator::PyAddToCache);

  // refinement
  exportApprox.def("RefineNear", &Approximator::PyRefineNear);

  // nearest neighbors 
  exportApprox.def("ResetNumNearestNeighbors", &GPApproximator::ResetNumNearestNeighbors);
  exportApprox.def("NumNearestNeighbors", &GPApproximator::NumNearestNeighbors);

  // get the cache points 
  exportApprox.def("GetCachePoints", &Approximator::PyGetCachePoints);

  // get the model being approximated
  exportApprox.def("GetModel", &PolynomialApproximator::GetModel);

  // convert to parents
  py::implicitly_convertible<shared_ptr<GPApproximator>, shared_ptr<OneInputJacobianModPiece> >();
}
