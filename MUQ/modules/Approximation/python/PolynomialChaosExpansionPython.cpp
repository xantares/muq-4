#include "MUQ/Approximation/python/PolynomialChaosExpansionPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

boost::python::list PolynomialChaosExpansion::PyComputeVariance() const{
  return GetPythonVector(ComputeVariance());
}

boost::python::list PolynomialChaosExpansion::PyComputeCovariance() const{
  return GetPythonMatrix<double>(ComputeCovariance());
}

boost::python::list PolynomialChaosExpansion::PyComputeMean() const{
  return GetPythonVector(ComputeMean());
}

boost::python::list PolynomialChaosExpansion::PyComputeVarianceJacobian() const{
  return GetPythonMatrix<double>(ComputeVarianceJacobian());
}

boost::python::list PolynomialChaosExpansion::PyComputeVarianceGradient(int outInd) const{
  return GetPythonVector(ComputeVarianceGradient(outInd));
}

boost::python::list PolynomialChaosExpansion::PyComputeVarianceHessian() const{
  return GetPythonMatrix<double>(ComputeVariance());
}

boost::python::list PolynomialChaosExpansion::PyComputeMagnitude() const{
  return GetPythonVector(ComputeMagnitude());
}

boost::python::list PolynomialChaosExpansion::PyComputeSobolTotalSensitivityIndex(unsigned int targetDim) const{
  return GetPythonVector(ComputeSobolTotalSensitivityIndex(targetDim));
}

boost::python::list PolynomialChaosExpansion::PyComputeAllSobolTotalSensitivityIndices() const{
  return GetPythonMatrix<double>(ComputeAllSobolTotalSensitivityIndices());
}

boost::python::list PolynomialChaosExpansion::PyComputeMainSensitivityIndex(unsigned int targetDim) const{
  return GetPythonVector(ComputeMainSensitivityIndex(targetDim));
}

boost::python::list PolynomialChaosExpansion::PyComputeAllMainSensitivityIndices() const{
  return GetPythonMatrix<double>(ComputeAllMainSensitivityIndices());
}

void muq::Approximation::ExportPolynomialChaosExpansion()
{
  py::class_<ExpansionBase, shared_ptr<ExpansionBase>,
  py::bases<muq::Modelling::OneInputJacobianModPiece>, boost::noncopyable> exportBase("ExpansionBase", py::no_init);
  
  py::class_<PolynomialExpansion, shared_ptr<PolynomialExpansion>,
  py::bases<ExpansionBase>, boost::noncopyable> exportPoly("PolynomialExpansion", py::no_init);
  
  py::class_<PolynomialChaosExpansion, shared_ptr<PolynomialChaosExpansion>,
  py::bases<PolynomialExpansion>, boost::noncopyable> exportPce("PolynomialChaosExpansion", py::no_init);
  
  // functions
  exportPce.def("print", &PolynomialChaosExpansion::print);
  exportPce.def("printNormalized", &PolynomialChaosExpansion::printNormalized);
  
  // static functions
  exportPce.def("SaveToFile",&PolynomialChaosExpansion::SaveToFile).staticmethod("SaveToFile");
  exportPce.def("LoadFromFile",&PolynomialChaosExpansion::LoadFromFile).staticmethod("LoadFromFile");
  
  // functions with vectors that need translating
  exportPce.def("ComputeVariance", &PolynomialChaosExpansion::PyComputeVariance);
  exportPce.def("ComputeCovariance", &PolynomialChaosExpansion::PyComputeCovariance);
  exportPce.def("ComputeMean", &PolynomialChaosExpansion::PyComputeMean);
  exportPce.def("ComputeVarianceJacobian", &PolynomialChaosExpansion::PyComputeVarianceJacobian);
  exportPce.def("ComputeVarianceHessian", &PolynomialChaosExpansion::PyComputeVarianceHessian);
  exportPce.def("ComputeMagnitude", &PolynomialChaosExpansion::PyComputeMagnitude);
  exportPce.def("ComputeSobolTotalSensitivityIndex", &PolynomialChaosExpansion::PyComputeSobolTotalSensitivityIndex);
  exportPce.def("ComputeAllSobolTotalSensitivityIndices", &PolynomialChaosExpansion::PyComputeAllSobolTotalSensitivityIndices);
  exportPce.def("ComputeMainSensitivityIndex", &PolynomialChaosExpansion::PyComputeMainSensitivityIndex);
  exportPce.def("ComputeAllMainSensitivityIndices", &PolynomialChaosExpansion::PyComputeAllMainSensitivityIndices);
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<PolynomialChaosExpansion>, shared_ptr<ModPiece> >();
  py::implicitly_convertible<shared_ptr<PolynomialChaosExpansion>, shared_ptr<PolynomialExpansion> >();
  py::implicitly_convertible<shared_ptr<PolynomialChaosExpansion>, shared_ptr<ExpansionBase> >();
  py::implicitly_convertible<shared_ptr<PolynomialChaosExpansion>, shared_ptr<OneInputJacobianModPiece> >();
}
