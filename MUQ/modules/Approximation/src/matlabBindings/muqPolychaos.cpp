
#include "mex.h"
#include "string.h"

#include <exception>
#include <map>

#include <boost/bind.hpp>

#include <Eigen/Core>

#include "MUQ/Utilities/CachedFunctionWrapper.h"
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/PolynomialChaos/utilities/VariableCollection.h"
#include "MUQ/PolynomialChaos/polynomials/LegendrePolynomials1DRecursive.h"
#include "MUQ/PolynomialChaos/polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/PolynomialChaos/quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/PolynomialChaos/quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/PolynomialChaos/quadrature/ExpGrowthQuadratureFamily1D.h"
#include "MUQ/PolynomialChaos/smolyak/SmolyakPCEFactory.h"

using namespace std;
using namespace Eigen;
using namespace muq::PolynomialChaos;
using namespace muq::utilities;

//use a single indexing scheme so that we won't confuse factories for PCEs.
int nextCacheIndex = 1;

//set aside a cache of objects
map<int, SmolyakPCEFactory::Ptr> factoryCache;
map<int, PolynomialChaosExpansion::Ptr> pceCache;
bool googleLogInitialized = false;


//A wrapper for a matlab function into an eigen colvec->colvec.
VectorXd UserMatlabFn(VectorXd const& input, std::shared_ptr<string> fnName, unsigned inputDim, unsigned outputDim)
{
  mxArray *rhs, *lhs;

  //allocate the input data matrix
  rhs = mxCreateNumericMatrix(inputDim, 1, mxDOUBLE_CLASS, mxREAL);
  double *rhsData = mxGetPr(rhs);

  //copy the values over
  for (unsigned int i = 0; i < inputDim; ++i) {
    rhsData[i] = input(i);
  }

  //dereference the input name and call it
  mexCallMATLAB(1, &lhs, 1, &rhs, (*fnName).c_str());

  //now copy out the result
  VectorXd result(outputDim);

  if (!mxIsDouble(lhs) || (mxGetNumberOfElements(lhs) != outputDim)) {
    mexErrMsgTxt("Wrong number of outputs from user function.");
  }

  double *lhsData = mxGetPr(lhs);

  for (unsigned int i = 0; i < outputDim; ++i) {
    result(i) = lhsData[i];
  }

  return result;
}

SmolyakPCEFactory::Ptr ConstructSmolyakPCEFactory(int nrhs, const mxArray *prhs[])
{
  if (nrhs != 4) {
    mexErrMsgTxt("CreatePCEFactory requires 4 inputs.");
  }


  if (!mxIsNumeric(prhs[3]) || (mxGetM(prhs[3]) != 1) || (mxGetN(prhs[3]) != 1)) {
    mexErrMsgTxt("4th input must be one number.");
  }

  unsigned outputDim = static_cast<unsigned>(mxGetScalar(prhs[3]));

  if (!mxIsChar(prhs[2]) || (mxGetM(prhs[2]) != 1)) {
    mexErrMsgTxt("Third input must be a string.");
  }

  //copy out the name of the function to approx
  char  *fnName;
  mwSize buflen;
  buflen = mxGetN(prhs[2]) * sizeof(mxChar) + 1;
  fnName = static_cast<char *>(mxMalloc(buflen));
  mxGetString(prhs[2], fnName, buflen);

  //copy the name into a reference to a string
  std::shared_ptr<string> fnNameStr =  std::shared_ptr<string>(new string(fnName));

  //set up the variable collection
  VariableCollection::Ptr vars = VariableCollection::Ptr(new VariableCollection());

  HermitePolynomials1DRecursive::Ptr hermitePoly = HermitePolynomials1DRecursive::Ptr(
    new HermitePolynomials1DRecursive());
  LegendrePolynomials1DRecursive::Ptr legendrePoly = LegendrePolynomials1DRecursive::Ptr(
    new LegendrePolynomials1DRecursive());

  GaussPattersonQuadrature1D::Ptr  pattersonQuad  = GaussPattersonQuadrature1D::Ptr(new GaussPattersonQuadrature1D());
  GaussHermiteQuadrature1D::Ptr    hermiteQuad    = GaussHermiteQuadrature1D::Ptr(new GaussHermiteQuadrature1D());
  ExpGrowthQuadratureFamily1D::Ptr expHermiteQuad =
    ExpGrowthQuadratureFamily1D::Ptr(new ExpGrowthQuadratureFamily1D(hermiteQuad));

  if (!mxIsCell(prhs[1])) {
    mexErrMsgTxt("2nd input must be a cell array.");
  }

  mwSize   cellSize = mxGetNumberOfElements(prhs[1]);
  unsigned inputDim = static_cast<unsigned>(cellSize); //get unsigned version

  for (unsigned int i = 0; i < inputDim; ++i) {
    //grab the matlab array with the name
    mxArray *dimTypeArray = mxGetCell(prhs[1], i);

    char  *dimType;
    mwSize buflen;
    buflen  = mxGetN(dimTypeArray) * sizeof(mxChar) + 1;
    dimType = static_cast<char *>(mxMalloc(buflen));
    mxGetString(dimTypeArray, dimType, buflen);

    if (!strcmp("Gaussian", dimType)) {
      vars->PushVariable("x", hermitePoly, expHermiteQuad);
    } else if (!strcmp("Uniform", dimType)) {
      vars->PushVariable("x", legendrePoly, pattersonQuad);
    } else {
      mexErrMsgTxt("Variable type not supported.");
    }
  }

  //create a cached function wrapper, binding in the name of the function
  CachedFunctionWrapper::Ptr userFn =
    CachedFunctionWrapper::Ptr(new CachedFunctionWrapper(boost::bind(UserMatlabFn, _1, fnNameStr, inputDim,
                                                                     outputDim),                         inputDim,
                                                         outputDim));

  //return factory set up with no limiting set
  return SmolyakPCEFactory::Ptr(new SmolyakPCEFactory(vars, userFn, 0));
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  if (!googleLogInitialized) {
    google::InitGoogleLogging("");
    googleLogInitialized = true;
  }

  //mxArray   *new_number, *str;
  double out;

  LegendrePolynomials1DRecursive legendrePoly;

  int handleToReturn = 0;


  if (nlhs > 1) {
      mexErrMsgTxt("At most one output.");
  }

  if (nrhs <= 1) {
      mexErrMsgTxt("Requires at least one input.");
  }

  if (!mxIsChar(prhs[0]) || (mxGetM(prhs[0]) != 1)) {
      mexErrMsgTxt("First input must be a string.");
  }


  //        mexPrintf("M\n");mexEvalString("drawnow;");


  char  *commandStr;
  mwSize buflen;
  buflen     = mxGetN(prhs[0]) * sizeof(mxChar) + 1;
  commandStr = static_cast<char *>(mxMalloc(buflen));
  mxGetString(prhs[0], commandStr, buflen);

  if (!strcmp("CreatePCEFactory", commandStr)) {
    handleToReturn               = nextCacheIndex;
    factoryCache[nextCacheIndex] = ConstructSmolyakPCEFactory(nrhs, prhs);
    ++nextCacheIndex;
  } else if (!strcmp("BeginAdaptForTime", commandStr)) {
    if (nrhs != 3) {
      mexErrMsgTxt("Requires at three inputs.");
    }
    if (!mxIsNumeric(prhs[1]) || (mxGetM(prhs[1]) != 1) || (mxGetN(prhs[1]) != 1)) {
      mexErrMsgTxt("2nd input must be one number.");
    }
    if (!mxIsNumeric(prhs[2]) || (mxGetM(prhs[2]) != 1) || (mxGetN(prhs[2]) != 1)) {
      mexErrMsgTxt("3nd input must be one number.");
    }
    int factoryIndex    = static_cast<int>(mxGetScalar(prhs[1]));
    double secondsToRun = mxGetScalar(prhs[2]);

    if (!factoryCache.count(factoryIndex)) {
      mexErrMsgTxt("Invalid handle.");
    }

    //compute
    handleToReturn = nextCacheIndex;

    pceCache[nextCacheIndex] = factoryCache[factoryIndex]->StartAdaptiveTimed(1, secondsToRun);
    mexPrintf("Expansion has %d terms.\n", pceCache[nextCacheIndex]->NumberOfPolynomials());
    ++nextCacheIndex;
  } else if (!strcmp("ContinueAdaptForTime", commandStr)) {
    if (nrhs != 3) {
      mexErrMsgTxt("Requires three inputs.");
    }
    if (!mxIsNumeric(prhs[1]) || (mxGetM(prhs[1]) != 1) || (mxGetN(prhs[1]) != 1)) {
      mexErrMsgTxt("2nd input must be one number.");
    }
    if (!mxIsNumeric(prhs[2]) || (mxGetM(prhs[2]) != 1) || (mxGetN(prhs[2]) != 1)) {
      mexErrMsgTxt("3nd input must be one number.");
    }
    int factoryIndex    = static_cast<int>(mxGetScalar(prhs[1]));
    double secondsToRun = mxGetScalar(prhs[2]);

    if (!factoryCache.count(factoryIndex)) {
      mexErrMsgTxt("Invalid handle.");
    }

    //compute
    handleToReturn = nextCacheIndex;

    pceCache[nextCacheIndex] = factoryCache[factoryIndex]->AdaptForTime(secondsToRun, 0);
    mexPrintf("Expansion now has %d terms.\n", pceCache[nextCacheIndex]->NumberOfPolynomials());
    ++nextCacheIndex;
  } else if (!strcmp("EvaluatePCE", commandStr)) {
    if (nrhs != 3) {
      mexErrMsgTxt("Requires three inputs.");
    }
    if (!mxIsNumeric(prhs[1]) || (mxGetM(prhs[1]) != 1) || (mxGetN(prhs[1]) != 1)) {
      mexErrMsgTxt("2nd input must be one number.");
    }
    if (!mxIsNumeric(prhs[2]) || (mxGetN(prhs[2]) != 1)) {
      mexErrMsgTxt("3nd input must be a column vector.");
    }
    int pceIndex = static_cast<int>(mxGetScalar(prhs[1]));

    //copy in
    VectorXd pceInput(mxGetM(prhs[2]));
    double  *pceData = mxGetPr(prhs[2]);
    for (unsigned i = 0; i < pceInput.rows(); ++i) {
      pceInput(i) = pceData[i];
    }


    //check that the handle works
    if (!pceCache.count(pceIndex)) {
      mexErrMsgTxt("Invalid PCE handle.");
    }

    //compute the evaluation
    VectorXd pceResult = pceCache[pceIndex]->Evaluate(pceInput);

    //copy the result back
    plhs[0] = mxCreateNumericMatrix(pceResult.rows(), 1, mxDOUBLE_CLASS, mxREAL);
    double *handleReturnArrayPtr = mxGetPr(plhs[0]);
    for (unsigned i = 0; i < pceResult.rows(); ++i) {
      handleReturnArrayPtr[i] = pceResult(i);
    }


    handleToReturn = 0;
  } else if (!strcmp("SavePCE", commandStr)) {
    if (nrhs != 3) {
      mexErrMsgTxt("Requires three inputs.");
    }
    if (!mxIsChar(prhs[1]) || (mxGetM(prhs[1]) != 1)) {
      mexErrMsgTxt("Second input must be a string.");
    }

    if (!mxIsNumeric(prhs[2]) || (mxGetM(prhs[2]) != 1) || (mxGetN(prhs[2]) != 1)) {
      mexErrMsgTxt("3nd input must be one number.");
    }
    int pceIndex = static_cast<int>(mxGetScalar(prhs[2]));

    //grab the filename
    char  *filename;
    mwSize filebuflen = mxGetN(prhs[1]) * sizeof(mxChar) + 1;
    filename = static_cast<char *>(mxMalloc(filebuflen));
    mxGetString(prhs[1], filename, filebuflen);

    PolynomialChaosExpansion::SaveToFile(pceCache[pceIndex], filename);
  } else if (!strcmp("LoadPCE", commandStr)) {
    if (nrhs != 2) {
      mexErrMsgTxt("Requires two inputs.");
    }
    if (!mxIsChar(prhs[1]) || (mxGetM(prhs[1]) != 1)) {
      mexErrMsgTxt("Second input must be a string.");
    }

    //grab the filename
    char  *filename;
    mwSize filebuflen = mxGetN(prhs[1]) * sizeof(mxChar) + 1;
    filename = static_cast<char *>(mxMalloc(filebuflen));
    mxGetString(prhs[1], filename, filebuflen);

    handleToReturn           = nextCacheIndex;
    pceCache[nextCacheIndex] = PolynomialChaosExpansion::LoadFromFile(filename);
    ++nextCacheIndex;
  } else if (!strcmp("SaveSmolyakFactory", commandStr)) {
    if (nrhs != 3) {
      mexErrMsgTxt("Requires three inputs.");
    }
    if (!mxIsChar(prhs[1]) || (mxGetM(prhs[1]) != 1)) {
      mexErrMsgTxt("Second input must be a string.");
    }

    if (!mxIsNumeric(prhs[2]) || (mxGetM(prhs[2]) != 1) || (mxGetN(prhs[2]) != 1)) {
      mexErrMsgTxt("3nd input must be one number.");
    }
    int smolyakIndex = static_cast<int>(mxGetScalar(prhs[2]));

    //grab the filename
    char  *filename;
    mwSize filebuflen = mxGetN(prhs[1]) * sizeof(mxChar) + 1;
    filename = static_cast<char *>(mxMalloc(filebuflen));
    mxGetString(prhs[1], filename, filebuflen);

    SmolyakPCEFactory::SaveProgress(factoryCache[smolyakIndex], filename);
  } else if (!strcmp("LoadSmolyakFactory", commandStr)) {
    if (nrhs != 5) {
      mexErrMsgTxt("Requires five inputs.");
    }
    if (!mxIsChar(prhs[1]) || (mxGetM(prhs[1]) != 1)) {
      mexErrMsgTxt("Second input must be a string.");
    }

    //grab the filename
    char  *filename;
    mwSize filebuflen = mxGetN(prhs[1]) * sizeof(mxChar) + 1;
    filename = static_cast<char *>(mxMalloc(filebuflen));
    mxGetString(prhs[1], filename, filebuflen);

    if (!mxIsNumeric(prhs[3]) || (mxGetM(prhs[3]) != 1) || (mxGetN(prhs[3]) != 1)) {
      mexErrMsgTxt("4th input must be one number.");
    }

    unsigned inputDim = static_cast<unsigned>(mxGetScalar(prhs[3]));

    if (!mxIsNumeric(prhs[4]) || (mxGetM(prhs[4]) != 1) || (mxGetN(prhs[4]) != 1)) {
      mexErrMsgTxt("5th input must be one number.");
    }

    unsigned outputDim = static_cast<unsigned>(mxGetScalar(prhs[4]));


    if (!mxIsChar(prhs[2]) || (mxGetM(prhs[2]) != 1)) {
      mexErrMsgTxt("Third input must be a string.");
    }

    //copy out the name of the function to approx
    char  *fnName;
    mwSize buflen;
    buflen = mxGetN(prhs[2]) * sizeof(mxChar) + 1;
    fnName = static_cast<char *>(mxMalloc(buflen));
    mxGetString(prhs[2], fnName, buflen);

    //copy the name into a reference to a string
    std::shared_ptr<string> fnNameStr =  std::shared_ptr<string>(new string(fnName));


    handleToReturn               = nextCacheIndex;
    factoryCache[nextCacheIndex] =
      SmolyakPCEFactory::LoadProgress(filename, boost::bind(UserMatlabFn, _1, fnNameStr, inputDim, outputDim));
    ++nextCacheIndex;
  } else {
    mexErrMsgTxt("Not a valid command.");
  }


  if (handleToReturn) {
    plhs[0] = mxCreateNumericMatrix(1, 1, mxDOUBLE_CLASS, mxREAL);
    double *handleReturnArrayPtr = mxGetPr(plhs[0]);
    handleReturnArrayPtr[0] = static_cast<double>(handleToReturn);
  }
}
