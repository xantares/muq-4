set(polychaos_matlab_sources ${CMAKE_CURRENT_SOURCE_DIR}/muqPolychaos.cpp)




#We must use the system command mex to compile the mex function, this calls a script that does so.
ADD_CUSTOM_COMMAND(OUTPUT polychaosMexCompilationStep
    COMMAND   ${CMAKE_CURRENT_SOURCE_DIR}/buildMex ARGS ${polychaos_matlab_sources} -output ${INSTALL_LIB_DIR}/muqPolychaos -I${PROJECT_SOURCE_DIR} /usr/local/lib/libboost_date_time.a    ${INSTALL_LIB_DIR}/libpolychaosLibStatic.a  ${INSTALL_LIB_DIR}/libMuqUtilitiesStatic.a  /usr/local/lib/libboost_serialization.a  -lglog
    DEPENDS polychaosLibStatic MuqUtilitiesStatic
    VERBATIM)

ADD_CUSTOM_TARGET(polychaosMatlabBindings ALL
    ${CMAKE_COMMAND} -E echo "Building target polychaosMatlabBindings"
    DEPENDS polychaosMexCompilationStep polychaosLib MuqUtilities ${polychaos_matlab_sources}
    VERBATIM)
 
