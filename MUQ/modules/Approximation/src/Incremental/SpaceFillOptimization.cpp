#include "MUQ/Approximation/Incremental/SpaceFillOptimization.h"

using namespace std;
using namespace muq::Optimization;
using namespace muq::Approximation;

SpaceFillOptimization::SpaceFillOptimization(unsigned int const dim, Eigen::MatrixXd const& neighbors) : 
  OptProbBase(dim), neighbors(neighbors) {}

double SpaceFillOptimization::eval(Eigen::VectorXd const& in) 
{ 
  return  -0.5 * (neighbors.colwise() - in).colwise().squaredNorm().minCoeff();
}

double SpaceFillOptimization::grad(Eigen::VectorXd const& in, Eigen::VectorXd& gradient) 
{
  Eigen::VectorXd::Index minIndex;
  double minDistance = 0.5 * (neighbors.colwise() - in).colwise().squaredNorm().minCoeff(&minIndex);
  
  gradient = -in.array() + neighbors.col(minIndex).array();
  return -minDistance;
}
