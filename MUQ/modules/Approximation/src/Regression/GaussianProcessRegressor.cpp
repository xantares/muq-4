#include "MUQ/Approximation/Regression/GaussianProcessRegressor.h"

#include <iomanip>

#include <boost/property_tree/ptree.hpp>
#include <Eigen/Cholesky>
#include <Eigen/Dense>

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/FiniteDifferencer.h"
#include "MUQ/Utilities/HDF5Logging.h"

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"
#include "MUQ/Optimization/Problems/OptProbBase.h"

#include "MUQ/Geostatistics/AnisotropicPowerKernelNugget.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Geostatistics;
using namespace muq::Optimization;
using namespace muq::Approximation;

GaussianProcessRegressor::GaussianProcessRegressor(int dimIn, int dimOut) : Regressor(dimIn, dimOut)
{
  Kernel = make_shared<muq::Geostatistics::AnisotropicPowerKernelNugget>(Eigen::VectorXd::Ones(dimIn), 2, 1e-4);
}

GaussianProcessRegressor::GaussianProcessRegressor(int dimIn, int dimOut,
                                                   boost::property_tree::ptree& properties) : Regressor(dimIn, dimOut)
{
  nuggetScale = ReadAndLogParameter(properties, "Regressor.nuggetScale", nuggetScale);

  Kernel = make_shared<muq::Geostatistics::AnisotropicPowerKernelNugget>(Eigen::VectorXd::Ones(dimIn), 2, 1e-4);
}

double gammaUnNorm(double x, double shape, double scale)
{
  return log(x) * (shape - 1.0) + (-x / scale);
}

double gammaUnNormDeriv(double x, double shape, double scale)
{
  return (shape * scale - scale - x) / (scale * x);
}

class GpMle : public OptProbBase {
public:

  GpMle(MatrixXd const                                                  & xs,
        MatrixXd const                                                  & ys,
        std::shared_ptr<muq::Geostatistics::AnisotropicPowerKernelNugget> kernel,
        double                                                            nuggetScale,
        double                                                            correlationLengthScale) : OptProbBase(
                                                                                                      kernel->GetParms().rows()),
                                                                                                    xs(xs), ys(ys),
  kernel(kernel), nuggetScale(
                                                                                                      nuggetScale),
                                                                                                    correlationLengthScale(
                                                                                                      correlationLengthScale)
  {}

  virtual ~GpMle() = default;

  virtual double eval(const Eigen::VectorXd& xc) override
  {
    kernel->SetParms(xc);
    MatrixXd cov = kernel->BuildCov(xs);
    cholCreator.compute(cov.selfadjointView<Eigen::Lower>());

    double logDensity = -0.5 * cholCreator.vectorD().array().log().matrix().sum() + gammaUnNorm(xc(0), 2, nuggetScale); //determinant
                                                                                                                        // of
                                                                                                                        // covariance
                                                                                                                        // //add
                                                                                                                        // prior
                                                                                                                        // on
                                                                                                                        // terms
    double N          = xs.cols();

    for (int i = 1; i < xc.rows(); ++i) {
      logDensity += gammaUnNorm(xc(i), 2, correlationLengthScale);
    }

    //compute all the psis at once
    VectorXd allPsi = (ys * (cholCreator.solve(ys.transpose()))).diagonal();

    for (int dataIndex = 0; dataIndex < ys.rows(); ++dataIndex) {
      //add in likelihood term
      logDensity += -N / 2.0 / static_cast<double>(ys.rows()) * log(allPsi(dataIndex) / 2.0); //average the different
                                                                                              // outputs
    }
    return -logDensity;
  }

  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient) override
  {
    // resize the gradient
    gradient.resize(dim);

    double logDensity = eval(xc);
    vector<MatrixXd> GradMat;
    kernel->BuildGradMat(xs, GradMat);

    //compute the gradient of the determinant term
    for (int paramIndex = 0; paramIndex < dim; ++paramIndex) {
        gradient(paramIndex) = -0.5 * (cholCreator.solve(GradMat.at(paramIndex))).trace();
      if (paramIndex == 0) {
        gradient(paramIndex) += gammaUnNormDeriv(xc(0), 2, nuggetScale);
      } else {
        gradient(paramIndex) += gammaUnNormDeriv(xc(paramIndex), 2, correlationLengthScale);
      }
    }
    double N = xs.cols();

    MatrixXd alphas = cholCreator.solve(ys.transpose());

    //compute gradient of the likelihood terms
    for (int outputIndex = 0; outputIndex < ys.rows(); ++outputIndex) {
      VectorXd alpha = alphas.col(outputIndex);
      double   psi   = ys.row(outputIndex).transpose().dot(alpha);
      for (int paramIndex = 0; paramIndex < dim; ++paramIndex) {
        double partOfGradient =  N / 2.0 / psi * alpha.transpose().dot(GradMat.at(paramIndex) * alpha);

        gradient(paramIndex) += partOfGradient;
      }
    }
    gradient = -gradient;

    return logDensity;
  }

private:

  LDLT<MatrixXd> cholCreator;
  //    LLT<MatrixXd> cholCreator;
  MatrixXd xs;
  MatrixXd ys;
  std::shared_ptr<muq::Geostatistics::AnisotropicPowerKernelNugget> kernel;

  double nuggetScale;
  double correlationLengthScale;
};

void GaussianProcessRegressor::Fit(const Eigen::MatrixXd& xsIn, const Eigen::MatrixXd& ysIn, Eigen::VectorXd const&){
  Fit(xsIn, ysIn);	
}


void GaussianProcessRegressor::Fit(const Eigen::MatrixXd& xsIn, const Eigen::MatrixXd& ysIn)
{
  assert(xsIn.rows() == inputSizes(0));
  assert(ysIn.rows() == outputSize);

  assert(xsIn.cols() == xsIn.cols());


  xScales = xsIn.rowwise().maxCoeff() - xsIn.rowwise().minCoeff();

  scaledXs = xsIn.array() / xScales.replicate(1, xsIn.cols()).array();


  outputEmpiricalMean = ysIn.rowwise().mean();
  MatrixXd centeredYs = (ysIn - outputEmpiricalMean.replicate(1, ysIn.cols()));

  yScales  = centeredYs.rowwise().norm() / sqrt(static_cast<double>(centeredYs.cols() - 1.0));
  scaledYs = centeredYs.array() / yScales.replicate(1, ysIn.cols()).array();

  auto mleProblem = make_shared<GpMle>(scaledXs, scaledYs, Kernel, nuggetScale, correlationLengthScale);

  int optDim = Kernel->GetParms().rows();
  //bound all of the values to be positive
  for (int i = 0; i < optDim; ++i) {
    std::shared_ptr<muq::Optimization::ConstraintBase> positivityBound =
      std::make_shared<muq::Optimization::BoundConstraint>(optDim, i, 1e-15, true);
    mleProblem->AddConstraint(positivityBound, false);
    std::shared_ptr<muq::Optimization::ConstraintBase> maxBound = std::make_shared<muq::Optimization::BoundConstraint>(
      optDim,
      i,
      10,
      false);
    mleProblem->AddConstraint(maxBound, false);
  }

  boost::property_tree::ptree params;
#ifdef MUQ_USE_NLOPT
  params.put("Opt.Method", "NLOPT");
  params.put("Opt.ConstraintHandler", "Native");
  params.put("Opt.NLOPT.Method", "BOBYQA");
  params.put("Opt.NLOPT.xtol_rel", 1e-10);
  params.put("Opt.NLOPT.xtol_abs", 1e-10);
  params.put("Opt.NLOPT.ftol_rel", 1e-8);
  params.put("Opt.NLOPT.ftol_abs", 1e-10);
#else // ifdef MUQ_USE_NLOPT
  params.put("Opt.Method", "BFGS_Line");
  params.put("Opt.ConstraintHandler", "AugLag");
  params.put("Opt.xtol", 1e-12);
  params.put("Opt.gtol", 1e-8);
  params.put("Opt.ftol", 1e-8);
  params.put("Opt.verbosity", 0);
  params.put("Opt.MaxIts", 10000);
  params.put("Opt.ctol", 1e-12);
  params.put("Opt.LineSearch.LineIts", 100);
  params.put("Opt.StepLength", 0);
#endif // ifdef MUQ_USE_NLOPT

  auto solver = OptAlgBase::Create(mleProblem, params);

  VectorXd xStart = VectorXd::Constant(optDim, 1);

    xStart(0) = nuggetScale * .01; //set nugget to be small

  for (int i = 1; i < optDim; ++i) {
    xStart(i) = .25;
  }

  VectorXd solved = solver->solve(xStart);

  if (solver->GetStatus() <= 0) {
    LOG(ERROR) << "Solver status " << solver->GetStatus();
  }
  assert(solver->GetStatus() > 0);
  Kernel->SetParms(solved);


  //create the stored products
  cholCreator.compute(Kernel->BuildCov(scaledXs));
  KinvYs = cholCreator.solve(scaledYs.transpose());


  //be careful that cached results cannot be returned
  InvalidateCache();
}

/** Use the kriging weights to predict the output for a particular input location. */
Eigen::VectorXd GaussianProcessRegressor::EvaluateImpl(const Eigen::VectorXd& x)
{
  VectorXd xScaled         = x.array() / xScales.array(); // build the covariance between the input location and the
                                                          // data locations
  Eigen::VectorXd InCovDat = Eigen::VectorXd::Zero(scaledXs.cols());

  for (unsigned int i = 0; i < scaledXs.cols(); ++i) {
    InCovDat(i) = Kernel->Kernel(xScaled, scaledXs.col(i));
  }

  return (InCovDat.transpose() * KinvYs).transpose().array() * yScales.array() + outputEmpiricalMean.array();
}

Eigen::VectorXd GaussianProcessRegressor::GetPredictedVariance(const Eigen::VectorXd& x)
{
  VectorXd xScaled = x.array() / xScales.array();

  VectorXd variances(outputSize);

  // build the covariance between the input location and the data locations
  Eigen::VectorXd InCovDat = Eigen::VectorXd::Zero(scaledXs.cols());

  for (unsigned int i = 0; i < scaledXs.cols(); ++i) {
    InCovDat[i] = Kernel->Kernel(xScaled, scaledXs.col(i));
  }
  double N                        = scaledXs.cols();
  double spatialVarianceComponent = InCovDat.transpose() * cholCreator.solve(InCovDat);
  for (int i = 0; i < outputSize; ++i) {
    //scaledYs*KinvYs is psi
    variances(i) =
      (scaledYs.row(i) * KinvYs.col(i) * (Kernel->Kernel(xScaled, xScaled) - spatialVarianceComponent) / N)(0, 0);
  }

  return variances.array() * yScales.array() * N / (N - 2.0);
}

Eigen::VectorXd GaussianProcessRegressor::GetNuggetVariance(const Eigen::VectorXd& x)
{
  VectorXd xScaled = x.array() / xScales.array();
  double   N       = scaledXs.cols();
  VectorXd variances(outputSize);

  for (int i = 0; i < outputSize; ++i) {
    //scaledYs*KinvYs is psi
    variances(i) = (scaledYs.row(i) * KinvYs.col(i) * (Kernel->Kernel(xScaled, xScaled) - 1.0) / N)(0, 0);
  }

  return variances.array() * yScales.array() * N / (N - 2.0);
}

Eigen::MatrixXd GaussianProcessRegressor::JacobianImpl(const Eigen::VectorXd& x)
{
  assert(false);
  return MatrixXd(1, 1);
}

void GaussianProcessRegressor::FitNoOpt(const Eigen::MatrixXd& xsIn, const Eigen::MatrixXd& ysIn)
{
  assert(xsIn.rows() == inputSizes(0));
  assert(ysIn.rows() == outputSize);

  assert(xsIn.cols() == xsIn.cols());
  scaledXs = xsIn.array() / xScales.replicate(1, xsIn.cols()).array();
  scaledYs = ysIn.array() / yScales.replicate(1, ysIn.cols()).array();


  //reusing the old kernel, so just perform the factorization
  cholCreator.compute(Kernel->BuildCov(scaledXs));
  KinvYs = cholCreator.solve(scaledYs.transpose());


  //be careful that cached results cannot be returned
  InvalidateCache();
}

Eigen::VectorXd GaussianProcessRegressor::SelectNextPointRand(Eigen::VectorXd const       & x,
                                                              std::vector<Eigen::VectorXd>& candidates)
{
  VectorXd scores(candidates.size());
  double   weightTotal = 0;
  VectorXd weights(candidates.size());
  int i = 0;

  for (auto unscaledCandidate : candidates) {
    //use real corr lengths, so no need to scale
    VectorXd candidateX = (unscaledCandidate - x).array() / GetCorrelationLengths().array();
    double   density    = gammaUnNorm(candidateX.norm(), 1, 6);

    weights(i) = density;
    ++i;
  }

  weights = weights.array().exp();

  for (int j = 1; j < weights.size(); ++j) {
    weights(j) += weights(j - 1);
  }


  weights /= weights(weights.size() - 1);
  //pick which one
  double draw = muq::Utilities::RandomGenerator::GetUniform();
  //find it in the list
  for (i = 0; draw > weights(i) && i < weights.rows(); ++i) {}

  VectorXd best = candidates.at(i);
  candidates.erase(candidates.begin() + i);
  return best;
}

Eigen::VectorXd GaussianProcessRegressor::GetCorrelationLengths()
{
  VectorXd parms = Kernel->GetParms();

  return parms.tail(parms.rows() - 1).array() * xScales.array();
}

double GaussianProcessRegressor::GetNugget()
{
  VectorXd parms = Kernel->GetParms();

  return parms(0);
}

