#include "gtest/gtest.h"


#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"
#include "MUQ/Approximation/Expansions/PolynomialExpansion.h"

#include "MUQ/Utilities/multiIndex/MultiIndex.h"
#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

#include "MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h"
#include "MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h"


using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;
using namespace muq::Approximation;

TEST(ApproximationPolynomialExpansion, ZeroCoeffHermite)
{
  const int outDim = 10;
  const int inputDim = 5;
  const int order = 3;
  
  shared_ptr<MultiIndexSet> multis = MultiIndexFactory::CreateTotalOrder(inputDim, order);
  shared_ptr<RecursivePolynomialFamily1D> poly = make_shared<HermitePolynomials1DRecursive>();
  
  PolynomialExpansion expansion(outDim,multis,poly);
  
  Eigen::VectorXd input = Eigen::VectorXd::Random(inputDim);
  
  Eigen::VectorXd output = expansion.Evaluate(input);
  for(int i=0; i<outDim; ++i)
    EXPECT_DOUBLE_EQ(0.0,output(i));
  
  Eigen::MatrixXd jac = expansion.Jacobian(input);
  for(int i=0; i<inputDim; ++i){
    for(int j=0; j<outDim; ++j)
      EXPECT_DOUBLE_EQ(0.0,jac(j,i));
  }

}

TEST(ApproximationPolynomialExpansion, ZeroCoeffLegendre)
{
  const int outDim = 10;
  const int inputDim = 5;
  const int order = 3;
  
  shared_ptr<MultiIndexSet> multis = MultiIndexFactory::CreateTotalOrder(inputDim, order);
  shared_ptr<RecursivePolynomialFamily1D> poly = make_shared<LegendrePolynomials1DRecursive>();
  
  PolynomialExpansion expansion(outDim,multis,poly);
  
  Eigen::VectorXd input = Eigen::VectorXd::Random(inputDim);
  
  Eigen::VectorXd output = expansion.Evaluate(input);
  for(int i=0; i<outDim; ++i)
    EXPECT_DOUBLE_EQ(0.0,output(i));
  
  Eigen::MatrixXd jac = expansion.Jacobian(input);
  for(int i=0; i<inputDim; ++i){
    for(int j=0; j<outDim; ++j)
      EXPECT_DOUBLE_EQ(0.0,jac(j,i));
  }
  
}

TEST(ApproximationPolynomialExpansion, BasicLinearHermite)
{
  const int outDim = 1;
  const int inputDim = 2;
  const int order = 1;
  
  shared_ptr<MultiIndexSet> multis = MultiIndexFactory::CreateTotalOrder(inputDim, order);
  shared_ptr<RecursivePolynomialFamily1D> poly = make_shared<HermitePolynomials1DRecursive>();
  
  PolynomialExpansion expansion(Eigen::MatrixXd::Ones(outDim,multis->GetNumberOfIndices()),multis,poly);
  
  Eigen::VectorXd input(2);
  Eigen::VectorXd output;
  
  input << 0.5, 0.0;
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(2.0,output(0));
  
  Eigen::MatrixXd jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(2.0,jac(0));
  EXPECT_DOUBLE_EQ(2.0,jac(1));
  
  input << 0.0, 0.5;
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(2.0,output(0));
  
  jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(2.0,jac(0));
  EXPECT_DOUBLE_EQ(2.0,jac(1));
  
  input << 0.5, 0.5;
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(3.0,output(0));
  
  jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(2.0,jac(0));
  EXPECT_DOUBLE_EQ(2.0,jac(1));
  
}

TEST(ApproximationPolynomialExpansion, BasicLinearLegendre)
{
  const int outDim = 1;
  const int inputDim = 2;
  const int order = 1;
  
  shared_ptr<MultiIndexSet> multis = MultiIndexFactory::CreateTotalOrder(inputDim, order);
  shared_ptr<RecursivePolynomialFamily1D> poly = make_shared<LegendrePolynomials1DRecursive>();
  
  PolynomialExpansion expansion(Eigen::MatrixXd::Ones(outDim,multis->GetNumberOfIndices()),multis,poly);
  
  Eigen::VectorXd input(2);
  Eigen::VectorXd output;
  
  input << 0.5, 0.0;
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(1.5,output(0));
  
  Eigen::MatrixXd jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(1.0,jac(0));
  EXPECT_DOUBLE_EQ(1.0,jac(1));
  
  input << 0.0, 0.5;
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(1.5,output(0));
  
  jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(1.0,jac(0));
  EXPECT_DOUBLE_EQ(1.0,jac(1));
  
  input << 0.5, 0.5;
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(2.0,output(0));
  
  jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(1.0,jac(0));
  EXPECT_DOUBLE_EQ(1.0,jac(1));
  
}


TEST(ApproximationPolynomialExpansion, BasicCubicHermite)
{
  const int outDim = 1;
  const int inputDim = 2;
  const int order = 3;
  
  shared_ptr<MultiIndexSet> multis = MultiIndexFactory::CreateTotalOrder(inputDim, order);
  shared_ptr<RecursivePolynomialFamily1D> poly = make_shared<HermitePolynomials1DRecursive>();
  
  PolynomialExpansion expansion(Eigen::MatrixXd::Ones(outDim,multis->GetNumberOfIndices()),multis,poly);
  
  Eigen::VectorXd input(2);
  Eigen::VectorXd output;
  
  input << 0.5, 0.0;
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(-8.0,output(0));
  
  Eigen::MatrixXd jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(-4.0,jac(0));
  EXPECT_DOUBLE_EQ(-10.0,jac(1));
  
  input << 0.0, 0.5;
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(-8.0,output(0));
  
  jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(-10.0,jac(0));
  EXPECT_DOUBLE_EQ(-4.0,jac(1));
  
  input << 0.5, 0.5;
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(-10.0,output(0));
  
  jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(4.0,jac(0));
  EXPECT_DOUBLE_EQ(4.0,jac(1));
  
}


TEST(ApproximationPolynomialExpansion, BasicCubicLegendre)
{
  const int outDim = 1;
  const int inputDim = 2;
  const int order = 3;
  
  shared_ptr<MultiIndexSet> multis = MultiIndexFactory::CreateTotalOrder(inputDim, order);
  shared_ptr<RecursivePolynomialFamily1D> poly = make_shared<LegendrePolynomials1DRecursive>();
  
  PolynomialExpansion expansion(Eigen::MatrixXd::Ones(outDim,multis->GetNumberOfIndices()),multis,poly);
  
  Eigen::VectorXd input(2);
  Eigen::VectorXd output;
  
  input << 0.5, 0.0;
  output = expansion.Evaluate(input);
  double trueVal = 1 + input(1) + 0.5*(3.0*pow(input(1),2.0)-1) + 0.5*(5.0*pow(input(1),3.0)-3.0*input(1)) +
  input(0) + input(0)*input(1) + 0.5*input(0)*(3.0*pow(input(1),2.0)-1) + 0.5*(3.0*pow(input(0),2.0)-1) + 0.5*(3.0*pow(input(0),2.0)-1)*input(1) + 0.5*(5.0*pow(input(0),3.0)-3.0*input(0));
  EXPECT_DOUBLE_EQ(trueVal,output(0));
  
  Eigen::VectorXd trueJac(2);
  trueJac(0) = 1.0 + input(1) + 0.5*(3.0*pow(input(1),2.0)-1.0) + 3.0*input(0) + 3.0*input(0)*input(1) + 0.5*(15.0*pow(input(0),2.0)-3.0);
  trueJac(1) = 1 + 3.0*input(1) + 0.5*(15*pow(input(1),2.0)-3.0) + input(0) + 3*input(1)*input(0) + 0.5*(3.0*pow(input(0),2.0)-1.0);
  Eigen::MatrixXd jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(trueJac(0),jac(0));
  EXPECT_DOUBLE_EQ(trueJac(1),jac(1));
  
  input << 0.0, 0.5;
  trueVal = 1 + input(1) + 0.5*(3.0*pow(input(1),2.0)-1) + 0.5*(5.0*pow(input(1),3.0)-3.0*input(1)) +
  input(0) + input(0)*input(1) + 0.5*input(0)*(3.0*pow(input(1),2.0)-1) + 0.5*(3.0*pow(input(0),2.0)-1) + 0.5*(3.0*pow(input(0),2.0)-1)*input(1) + 0.5*(5.0*pow(input(0),3.0)-3.0*input(0));
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(trueVal,output(0));
  
  trueJac(0) = 1.0 + input(1) + 0.5*(3.0*pow(input(1),2.0)-1.0) + 3.0*input(0) + 3.0*input(0)*input(1) + 0.5*(15.0*pow(input(0),2.0)-3.0);
  trueJac(1) = 1 + 3.0*input(1) + 0.5*(15*pow(input(1),2.0)-3.0) + input(0) + 3*input(1)*input(0) + 0.5*(3.0*pow(input(0),2.0)-1.0);
  jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(trueJac(0),jac(0));
  EXPECT_DOUBLE_EQ(trueJac(1),jac(1));
  
  input << 0.5, 0.5;
  trueVal = 1 + input(1) + 0.5*(3.0*pow(input(1),2.0)-1) + 0.5*(5.0*pow(input(1),3.0)-3.0*input(1)) +
  input(0) + input(0)*input(1) + 0.5*input(0)*(3.0*pow(input(1),2.0)-1) + 0.5*(3.0*pow(input(0),2.0)-1) + 0.5*(3.0*pow(input(0),2.0)-1)*input(1) + 0.5*(5.0*pow(input(0),3.0)-3.0*input(0));
  output = expansion.Evaluate(input);
  EXPECT_DOUBLE_EQ(trueVal,output(0));
  
  trueJac(0) = 1.0 + input(1) + 0.5*(3.0*pow(input(1),2.0)-1.0) + 3.0*input(0) + 3.0*input(0)*input(1) + 0.5*(15.0*pow(input(0),2.0)-3.0);
  trueJac(1) = 1 + 3.0*input(1) + 0.5*(15*pow(input(1),2.0)-3.0) + input(0) + 3*input(1)*input(0) + 0.5*(3.0*pow(input(0),2.0)-1.0);
  jac = expansion.Jacobian(input);
  EXPECT_DOUBLE_EQ(trueJac(0),jac(0));
  EXPECT_DOUBLE_EQ(trueJac(1),jac(1));
  
}


TEST(ApproximationPolynomialExpansion, Serialization)
{
  string filename = "results/tests/PolynomialExpansionArchive.txt";
  {
    const int outDim = 1;
    const int inputDim = 2;
    const int order = 3;
  
    shared_ptr<MultiIndexSet> multis = MultiIndexFactory::CreateTotalOrder(inputDim, order);
    shared_ptr<RecursivePolynomialFamily1D> poly = make_shared<HermitePolynomials1DRecursive>();
  
    std::shared_ptr<PolynomialExpansion> expansion = make_shared<PolynomialExpansion>(Eigen::MatrixXd::Ones(outDim,multis->GetNumberOfIndices()),multis,poly);
  
    ofstream ofs(filename.c_str());
    boost::archive::text_oarchive oa(ofs);
    oa << expansion;
  
  }
  
  std::shared_ptr<PolynomialExpansion> expansion2;
  std::ifstream ifs(filename.c_str());
  boost::archive::text_iarchive ia(ifs);
    
  ia >> expansion2;
  
  Eigen::VectorXd input(2);
  Eigen::VectorXd output;
  
  input << 0.5, 0.0;
  output = expansion2->Evaluate(input);
  EXPECT_EQ(-8.0,output(0));
  
  input << 0.0, 0.5;
  output = expansion2->Evaluate(input);
  EXPECT_EQ(-8.0,output(0));
  
  input << 0.5, 0.5;
  output = expansion2->Evaluate(input);
  EXPECT_EQ(-10.0,output(0));
}