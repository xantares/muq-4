#include <boost/property_tree/ptree.hpp> // needed here to avoid weird osx "toupper" bug when compiling MUQ with python

#include <iostream>
#include <stddef.h>


#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenUtils.h"

#include "MUQ/Utilities/Quadrature/ExpGrowthQuadratureFamily1D.h"
#include "MUQ/Utilities/Quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/Utilities/QuadratureTestFunctions.h"
#include "MUQ/Utilities/VariableCollection.h"
#include "MUQ/Modelling/CachedModPiece.h"
#include "MUQ/Modelling/ModPieceTemplates.h"

#include "MUQ/Approximation/smolyak/SmolyakQuadrature.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

TEST(SmolyakIsoSparseQuadrature, dasqTest2DIso1)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, legendreQuad1D);
  varCollection->PushVariable("y", nullptr, legendreQuad1D);


  auto fn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DIso1, 2, 1);


  SmolyakQuadrature isoQuad(varCollection, fn);
  VectorXd integral = isoQuad.StartFixedTerms(12);
  VectorXd correctIntegral(1);
  correctIntegral << 14.9364826562485405;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-12);
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DIso2)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, legendreQuad1D);
  varCollection->PushVariable("y", nullptr, legendreQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DIso2, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature isoQuad(varCollection, fn);
  VectorXd integral = isoQuad.StartFixedTerms(14);
  VectorXd correctIntegral(1);
  correctIntegral << 4.75446580837249437;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-12);
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DIso3)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, legendreQuad1D);
  varCollection->PushVariable("y", nullptr, legendreQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DIso3, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);
  SmolyakQuadrature isoQuad(varCollection, fn);
  VectorXd integral = isoQuad.StartFixedTerms(20);
  VectorXd correctIntegral(1);
  correctIntegral << 0.0785385998857827117;
  
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-12);
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DAniso1)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, legendreQuad1D);
  varCollection->PushVariable("y", nullptr, legendreQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DAniso1, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature isoQuad(varCollection, fn);
  VectorXd integral = isoQuad.StartFixedTerms(14);
  VectorXd correctIntegral(1);
  correctIntegral <<  8.21506546093669728;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-12);
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DAniso2)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, legendreQuad1D);
  varCollection->PushVariable("y", nullptr, legendreQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DAniso2, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature isoQuad(varCollection, fn);
  VectorXd integral = isoQuad.StartFixedTerms(14);
  VectorXd correctIntegral(1);
  correctIntegral <<  32.0801216275935365;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-12);
}

TEST(SmolyakIsoSparseQuadrature, dasqTest2DAniso3)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, legendreQuad1D);
  varCollection->PushVariable("y", nullptr, legendreQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DAniso3, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature isoQuad(varCollection, fn);
  VectorXd integral = isoQuad.StartFixedTerms(15);
  VectorXd correctIntegral(1);
  correctIntegral << 0.110897342159703106;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-12);
}


TEST(SmolyakAdaptiveSparseQuadrature, simplePoly1D)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var

  //	EXPECT_TRUE(false) << "test message.";

  varCollection->PushVariable("x1", nullptr, pattersonQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(simplePoly1D, 1, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature quad(varCollection, fn);
  VectorXd integral = quad.StartAdaptiveToTolerance(3, 1e-10);

  VectorXd correctIntegral(1);
  correctIntegral << 7.0 + 1.0 / 3.0;

  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-8); //analytic answer: 7+1/3
}

TEST(SmolyakAdaptiveSparseQuadrature, simplePoly3D)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var

  //	EXPECT_TRUE(false) << "test message.";

  varCollection->PushVariable("x1", nullptr, pattersonQuad1D);
  varCollection->PushVariable("x2", nullptr, pattersonQuad1D);
  varCollection->PushVariable("x3", nullptr, pattersonQuad1D);

  auto rawFn = make_shared<WrapVectorFunctionModPiece>(simplePoly3D, 3, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature quad(varCollection, fn);
  VectorXd integral = quad.StartAdaptiveToTolerance(3, 1e-10);

  VectorXd correctIntegral(1);
  correctIntegral << 8.0/9.0;

  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-8); //analytic answer: 8/9
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DIso1)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, pattersonQuad1D);
  varCollection->PushVariable("y", nullptr, pattersonQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DIso1, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature quad(varCollection, fn);
  VectorXd integral = quad.StartAdaptiveToTolerance(3, 1e-10);
  VectorXd correctIntegral(1);
  correctIntegral << 14.9364826562485405;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-10);
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DIso2)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, pattersonQuad1D);
  varCollection->PushVariable("y", nullptr, pattersonQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DIso2, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature quad(varCollection, fn);
  VectorXd integral = quad.StartAdaptiveToTolerance(3, 1e-10);
  VectorXd correctIntegral(1);
  correctIntegral << 4.75446580837249437;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-10);
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DIso3)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, pattersonQuad1D);
  varCollection->PushVariable("y", nullptr, pattersonQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DIso3, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature quad(varCollection, fn);
  VectorXd integral = quad.StartAdaptiveToTolerance(3, 1e-15);
  VectorXd correctIntegral(1);
  correctIntegral << 0.0785385998857827117;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-10);
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DAniso1)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, pattersonQuad1D);
  varCollection->PushVariable("y", nullptr, pattersonQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DAniso1, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature quad(varCollection, fn);
  VectorXd integral = quad.StartAdaptiveToTolerance(3, 1e-14);
  VectorXd correctIntegral(1);
  correctIntegral <<  8.21506546093669728;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-14);
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DAniso2)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, pattersonQuad1D);
  varCollection->PushVariable("y", nullptr, pattersonQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DAniso2, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature quad(varCollection, fn);
  VectorXd integral = quad.StartAdaptiveToTolerance(4, 1e-13);
  VectorXd correctIntegral(1);
  correctIntegral << 32.0801216275935365;

  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-10);
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DAniso3)
{
  GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, pattersonQuad1D);
  varCollection->PushVariable("y", nullptr, pattersonQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DAniso3, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature quad(varCollection, fn);
  VectorXd integral = quad.StartAdaptiveToTolerance(3, 1e-10);
  VectorXd correctIntegral(1);
  correctIntegral << 0.110897342159703106;
  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-14);
}

TEST(SmolyakAdaptiveSparseQuadrature, dasqTest2DAniso3ExpGrowth)
{
  GaussLegendreQuadrature1D::Ptr   legendreQuad1D(new GaussLegendreQuadrature1D());
  ExpGrowthQuadratureFamily1D::Ptr expLegendreQuad1D(new ExpGrowthQuadratureFamily1D(legendreQuad1D));
  VariableCollection::Ptr varCollection(new VariableCollection());

  //just one 1D var
  varCollection->PushVariable("x", nullptr, expLegendreQuad1D);
  varCollection->PushVariable("y", nullptr, expLegendreQuad1D);


  auto rawFn = make_shared<WrapVectorFunctionModPiece>(dasqTest2DAniso3, 2, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SmolyakQuadrature quad(varCollection, fn);
  VectorXd integral = quad.StartAdaptiveToTolerance(3, 1e-10);
  VectorXd correctIntegral(1);
  correctIntegral << 0.110897342159703106;

  EXPECT_NEAR(correctIntegral[0], integral[0], 1e-12);
}

///Based on dasqTest2DAniso3
// TEST(SmolyakAdaptiveSparseQuadrature,SaveAndLoad)
// {
//      GaussPattersonQuadrature1D::Ptr pattersonQuad1D(new GaussPattersonQuadrature1D());
//      VariableCollection::Ptr varCollection(new VariableCollection());
//      //just one 1D var
//      varCollection->PushVariable("x", nullptr, pattersonQuad1D);
//      varCollection->PushVariable("y", nullptr, pattersonQuad1D);
//
//
//      FunctionWrapper::Ptr fn= FunctionWrapper::WrapVectorFunction(dasqTest2DAniso3,2,1);
//      SmolyakQuadrature::Ptr outQuad(new SmolyakQuadrature(varCollection,fn));
//
//      outQuad->StartAdaptiveToTolerance(3,1e-10);
//
//      SmolyakQuadrature::SaveProgress(outQuad, "results/tests/SmolyakAdaptiveSparseQuadrature_SaveAndLoad");
//
//      SmolyakQuadrature::Ptr inQuad = SmolyakQuadrature::LoadProgress(
//                      "results/tests/SmolyakAdaptiveSparseQuadrature_SaveAndLoad", &dasqTest2DAniso3);
//
//      VectorXd integral = inQuad->AdaptToTolerance(1e-15);
//      VectorXd correctIntegral(1);
//      correctIntegral << 0.110897342159703106;
//      EXPECT_TRUE(MatrixApproxEqual( correctIntegral, integral, 1e-15));
// }
