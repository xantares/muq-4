
#include "UnconstrainedTestClasses.h"

using namespace muq::Optimization;
using namespace std;


TEST_F(Optimization_RoseLineSearchTest, RosenbrockCOBYLA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "COBYLA");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockBOBYQA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "BOBYQA");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockNEWUOA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "NEWUOA");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockPRAXIS)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "PRAXIS");
}


TEST_F(Optimization_RoseLineSearchTest, RosenbrockNM)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "NM");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockSbplx)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "Sbplx");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockMMA)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "MMA");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockSLSQP)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "SLSQP");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockLBFGS)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "LBFGS");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockPreTN)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "PreTN");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockLMVM)
{
  // set the method
  testParams.put("Opt.Method", "NLOPT");
  testParams.put("Opt.NLOPT.Method", "LMVM");
}
