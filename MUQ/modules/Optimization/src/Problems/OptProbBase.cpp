
#include "MUQ/Optimization/Problems/OptProbBase.h"

// std library includes
#include <functional>
#include <iostream>

// Eigen includes
#include <Eigen/Dense>

// Other muq includes
#include "MUQ/Utilities/FiniteDifferencer.h"

using namespace muq::Optimization;
using namespace muq::Utilities;
using namespace std;

Eigen::VectorXd justGrad(OptProbBase *obj, const Eigen::VectorXd& xc)
{
  Eigen::VectorXd grad;

  obj->grad(xc, grad);
  return grad;
}

/** Evaluate the gradient, using finite differences if a more sophisticated method was not implemented in child class.
 */
double OptProbBase::grad(const Eigen::VectorXd& xc, Eigen::VectorXd& grad)
{
  auto f1 = std::bind(&OptProbBase::eval, this, std::placeholders::_1);

  return computeFdGrad(f1, xc, grad, reltol, abstol);
}

/** Apply the Hessian inverse (or approximate Hessian) to the input vector.  This function defaults to finite
 *  differences but more efficient approximations such as Gauss-Newton model Hessians for nonlinear least squares
 * problems
 *  are implemented in child classes. */
Eigen::VectorXd OptProbBase::applyInvHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
{
  // solve the linear system
  return getHess(xc).selfadjointView<Eigen::Lower>().ldlt().solve(vecIn);
}

Eigen::MatrixXd OptProbBase::getInvHess(const Eigen::VectorXd& xc)
{
  return getHess(xc).selfadjointView<Eigen::Lower>().ldlt().solve(Eigen::MatrixXd::Identity(xc.size(), xc.size()));
}

/** Apply the Hessian (or approximate Hessian) to the input vector.  This function defaults to finite differences but
 *  more efficient approximations such as Gauss-Newton model Hessians for nonlinear least squares problems are
 * implemented
 *  in child classes.  */
Eigen::VectorXd OptProbBase::applyHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
{
  return getHess(xc) * vecIn;
}

Eigen::MatrixXd OptProbBase::getHess(const Eigen::VectorXd& xc)
{
  auto f = std::bind(&OptProbBase::eval, this, std::placeholders::_1);
  auto g = std::bind(&justGrad, this, std::placeholders::_1);
  Eigen::MatrixXd hess;

  computeFdHess(f, g, xc, hess, reltol, abstol);

  return hess;
}

void OptProbBase::AddConstraint(std::shared_ptr<ConstraintBase> c, bool isEqual)
{
  if (isEqual) {
    equalityConsts.Add(c);
  } else {
    inequalityConsts.Add(c);
  }
}

// the following functions extract the type of optimization problem, i.e. unconstrained, bound constrained, etc...
bool OptProbBase::isConstrained() const
{
  return !isUnconstrained();
}

bool OptProbBase::isUnconstrained() const
{
  return (equalityConsts.NumConstraints() == 0) && (inequalityConsts.NumConstraints() == 0);
}

bool OptProbBase::isBoundConstrained() const
{
  return inequalityConsts.Bounds.size() > 0;
}

bool OptProbBase::isPartlyBoundConstrained() const
{
  if (inequalityConsts.Bounds.size() > 0) {
    // check to see if all the variables have upper and lower bounds, i.e. is fully bound constrained
    std::vector<bool> isLowerBound(dim, false);
    std::vector<bool> isUpperBound(dim, false);

    for (auto i = inequalityConsts.Bounds.begin(); i < inequalityConsts.Bounds.end(); ++i) {
      if ((*i)->isLower()) {
        isLowerBound[(*i)->WhichDim()] = true;
      } else {
        isUpperBound[(*i)->WhichDim()] = true;
      }
    }

    bool allLowerBound = true;
    bool allUpperBound = true;

    for (auto i = isLowerBound.begin(); i < isLowerBound.end(); ++i) {
      allLowerBound = ((allLowerBound) && (*i));
    }
    for (auto i = isUpperBound.begin(); i < isUpperBound.end(); ++i) {
      allUpperBound = ((allUpperBound) && (*i));
    }

    // if allLowerBound
    return !(allLowerBound && allUpperBound);
  } else {
    return false;
  }
}

bool OptProbBase::isFullyBoundConstrained() const
{
  if (inequalityConsts.Bounds.size() > 0) {
    return !isPartlyBoundConstrained();
  } else {
    return false;
  }
}

bool OptProbBase::isLinearlyConstrained() const
{
  return (equalityConsts.NumLinear() > 0) || (inequalityConsts.NumLinear() > 0);
}

bool OptProbBase::isNonlinearlyConstrained() const
{
  return (equalityConsts.NumNonlinear() > 0) || (inequalityConsts.NumNonlinear() > 0);
}

bool OptProbBase::isStochastic() const
{
  return false;
}

