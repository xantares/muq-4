
#include "MUQ/Optimization/Algorithms/Newton.h"

using namespace muq::Optimization;
using namespace std;

REGISTER_OPT_DEF_TYPE(Newton)
/** parameter list constructor */
Newton::Newton(shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties) : GradLineSearch(ProbPtr,
                                                                                                          properties)
{}

/** step function, return the BFGS direction*/
Eigen::VectorXd Newton::step()
{
  Eigen::VectorXd output;

  // find the objective function and gradient
  fold = OptProbPtr->grad(xc, gc);

  // if it is, we cannot update the pproximate hessian inverse and we just use what we have:
  output = -1.0 * OptProbPtr->applyInvHess(xc, gc);
  return output;
}

