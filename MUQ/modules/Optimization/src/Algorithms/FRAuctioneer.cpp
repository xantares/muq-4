
#include "MUQ/Optimization/Algorithms/FRAuctioneer.h"

#include <iostream>

using namespace muq::Optimization;
using namespace std;


Eigen::MatrixXd BuildDistMat(const Eigen::MatrixXd& xi, const Eigen::MatrixXd& xo)
{
  int Npts = xi.cols();
  Eigen::MatrixXd aOut(xi.cols(), xo.cols());

  // initialize all the bidders
  for (int i = 0; i < Npts; ++i) {
    for (int k = 0; k < Npts; ++k) {
      aOut(i, k) = -1.0 * (xi.col(i) - xo.col(k)).squaredNorm();
    }
  }
  return aOut;
}

FRAuctioneer::FRAuctioneer(const Eigen::MatrixXd& dists) : a(dists), size(dists.cols()),
                                                           Bidder2Object(dists.cols(), -1), Object2Bidder(
                                                             dists.cols(), -1)
{
  // make sure this is a square problem
  assert(dists.cols() == dists.rows());
}

FRAuctioneer::FRAuctioneer(const Eigen::MatrixXd& xi, const Eigen::MatrixXd& xo) : FRAuctioneer(BuildDistMat(xi, xo)) {}


void FRAuctioneer::solve(boost::property_tree::ptree& params)
{
  prices = Eigen::VectorXd::Zero(a.cols());

  //profits = Eigen::VectorXd::Zero(a.cols());

  for (int i = 0; i < size; ++i) {
    Object2Bidder[i] = i;
    Bidder2Object[i] = i;
  }

  double tol   = params.get("Opt.Auction.EpsTol", 0.002);
  double eps   = params.get("Opt.Auction.MaxEps", 0.5);
  double scale = params.get("Opt.Auction.EpsScale", 0.125);

  int verbose = params.get("Verbose", 0);

  if (verbose > 0) {
    std::cout << "Solving assignment problem using the FRAuction algorithm...\n";
  }

  // keep making the bidders "approximately happy" until they are completely happy
  while (eps > tol) {
    if (verbose > 0) {
      std::cout << "\tCurrently working on epsilon = " << eps << " problem.\n";
    }

    solveEps(eps);
    eps *= scale;
  }
}

void FRAuctioneer::solve()
{
  boost::property_tree::ptree emptyTree;

  solve(emptyTree);
}

void FRAuctioneer::solve(const std::string& paramFile)
{
  boost::property_tree::ptree temp;
  boost::property_tree::read_xml(paramFile, temp);

  solve(temp);
}

void FRAuctioneer::solveEps(double eps)
{
  // add everybody to unassigned list
  UnassignedBidders.clear();
  for (int i = 0; i < size; ++i) {
    UnassignedBidders.insert(i);
  }

  // continue while all bidders until all bidders are "almost happy"
  int its = 0;
  while (UnassignedBidders.size() > 0) {
    ++its;

    bool SwapDirection = false;

    //        while(!SwapDirection){
    // //////////////////////////////////
    // Bidding phase
    // //////////////////////////////////

    // get the next unhappy bidder
    int  nsteps = its % UnassignedBidders.size();
    auto iter   = UnassignedBidders.begin();
    advance(iter, nsteps);
    int i = *iter;

    // get a new bid
    int maxInd, secInd;
    getPriceMaxes(i, maxInd, secInd);


    // ///////////////////////////////////
    // Assignment phase
    // ///////////////////////////////////

    // find the person with the current index
    int CurrObject       = Bidder2Object[i];
    int BestObjectBidder = Object2Bidder[maxInd];

    // if btemp==i, then the current bidder already has the object
    if (BestObjectBidder != i) {
      // add the previous bidder to the unassigned list
      UnassignedBidders.insert(BestObjectBidder);

      // update the assignments
      Bidder2Object[BestObjectBidder] = CurrObject;
      Bidder2Object[i]                = maxInd;
      Object2Bidder[CurrObject]       = BestObjectBidder;
      Object2Bidder[maxInd]           = i;
    } else {
      SwapDirection = true;
    }

    // bidder i is now assigned
    UnassignedBidders.erase(i);

    // if the object was previously unassigned, remove it
    //        if(UnassignedObjects.find(maxInd)!=UnassignedObjects.end())
    //            UnassignedObjects.erase(maxInd);
    //
    // update the price of the object
    double wi = a(i, secInd) - prices(secInd);
    prices[maxInd] = a(i, maxInd) - wi + eps;

    // update the profit of bidder i
    //profits[i] = a(i,maxInd) - prices[maxInd];
    //        }
    //
    //        SwapDirection= false;
    //        while(!SwapDirection){
    //            // //////////////////////////////////
    //            // Reverse Bidding phase
    //            // //////////////////////////////////
    //
    //            // grab the next unassigned object
    //            int j = *UnassignedObjects.begin();
    //
    //            // get a new bid
    //            int maxInd, secInd;
    //            getProfitMaxes(j, maxInd, secInd);
    //
    //
    //
    //            // ///////////////////////////////////
    //            // Reverse Assignment phase
    //            // ///////////////////////////////////
    //
    //            // find the person with the current index
    //            int CurrBidder = Object2Bidder[j];
    //            int BestBidderObject = Bidder2Object[maxInd];
    //
    //            if(BestBidderObject!=j){
    //                // add the previous object to the unassigned list
    //                UnassignedObjects.insert(BestBidderObject);
    //
    //                // update the assignments
    //                Bidder2Object[CurrBidder]=BestBidderObject;
    //                Bidder2Object[maxInd] = j;
    //                Object2Bidder[BestBidderObject] = CurrBidder;
    //                Object2Bidder[j] = maxInd;
    //            }else{
    //                SwapDirection = true;
    //            }
    //
    //            // object j is now assigned
    //            UnassignedObjects.erase(j);
    //
    //            // if the bidder was previously unassigned, remove it
    //            if(UnassignedBidders.find(maxInd)!=UnassignedBidders.end())
    //                UnassignedBidders.erase(maxInd);
    //
    //            // update the profit of the object
    //            profits[maxInd] += a(maxInd,j)-profits(maxInd) + a(secInd,j)-profits(secInd) + eps;
    //
    //            // update the price of bidder i
    //            prices[j] = a(maxInd,j) - profits[maxInd];
    //        }
  }
}

void FRAuctioneer::getPriceMaxes(int bidder, int& maxObj, int& secObj)
{
  // find the maximum and second largest values
  double maxVal = a(bidder, 0) - prices(0);
  double secVal = a(bidder, 1) - prices(1);

  if (maxVal > secVal) {
    maxObj = 0;
    secObj = 1;
  } else {
    std::swap<double>(maxVal, secVal);
    maxObj = 1;
    secObj = 0;
  }

  for (int j = 2; j < size; ++j) {
    double tempVal = a(bidder, j) - prices(j);

    if (tempVal > maxVal) {
      secVal = maxVal;
      maxVal = tempVal;

      secObj = maxObj;
      maxObj = j;
    } else if (tempVal > secVal) {
      secVal = tempVal;
      secObj = j;
    }
  }
}

//void FRAuctioneer::getProfitMaxes(int object, int &maxBidder, int &secBidder){
//
//
//    // find the maximum and second largest values
//    double maxVal = a(0,object)-profits(0);
//    double secVal = a(1,object)-profits(1);
//
//    if(maxVal>secVal){
//        maxBidder = 0;
//        secBidder = 1;
//    }else{
//        std::swap<double>(maxVal,secVal);
//        maxBidder = 1;
//        secBidder = 0;
//    }
//
//    for(int i=2; i<size; ++i){
//
//        double tempVal = a(i,object)-profits(i);
//
//        if(tempVal>maxVal){
//            secVal = maxVal;
//            maxVal = tempVal;
//
//            secBidder = maxBidder;
//            maxBidder = i;
//
//        }else if(tempVal>secVal){
//            secVal = tempVal;
//            secBidder = i;
//        }
//    }
//}


