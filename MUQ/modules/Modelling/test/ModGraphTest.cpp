
#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/ModPieceTemplates.h"
#include "MUQ/Modelling/ModGraphPiece.h"
#include "MUQ/Modelling/GaussianDensity.h"
#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"

using namespace muq::Modelling;
using namespace std;

// create a simple test modpiece
class testMod : public OneInputNoDerivModPiece {
public:

  testMod(int inDim, int outDim, double retValIn = 1.0) : OneInputNoDerivModPiece(inDim, outDim), retVal(retValIn) {}

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    return retVal * Eigen::VectorXd::Ones(outputSize);
  }

private:

  double retVal;
};

class testMod2 : public ModPiece {
public:

  testMod2(const Eigen::VectorXi& inDims, int outDim) : ModPiece(inDims, outDim, false, false, false, false, false) {}

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    return Eigen::VectorXd::Ones(outputSize);
  }
};

class squareMod : public OneInputFullModPiece {
public:

  squareMod(int inDim) : OneInputFullModPiece(inDim, inDim) {}

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    return input.cwiseProduct(input);
  }

  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) override
  {
    return 2.0 * sensitivity.cwiseProduct(input);
  }

  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) override
  {
    return (2.0 * input).asDiagonal();
  }

  virtual Eigen::VectorXd JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target) override
  {
    return Jacobian(input) * target;
  }

  virtual Eigen::MatrixXd HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) override
  {
    return (2.0 * sensitivity.cwiseProduct(Eigen::VectorXd::Ones(input.size()))).asDiagonal();
  }
};

class sumSquareMod : public OneInputFullModPiece {
public:

  sumSquareMod(int inDim, int outDim = 1) : OneInputFullModPiece(inDim, outDim) {}

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    return (input.cwiseProduct(input)).sum() * Eigen::VectorXd::Ones(outputSize);
  }

  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) override
  {
    Eigen::VectorXd output = Eigen::VectorXd::Zero(inputSizes[0]);

    for (int i = 0; i < outputSize; ++i) {
      output += 2.0 *input *sensitivity(i);
    }
    return output.transpose();
  }

  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) override
  {
    Eigen::MatrixXd output(outputSize, inputSizes[0]);

    for (int i = 0; i < outputSize; ++i) {
      output.row(i) = 2.0 * input;
    }
    return output;
  }

  virtual Eigen::VectorXd JacobianActionImpl(Eigen::VectorXd const& input,  Eigen::VectorXd const& target) override
  {
    return Jacobian(input) * target;
  }

  virtual Eigen::MatrixXd HessianImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) override
  {
    Eigen::MatrixXd output = Eigen::MatrixXd::Zero(inputSizes[0], inputSizes[0]);

    for (int i = 0; i < outputSize; ++i) {
      output += (sensitivity(i) * 2.0 * Eigen::VectorXd::Ones(inputSizes[0])).asDiagonal();
    }
    return output;
  }
};

class sinSumMod : public ModPiece {
public:

  sinSumMod(int dim, int numIn = 2) : ModPiece(dim * Eigen::VectorXi::Ones(numIn), dim, true, true, true, true,
                                               false)                                                                  {}

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    Eigen::VectorXd out = Eigen::VectorXd::Zero(outputSize);

    for (int i = 0; i < outputSize; ++i) {
      for (unsigned int j = 0; j < inputs.size(); ++j) {
        out[i] += sin(inputs[j][i]);
      }
    }
    return out;
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    Eigen::VectorXd out = Eigen::VectorXd::Zero(outputSize);

    for (int i = 0; i < outputSize; ++i) {
      out[i] = sensitivity(i) * cos(input[inputDimWrt][i]);
    }

    return out;
  }

  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override
  {
    // the jacobian is diagonal
    Eigen::MatrixXd output = Eigen::MatrixXd::Zero(outputSize, outputSize);

    for (int i = 0; i < outputSize; ++i) {
      output(i, i) = cos(input[inputDimWrt][i]);
    }

    return output;
  }

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override
  {
    return Jacobian(input, inputDimWrt) * target;
  }

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& inputs,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt) override
  {
    Eigen::MatrixXd hessian = Eigen::MatrixXd::Zero(outputSize, outputSize);

    for (int i = 0; i < outputSize; ++i) {
      hessian(i, i) -= sensitivity(i) * sin(inputs[inputDimWrt][i]);
    }
    return hessian;
  }
};


TEST(Modelling_ModGraph, BasicConstructionTest)
{
  // construct an empty ModGraph
  auto myGraph = make_shared<ModGraph>();

  // add a few nodes
  myGraph->AddNode(make_shared<testMod>(3, 2), "x0");
  myGraph->AddNode(make_shared<testMod>(2, 1), "x1");
  myGraph->AddNode(make_shared<testMod>(2, 1), "x2");
  myGraph->AddNode(make_shared<testMod2>(Eigen::VectorXi::Ones(2), 1), "x3");

  // make sure we have the correct number of inputs and outputs without any connections
  EXPECT_EQ(4, myGraph->NumOutputs());
  EXPECT_EQ(5, myGraph->NumInputs());

  // create an edge between the nodes
  myGraph->AddEdge("x0", "x1", 0);
  myGraph->AddEdge("x0", "x2", 0);
  myGraph->AddEdge("x1", "x3", 0);
  myGraph->AddEdge("x2", "x3", 1);

  // make sure we have the correct number of inputs and outputs with this diamond connected graph
  EXPECT_EQ(1, myGraph->NumOutputs());
  EXPECT_EQ(1, myGraph->NumInputs());
}

TEST(Modelling_ModGraph, SameNodeDeath)
{
  // construct an empty ModGraph
  auto myGraph = make_shared<ModGraph>();

  // try to add two nodes with the same name
  myGraph->AddNode(make_shared<testMod>(3, 2), "x0");
  ASSERT_DEATH(myGraph->AddNode(make_shared<testMod>(2, 1), "x0"), "(!NodeExists(name))*");
}

TEST(Modelling_ModGraph, SwapNodeTest)
{
  // construct an empty ModGraph
  auto myGraph = make_shared<ModGraph>();

  // add a base node
  myGraph->AddNode(make_shared<testMod>(3, 2, 1.0), "x");

  // make sure the base node evaluates to 1.0
  Eigen::VectorXd eval1 = myGraph->GetNodeModel("x")->Evaluate(Eigen::VectorXd::Zero(3));
  EXPECT_EQ(1.0, eval1[0]);
  EXPECT_EQ(1.0, eval1[1]);

  auto piece            = ModGraphPiece::Create(myGraph, "x");
  Eigen::VectorXd eval2 = piece->Evaluate(Eigen::VectorXd::Zero(3));
  EXPECT_EQ(1.0, eval2[0]);
  EXPECT_EQ(1.0, eval2[1]);

  piece->InvalidateCache();

  myGraph->SwapNode("x", make_shared<testMod>(3, 2, 0.5));
  eval1 = myGraph->GetNodeModel("x")->Evaluate(Eigen::VectorXd::Zero(3));
  EXPECT_EQ(0.5, eval1[0]);
  EXPECT_EQ(0.5, eval1[1]);

  //test that ModGraphPieces are also effected by swaps
  eval2 = piece->Evaluate(Eigen::VectorXd::Zero(3));
  EXPECT_EQ(0.5, eval2[0]);
  EXPECT_EQ(0.5, eval2[1]);
}


TEST(Modelling_ModGraph, BindNodeTest)
{
  // construct an empty ModGraph
  auto myGraph = make_shared<ModGraph>();

  // add a few nodes
  myGraph->AddNode(make_shared<testMod>(3, 2), "x0");
  myGraph->AddNode(make_shared<testMod>(2, 1), "x1");
  myGraph->AddNode(make_shared<testMod>(2, 1), "x2");
  myGraph->AddNode(make_shared<testMod2>(Eigen::VectorXi::Ones(2), 1), "x3");

  // make sure we have the correct number of inputs and outputs without any connections
  EXPECT_EQ(4, myGraph->NumOutputs());
  EXPECT_EQ(5, myGraph->NumInputs());

  // create an edge between the nodes
  myGraph->AddEdge("x0", "x1", 0);
  myGraph->AddEdge("x0", "x2", 0);
  myGraph->AddEdge("x1", "x3", 0);
  myGraph->AddEdge("x2", "x3", 1);

  // make sure we have the correct number of inputs and outputs with this diamond connected graph
  EXPECT_EQ(1, myGraph->NumOutputs());
  EXPECT_EQ(1, myGraph->NumInputs());

  myGraph->BindNode("x0", 0.1 * Eigen::VectorXd::Ones(2));

  EXPECT_EQ(0, myGraph->NumInputs());
  EXPECT_EQ(1, myGraph->NumOutputs());
}


TEST(Modelling_ModGraph, BindEdgeTest)
{
  // construct an empty ModGraph
  auto myGraph = make_shared<ModGraph>();

  // add a few nodes
  myGraph->AddNode(make_shared<testMod>(3, 2), "x0");
  myGraph->AddNode(make_shared<testMod>(2, 1), "x1");
  myGraph->AddNode(make_shared<testMod>(2, 1), "x2");
  myGraph->AddNode(make_shared<testMod2>(Eigen::VectorXi::Ones(2), 1), "x3");

  // make sure we have the correct number of inputs and outputs without any connections
  EXPECT_EQ(4, myGraph->NumOutputs());
  EXPECT_EQ(5, myGraph->NumInputs());

  // create an edge between the nodes
  myGraph->AddEdge("x0", "x1", 0);
  myGraph->AddEdge("x0", "x2", 0);
  myGraph->AddEdge("x1", "x3", 0);
  myGraph->AddEdge("x2", "x3", 1);

  // make sure we have the correct number of inputs and outputs with this diamond connected graph
  EXPECT_EQ(1, myGraph->NumOutputs());
  EXPECT_EQ(1, myGraph->NumInputs());

  myGraph->BindEdge("x1", 0, 0.1 * Eigen::VectorXd::Ones(2));

  EXPECT_EQ(1, myGraph->NumInputs());
  EXPECT_EQ(1, myGraph->NumOutputs());
}


TEST(Modelling_ModGraph, MultipleGraphsAndConstantTest)
{
  // construct an empty ModGraph
  auto myGraph = make_shared<ModGraph>();

  // add a few nodes
  myGraph->AddNode(make_shared<testMod>(3, 2), "x0");
  myGraph->AddNode(make_shared<testMod>(2, 1), "x1");
  myGraph->AddNode(make_shared<testMod>(2, 1), "x2");
  myGraph->AddNode(make_shared<testMod2>(Eigen::VectorXi::Ones(2), 1), "x3");

  // create an edge between the nodes
  myGraph->AddEdge("x0", "x1", 0);
  myGraph->AddEdge("x0", "x2", 0);
  myGraph->AddEdge("x1", "x3", 0);
  myGraph->AddEdge("x2", "x3", 1);

  // bind an edge
  myGraph->BindEdge("x1", 0, 0.1 * Eigen::VectorXd::Ones(2));

  EXPECT_FALSE(myGraph->isConstant("x3"));
  EXPECT_TRUE(myGraph->isConstant("x1"));
  EXPECT_FALSE(myGraph->isConstant("x2"));


  // construct an empty ModGraph
  ModGraph myGraph2;

  // add a few nodes
  myGraph2.AddNode(make_shared<testMod>(3, 2), "x0");
  myGraph2.AddNode(make_shared<testMod>(2, 1), "x1");
  myGraph2.AddNode(make_shared<testMod>(2, 1), "x2");
  myGraph2.AddNode(make_shared<testMod2>(Eigen::VectorXi::Ones(2), 1), "x3");

  // make sure we have the correct number of inputs and outputs without any connections
  EXPECT_EQ(4, myGraph2.NumOutputs());
  EXPECT_EQ(5, myGraph2.NumInputs());

  // create an edge between the nodes
  myGraph2.AddEdge("x0", "x1", 0);
  myGraph2.AddEdge("x0", "x2", 0);
  myGraph2.AddEdge("x1", "x3", 0);
  myGraph2.AddEdge("x2", "x3", 1);

  // make sure we have the correct number of inputs and outputs with this diamond connected graph
  EXPECT_EQ(1, myGraph2.NumOutputs());
  EXPECT_EQ(1, myGraph2.NumInputs());

  myGraph2.BindNode("x0", 0.1 * Eigen::VectorXd::Ones(2));

  EXPECT_EQ(0, myGraph2.NumInputs());
  EXPECT_EQ(1, myGraph2.NumOutputs());

  EXPECT_TRUE(myGraph2.isConstant("x3"));
}

TEST(Modelling_ModGraph, CutTest)
{
  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(2), "f2");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f1");
  myGraph->AddNode(make_shared<testMod>(2, 2), "x");
  myGraph->AddNode(make_shared<testMod>(1, 2), "y1");
  myGraph->AddNode(make_shared<testMod>(3, 2), "y2");

  // add edges
  myGraph->AddEdge("x", "f2", 0);
  myGraph->AddEdge("y1", "f1", 0);
  myGraph->AddEdge("y2", "f1", 1);
  myGraph->AddEdge("f1", "f2", 1);

  myGraph->BindNode("y1", 0.1 * Eigen::VectorXd::Ones(2));
  myGraph->BindNode("y2", 0.2 * Eigen::VectorXd::Ones(2));

  // the original graph still includes all 5 nodes including the constants
  EXPECT_EQ(5, myGraph->NumNodes());

  auto myGraph2 = myGraph->DependentCut("f2");

  // the DependentCut function collects constant branches into a single node and should only have 3 nodes
  EXPECT_EQ(3, myGraph2->NumNodes());
}

TEST(Modelling_ModGraph, ReplaceNodeWithInput)
{
  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(2), "f2");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f3");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f4");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f1");
  myGraph->AddNode(make_shared<testMod>(2, 2), "x");
  myGraph->AddNode(make_shared<testMod>(1, 2), "y1");
  myGraph->AddNode(make_shared<testMod>(3, 2), "y2");

  // add edges
  myGraph->AddEdge("x", "f2", 0);
  myGraph->AddEdge("y1", "f1", 0);
  myGraph->AddEdge("y2", "f1", 1);
  myGraph->AddEdge("f1", "f2", 1);
  myGraph->AddEdge("f2", "f3", 0);
  myGraph->AddEdge("y1", "f3", 1);
  myGraph->AddEdge("f1", "f4", 0);
  myGraph->AddEdge("f3", "f4", 1);

  // the original graph still includes all 7 nodes including the constants
  EXPECT_EQ(7, myGraph->NumNodes());

  auto myGraph2 = myGraph->ReplaceNodeWithInput("f1", "f4");

  // the DependentCut function collects constant branches into a single node and should only have 3 nodes
  EXPECT_EQ(6, myGraph2->NumNodes());
  EXPECT_EQ(7, myGraph->NumNodes());
}

TEST(Modelling_ModGraph, MoveEdges)
{
  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(2), "f2");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f3");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f4");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f1");
  myGraph->AddNode(make_shared<sinSumMod>(2), "t1");
  myGraph->AddNode(make_shared<sinSumMod>(2), "t2");
  myGraph->AddNode(make_shared<testMod>(2, 2), "x");
  myGraph->AddNode(make_shared<testMod>(1, 2), "y1");
  myGraph->AddNode(make_shared<testMod>(3, 2), "y2");

  // add edges
  myGraph->AddEdge("x", "f2", 0);
  myGraph->AddEdge("y1", "f1", 0);
  myGraph->AddEdge("y2", "f1", 1);
  myGraph->AddEdge("f1", "f2", 1);
  myGraph->AddEdge("f2", "f3", 0);
  myGraph->AddEdge("y1", "f3", 1);
  myGraph->AddEdge("f1", "f4", 0);
  myGraph->AddEdge("f3", "f4", 1);

  myGraph->MoveInputEdges("f3", "t1");
  myGraph->MoveOutputEdges("f2", "t2");

  // the DependentCut function collects constant branches into a single node and should only have 3 nodes
  EXPECT_EQ(7, myGraph->NumInputs());
  EXPECT_EQ(3, myGraph->NumOutputs());
}


TEST(Modelling_ModGraph, graphViz)
{
  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(2), "f2");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f1");
  myGraph->AddNode(make_shared<testMod>(2, 2), "x");
  myGraph->AddNode(make_shared<testMod>(1, 2), "y1");
  myGraph->AddNode(make_shared<testMod>(3, 2), "y2");

  // add edges
  myGraph->AddEdge("x", "f2", 0);
  myGraph->AddEdge("y1", "f1", 0);
  myGraph->AddEdge("y2", "f1", 1);
  myGraph->AddEdge("f1", "f2", 1);

  myGraph->writeGraphViz("results/tests/GraphViz/GraphTestDerivColor.pdf",ModGraph::DERIV_COLOR);
  myGraph->writeGraphViz("results/tests/GraphViz/GraphTestDerivLabel.pdf",ModGraph::DEFAULT_COLOR,true);
  myGraph->writeGraphViz("results/tests/GraphViz/GraphTestDerivColorLabel.pdf",ModGraph::DERIV_COLOR,true);
  
  myGraph->writeGraphViz("results/tests/GraphViz/GraphTest.png");
  myGraph->writeGraphViz("results/tests/GraphViz/GraphTest.jpg");
  myGraph->writeGraphViz("results/tests/GraphViz/GraphTest.tif");
  myGraph->writeGraphViz("results/tests/GraphViz/GraphTest.pdf");
  myGraph->writeGraphViz("results/tests/GraphViz/GraphTest.eps");
  myGraph->writeGraphViz("results/tests/GraphViz/GraphTest.svg");
}


TEST(Modelling_ModGraph, ConnectedTest)
{
  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(2), "f2");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f1");
  myGraph->AddNode(make_shared<testMod>(2, 2), "x");
  myGraph->AddNode(make_shared<testMod>(1, 2), "y1");
  myGraph->AddNode(make_shared<testMod>(3, 2), "y2");

  // add connectivity
  myGraph->AddEdge("x", "f2", 0);
  myGraph->AddEdge("y1", "f1", 0);
  myGraph->AddEdge("y2", "f1", 1);
  myGraph->AddEdge("f1", "f2", 1);


  EXPECT_TRUE(myGraph->NodesConnected("y1", "f2"));
  EXPECT_FALSE(myGraph->NodesConnected("y1", "x"));
}

TEST(Modelling_ModGraphPiece, BasicTest)
{
  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(2), "f2");
  myGraph->AddNode(make_shared<sinSumMod>(2), "f1");
  myGraph->AddNode(make_shared<squareMod>(2), "x");
  myGraph->AddNode(make_shared<squareMod>(2), "y1");
  myGraph->AddNode(make_shared<squareMod>(2), "y2");

  // add connectivity
  myGraph->AddEdge("x", "f2", 0);
  myGraph->AddEdge("y1", "f1", 0);
  myGraph->AddEdge("y2", "f1", 1);
  myGraph->AddEdge("f1", "f2", 1);
  myGraph->writeGraphViz("results/tests/GraphViz/BasicTest.pdf");


  myGraph->BindNode("y1", 0.1 * Eigen::VectorXd::Ones(2));
  myGraph->BindNode("y2", 0.2 * Eigen::VectorXd::Ones(2));

  auto GraphMod = ModGraphPiece::Create(myGraph, "f2");
  GraphMod->writeGraphViz("results/tests/GraphViz/BasicTestModel.pdf");

  // make sure this modpiece is the size we expect
  EXPECT_EQ(1, GraphMod->inputSizes.size());
  EXPECT_EQ(2, GraphMod->inputSizes[0]);
  EXPECT_EQ(2, GraphMod->outputSize);


  // evaluation testing
  Eigen::VectorXd input  = Eigen::VectorXd::Ones(2);
  Eigen::VectorXd output = GraphMod->Evaluate(input);

  EXPECT_DOUBLE_EQ(sin(input[0] * input[0]) + sin(sin(0.1) + sin(0.2)), output[0]);
  EXPECT_DOUBLE_EQ(sin(input[1] * input[1]) + sin(sin(0.1) + sin(0.2)), output[1]);

  // gradient testing (same as J^T*x)
  Eigen::VectorXd grad = GraphMod->Gradient(input, Eigen::VectorXd::Ones(2), 0);

  EXPECT_DOUBLE_EQ(2.0 * input[0] * cos(input[0] * input[0]), grad[0]);
  EXPECT_DOUBLE_EQ(2.0 * input[1] * cos(input[1] * input[1]), grad[1]);

  // Jacobian action testing (same as J*x)
  Eigen::VectorXd input2 = input + 1e-2 * Eigen::VectorXd::Random(2);
  Eigen::VectorXd linOut = GraphMod->JacobianAction(input, input2, 0);
  EXPECT_DOUBLE_EQ(2.0 * input[0] * cos(input[0] * input[0]) * input2[0], linOut[0]);
  EXPECT_DOUBLE_EQ(2.0 * input[1] * cos(input[1] * input[1]) * input2[1], linOut[1]);

  // Jacobian testing
  Eigen::MatrixXd jacOut = GraphMod->Jacobian(input, 0);
  EXPECT_DOUBLE_EQ(2.0 * input[0] * cos(input[0] * input[0]), jacOut(0, 0));
  EXPECT_DOUBLE_EQ(0,                                         jacOut(1, 0));
  EXPECT_DOUBLE_EQ(0,                                         jacOut(0, 1));
  EXPECT_DOUBLE_EQ(2.0 * input[1] * cos(input[1] * input[1]), jacOut(1, 1));

  // Hessian testing
  Eigen::MatrixXd hessOut = GraphMod->Hessian(input, Eigen::VectorXd::Ones(2), 0);

  EXPECT_DOUBLE_EQ(-4.0 * input[0] * input[0] * sin(input[0] * input[0]) + 2.0 * cos(input[0] * input[0]),
                   hessOut(0, 0));
  EXPECT_DOUBLE_EQ(0,
                   jacOut(1, 0));
  EXPECT_DOUBLE_EQ(0,
                   jacOut(0, 1));
  EXPECT_DOUBLE_EQ(-4.0 * input[1] * input[1] * sin(input[1] * input[1]) + 2.0 * cos(input[1] * input[1]),
                   hessOut(1, 1));
}


TEST(Modelling_ModGraphPiece, HessianDimensionChangeTest)
{
  const int inDim = 2;

  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(1), "sin");
  myGraph->AddNode(make_shared<sumSquareMod>(inDim), "square1");
  myGraph->AddNode(make_shared<sumSquareMod>(inDim), "square2");

  myGraph->AddEdge("square1", "sin", 0);
  myGraph->AddEdge("square2", "sin", 1);

  auto GraphMod = ModGraphPiece::Create(myGraph, "sin");

  Eigen::VectorXd input1 = Eigen::VectorXd::Ones(inDim);
  Eigen::VectorXd input2 = Eigen::VectorXd::Ones(inDim);

  // compute hessina
  Eigen::MatrixXd HessianOut = GraphMod->Hessian(input1, input2, Eigen::VectorXd::Ones(1), 0);

  // compute the exact solution
  const double diag0 = -4.0 * input1[0] * input1[0] * sin(input1.cwiseProduct(input1).sum()) + 2.0 * cos(input1.cwiseProduct(
                                                                                                           input1).sum());
  const double diag1 = -4.0 * input1[1] * input1[1] * sin(input1.cwiseProduct(input1).sum()) + 2.0 * cos(input1.cwiseProduct(
                                                                                                           input1).sum());
  const double offdiag = -4.0 * input1[0] * input1[1] * sin(input1.cwiseProduct(input1).sum());

  EXPECT_DOUBLE_EQ(diag0,   HessianOut(0, 0));
  EXPECT_DOUBLE_EQ(diag1,   HessianOut(1, 1));
  EXPECT_DOUBLE_EQ(offdiag, HessianOut(0, 1));
  EXPECT_DOUBLE_EQ(offdiag, HessianOut(1, 0));
}

TEST(Modelling_ModGraphPiece, HessianComplexGraphTest)
{
  const int inDim1 = 3;
  const int inDim2 = 4;
  const int outDim = 2;

  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(outDim, 4), "f1");
  myGraph->AddNode(make_shared<sumSquareMod>(inDim1, outDim), "x1");
  myGraph->AddNode(make_shared<sumSquareMod>(inDim2, outDim), "x2");
  myGraph->AddNode(make_shared<squareMod>(outDim), "y1");
  myGraph->AddNode(make_shared<squareMod>(outDim), "y2");
  myGraph->AddNode(make_shared<squareMod>(outDim), "z1");
  myGraph->AddNode(make_shared<squareMod>(outDim), "z2");

  // add connectivity
  myGraph->AddEdge("x1", "y1", 0);
  myGraph->AddEdge("x1", "y2", 0);
  myGraph->AddEdge("y1", "f1", 0);
  myGraph->AddEdge("y2", "f1", 1);

  myGraph->AddEdge("x2", "z1", 0);
  myGraph->AddEdge("x2", "z2", 0);
  myGraph->AddEdge("z1", "f1", 2);
  myGraph->AddEdge("z2", "f1", 3);
  auto GraphMod = ModGraphPiece::Create(myGraph, "f1");

  GraphMod->writeGraphViz("results/tests/GraphViz/HessianTest.pdf");

  Eigen::VectorXd input0 = Eigen::VectorXd::Random(inDim1);
  Eigen::VectorXd input1 = Eigen::VectorXd::Random(inDim2);

  std::vector<Eigen::VectorXd> inputs(2);
  inputs[0] = input0; inputs[1] = input1;

  Eigen::VectorXd sens = Eigen::VectorXd::Random(outDim);

  for (int in = 0; in < 2; ++in) {
    EXPECT_PRED_FORMAT3(muq::Utilities::MatrixApproxEqual, GraphMod->Hessian(input0,
                                                                             input1,
                                                                             sens,
                                                                             in), GraphMod->HessianByFD(inputs,
                                                                                                        sens,
                                                                                                        in), 1e-2);
  }
}

TEST(Modelling_ModGraphPiece, NodeOrdering)
{
  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(2), "f1");
  myGraph->AddNode(make_shared<squareMod>(2), "z");
  myGraph->AddNode(make_shared<squareMod>(2), "x1");

  // add connectivity
  myGraph->AddEdge("z", "f1", 0);
  myGraph->AddEdge("x1", "f1", 1);

  myGraph->writeGraphViz("results/tests/GraphViz/NodeOrderTest.pdf");

  auto GraphMod = ModGraphPiece::Create(myGraph, "f1");
  GraphMod->writeGraphViz("results/tests/GraphViz/NodeOrderPieceTest.pdf");

  std::vector<std::string> newOrder(2);
  newOrder[0] = "z";
  newOrder[1] = "x1";
  auto GraphMod2 = ModGraphPiece::Create(myGraph, "f1", newOrder);
  GraphMod->writeGraphViz("results/tests/GraphViz/NodeOrderPieceTest2.pdf");
}


TEST(Modelling_ModGraphPiece, DiamondTest)
{
  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<sinSumMod>(2), "f1");
  myGraph->AddNode(make_shared<squareMod>(2), "x");
  myGraph->AddNode(make_shared<squareMod>(2), "y1");
  myGraph->AddNode(make_shared<squareMod>(2), "y2");

  // add connectivity
  myGraph->AddEdge("x", "y1", 0);
  myGraph->AddEdge("x", "y2", 0);
  myGraph->AddEdge("y1", "f1", 0);
  myGraph->AddEdge("y2", "f1", 1);
  myGraph->writeGraphViz("results/tests/GraphViz/DiamondTest.pdf");
  auto GraphMod = ModGraphPiece::Create(myGraph, "f1");
  GraphMod->writeGraphViz("results/tests/GraphViz/DiamondPieceTest.pdf");

  // make sure this modpiece is the size we expect
  EXPECT_EQ(1, GraphMod->inputSizes.size());
  EXPECT_EQ(2, GraphMod->inputSizes[0]);
  EXPECT_EQ(2, GraphMod->outputSize);

  // evaluation testing
  Eigen::VectorXd input  = 0.5 * Eigen::VectorXd::Ones(2);
  Eigen::VectorXd output = GraphMod->Evaluate(input);

  EXPECT_DOUBLE_EQ(2.0 * sin(pow(input[0], 4.0)), output[0]);
  EXPECT_DOUBLE_EQ(2.0 * sin(pow(input[1], 4.0)), output[1]);


  // gradient testing
  Eigen::VectorXd grad = GraphMod->Gradient(input, Eigen::VectorXd::Ones(2), 0);

  EXPECT_DOUBLE_EQ(8.0 * pow(input[0], 3) * cos(pow(input[0], 4.0)), grad[0]);
  EXPECT_DOUBLE_EQ(8.0 * pow(input[1], 3) * cos(pow(input[1], 4.0)), grad[1]);

  // jacobian action testing
  Eigen::VectorXd input2 = input + 1e-2 * Eigen::VectorXd::Random(2);
  Eigen::VectorXd linOut = GraphMod->JacobianAction(input, input2, 0);
  EXPECT_DOUBLE_EQ(8.0 * pow(input[0], 3) * cos(pow(input[0], 4.0)) * input2[0], linOut[0]);
  EXPECT_DOUBLE_EQ(8.0 * pow(input[1], 3) * cos(pow(input[1], 4.0)) * input2[1], linOut[1]);

  // jacobian testing
  Eigen::MatrixXd jacOut = GraphMod->Jacobian(input, 0);
  EXPECT_DOUBLE_EQ(8.0 * pow(input[0], 3) * cos(pow(input[0], 4.0)), jacOut(0, 0));
  EXPECT_DOUBLE_EQ(0,                                                jacOut(1, 0));
  EXPECT_DOUBLE_EQ(0,                                                jacOut(0, 1));
  EXPECT_DOUBLE_EQ(8.0 * pow(input[1], 3) * cos(pow(input[1], 4.0)), jacOut(1, 1));

  // Hessian testing
  Eigen::MatrixXd hessOut = GraphMod->Hessian(input, Eigen::VectorXd::Ones(2), 0);
  const double    diag    = -32.0 *
                            pow(input[0],
                                6.0) * sin(pow(input[0], 4.0)) + 24 * pow(input[0], 2.0) * cos(pow(input[0], 4.0));

  EXPECT_DOUBLE_EQ(hessOut(0, 0), diag);
  EXPECT_DOUBLE_EQ(hessOut(1, 1), diag);
  EXPECT_DOUBLE_EQ(hessOut(1, 0), 0);
  EXPECT_DOUBLE_EQ(hessOut(0, 1), 0);
}

TEST(Modelling_ModPieceDensity, BasicCreation)
{
  auto myGraph = make_shared<ModGraph>();

  // add nodes
  myGraph->AddNode(make_shared<squareMod>(2), "x");
  auto dens = make_shared<GaussianDensity>(Eigen::VectorXd::Ones(2), .2);
  myGraph->AddNode(dens, "density");

  // add connectivity
  myGraph->AddEdge("x", "density", 0);

  auto graphDensity = ModGraphDensity::Create(myGraph, "density");

  Eigen::VectorXd x = Eigen::VectorXd::Constant(2, .3);
  
  EXPECT_NEAR(-2.678439153975245, dens->LogDensity(x),1e-7);
  EXPECT_NEAR(-4.368939153975245, graphDensity->LogDensity(x),1e-7);
}

TEST(Modelling_ModGraphPiece, UnionTest)
{
  auto a = make_shared<ModGraph>();
  auto b = make_shared<ModGraph>();
  auto sourceNode = make_shared<squareMod>(2);
  
  // add nodes
  a->AddNode(make_shared<sinSumMod>(2), "af2");
  a->AddNode(make_shared<sinSumMod>(2), "af1");
  a->AddNode(sourceNode, "x");
  a->AddNode(make_shared<squareMod>(2), "ay1");
  a->AddNode(make_shared<squareMod>(2), "ay2");

  // add connectivity
  a->AddEdge("x", "af2", 0);
  a->AddEdge("ay1", "af1", 0);
  a->AddEdge("ay2", "af1", 1);
  a->AddEdge("af1", "af2", 1);
  
    // add nodes
  b->AddNode(make_shared<sinSumMod>(2), "bf2");
  b->AddNode(make_shared<sinSumMod>(2), "bf1");
  b->AddNode(sourceNode, "x");
  b->AddNode(make_shared<squareMod>(2), "by1");
  b->AddNode(make_shared<squareMod>(2), "by2");

  // add connectivity
  b->AddEdge("x", "bf2", 0);
  b->AddEdge("by1", "bf1", 0);
  b->AddEdge("by2", "bf1", 1);
  b->AddEdge("bf1", "bf2", 1);
  
  auto unionGraph = ModGraph::FormUnion(a,b);
  unionGraph->writeGraphViz("results/tests/UnionTest.pdf");
  
  EXPECT_EQ(5, unionGraph->NumInputs()); //this tests that the "x" node is not duplicated
  EXPECT_EQ(2, unionGraph->NumOutputs()); //and we have both model outputs
}

TEST(Modelling_ModGraphPiece, UnionNameClashDeath)
{
  auto a = make_shared<ModGraph>();
  auto b = make_shared<ModGraph>();
  
  // add nodes - x is distinct but has the same name
  a->AddNode(make_shared<sinSumMod>(2), "af2");
  a->AddNode(make_shared<sinSumMod>(2), "af1");
  a->AddNode(make_shared<squareMod>(2), "x");

  b->AddNode(make_shared<squareMod>(2), "x");
  b->AddNode(make_shared<squareMod>(2), "by1");
  b->AddNode(make_shared<squareMod>(2), "by2");
  
  ASSERT_DEATH(ModGraph::FormUnion(a,b), "(result->GetNodeModel(currentName) == b->ModelGraph[v]->piece)*");
  
}

TEST(Modelling_ModGraphPiece, UnionEdgeClashDeath)
{
	//Both graphs share a node correctly, but both try to provide an input, so we don't know how to 
	//uniquely resolve it and hence assert out
  auto a = make_shared<ModGraph>();
  auto b = make_shared<ModGraph>();
  auto sourceNode = make_shared<squareMod>(2);
  
  // add nodes
  a->AddNode(make_shared<sinSumMod>(2), "af2");
  a->AddNode(make_shared<sinSumMod>(2), "af1");
  a->AddNode(sourceNode, "x");
  a->AddNode(make_shared<squareMod>(2), "ay1");
  a->AddNode(make_shared<squareMod>(2), "ay2");

  // add connectivity
  a->AddEdge("x", "af2", 0);
  a->AddEdge("ay1", "af1", 0);
  a->AddEdge("ay1", "x", 0); //clashes for the first input of x
  a->AddEdge("ay2", "af1", 1);
  a->AddEdge("af1", "af2", 1);
  
    // add nodes
  b->AddNode(make_shared<sinSumMod>(2), "bf2");
  b->AddNode(make_shared<sinSumMod>(2), "bf1");
  b->AddNode(sourceNode, "x");
  b->AddNode(make_shared<squareMod>(2), "by1");
  b->AddNode(make_shared<squareMod>(2), "by2");

  // add connectivity
  b->AddEdge("x", "bf2", 0);
  b->AddEdge("by1", "bf1", 0);
  b->AddEdge("by2", "x", 0); //clashes for the first input of x
  b->AddEdge("by2", "bf1", 1);
  b->AddEdge("bf1", "bf2", 1);
  
  ASSERT_DEATH(ModGraph::FormUnion(a,b), "(result->ModelGraph[*e_result]->GetDim() != b->ModelGraph[e]->GetDim())*");
}

TEST(Modelling_ModGraph, ReplaceNodeWithInput_duplicate_path)
{
	//Create a diamond to copy - make sure it comes out right
  auto graph = make_shared<ModGraph>();
  graph->AddNode(make_shared<VectorPassthroughModel>(1), "predecessor");
  graph->AddNode(make_shared<VectorPassthroughModel>(1), "input");
  graph->AddNode(make_shared<VectorPassthroughModel>(1), "split");
  graph->AddNode(make_shared<VectorPassthroughModel>(1), "below_split_l");  
  graph->AddNode(make_shared<VectorPassthroughModel>(1), "below_split_r");
  graph->AddNode(make_shared<sinSumMod>(1), "join");
  graph->AddNode(make_shared<VectorPassthroughModel>(1), "post_join");

  graph->AddEdge("predecessor", "input", 0);
  graph->AddEdge("input", "split", 0);
  graph->AddEdge("split", "below_split_l", 0);
  graph->AddEdge("split", "below_split_r", 0);
  graph->AddEdge("below_split_l", "join", 0);
  graph->AddEdge("below_split_r", "join", 1);
  graph->AddEdge("join", "post_join", 0);

  auto cutGraph = graph->ReplaceNodeWithInput("input", "join");
//   cutGraph->writeGraphViz("results/replace_duplicate_post.pdf");
  
  EXPECT_EQ(1, cutGraph->NumInputs());
  EXPECT_EQ(1, cutGraph->NumOutputs());
  
  EXPECT_EQ("join", cutGraph->GetOutputNodeName());
  EXPECT_EQ(vector<string>{"input"}, cutGraph->InputNames());
}
