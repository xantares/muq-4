#include <boost/property_tree/ptree.hpp>
#include "gtest/gtest.h"

#include <boost/math/constants/constants.hpp>

#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Modelling/RosenbrockRV.h"
#include "MUQ/Modelling/RosenbrockDensity.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;

TEST(RosenbrockDensityTest, DensityEvaluations) 
{
  // parameters
  const double a = 1.0;
  const double b = 4.0;

  // make a Rosenbrock random variable 
  auto dens = make_shared<RosenbrockDensity>(a, b);

  double logDens = dens->LogDensity(Eigen::Vector2d(0.0, 4.0));

  EXPECT_NEAR(logDens, -1.0*log(2.0*boost::math::constants::pi<double>()), 1.0e-8);

  Eigen::MatrixXd jac = dens->Jacobian(Eigen::Vector2d(0.0, 4.0));

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, jac.transpose(), Eigen::VectorXd::Zero(2), 1.0e-8);
  
  Eigen::MatrixXd hess = dens->Hessian(Eigen::Vector2d(0.0, 4.0), Eigen::VectorXd::Constant(1.0,1.0), 0);
  Eigen::MatrixXd trueHess(2,2);
  trueHess << -1, 0,
               0,-1;
  
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, trueHess, hess, 1.0e-8);
  
}

TEST(RosenbrockDensityTest, Sampling) 
{
  // number of samples 
  const unsigned int N = 1e5;

  // parameters
  const double a = 1.0;
  const double b = 4.0;

  // make a Rosenbrock random variable 
  auto rv = make_shared<RosenbrockRV>(a, b);

  // sample for the rosenbrock distribution
  const Eigen::MatrixXd samp = rv->Sample(N);

  // compute the mean
  const Eigen::VectorXd mean = samp.rowwise().sum() / (double)N;

  // compute the true mean 
  const Eigen::Vector2d trueMean(0.0, 8.0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, mean, trueMean, 5e-2);
}
