#include "MUQ/Modelling/python/CachedModPiecePython.h"

#ifdef MUQ_MPI
#include "MUQ/Modelling/ParallelCachedModPiece.h"
#endif


using namespace std;
namespace py = boost::python;
using namespace muq::Modelling;
using namespace muq::Utilities;

int CachedModPiece::PyCostOfEvaluations(py::list const& x)
{
  return CostOfEvaluations(GetEigenMatrix(x));
}

py::list CachedModPiece::PyFindKNearestNeighbors(py::list const& input,
                                                 unsigned const  countToFind)
{
  return GetPythonMatrix<double>(FindKNearestNeighbors(PythonListToVector(input), countToFind).transpose());
}

py::list CachedModPiece::PyFindNeighborsWithinRadius(py::list const& input,
                                                     double const    radius)
{
  return GetPythonMatrix<double>(FindNeighborsWithinRadius(PythonListToVector(input), radius).transpose());
}

void CachedModPiece::PyRemoveFromCache(py::list const& input)
{
  RemoveFromCache(PythonListToVector(input));
}

bool CachedModPiece::PyIsEntryInCacheMulti(py::list const& input)
{
  return IsEntryInCache(PythonListToVector(input));
}

bool CachedModPiece::PyIsEntryInCacheSingle(py::list const& input)
{
  return IsEntryInCache(GetEigenVector<Eigen::VectorXd>(input));
}

py::list CachedModPiece::PyGetCachePoints()
{
  return GetPythonMatrix<double>(GetCachePoints().transpose());
}

void           muq::Modelling::ExportCachedModPiece()
{
  // expose to python
  py::class_<CachedModPiece, shared_ptr<CachedModPiece>, py::bases<ModPiece>,
             boost::noncopyable> exportCachedModPiece("CachedModPiece",
                                                      py::init<shared_ptr<ModPiece> >());

  // member functions
  exportCachedModPiece.def("LoadCache", &CachedModPiece::LoadCache);
  exportCachedModPiece.def("SaveCache", &CachedModPiece::SaveCache);
  exportCachedModPiece.def("CostOfEvaluations", &CachedModPiece::PyCostOfEvaluations);
  exportCachedModPiece.def("FindKNearestNeighbors", &CachedModPiece::PyFindKNearestNeighbors);
  exportCachedModPiece.def("FindNeighborsWithinRadius", &CachedModPiece::PyFindNeighborsWithinRadius);
  exportCachedModPiece.def("RemoveFromCache", &CachedModPiece::PyRemoveFromCache);
  exportCachedModPiece.def("IsEntryInCache", &CachedModPiece::PyIsEntryInCacheMulti);
  exportCachedModPiece.def("IsEntryInCache", &CachedModPiece::PyIsEntryInCacheSingle);
  exportCachedModPiece.def("GetCachePoints", &CachedModPiece::PyGetCachePoints);
  exportCachedModPiece.def("GetNumOfEvals", &CachedModPiece::GetNumOfEvals);

  // allow conversion to ModPiece
  py::implicitly_convertible<shared_ptr<CachedModPiece>, shared_ptr<ModPiece> >();

#ifdef MUQ_MPI
  py::class_<ParallelCachedModPiece, shared_ptr<ParallelCachedModPiece>, py::bases<ModPiece>,
             boost::noncopyable> exportParallelCachedModPiece("ParallelCachedModPiece",
                                                      py::init<shared_ptr<ModPiece> >());
  py::implicitly_convertible<shared_ptr<ParallelCachedModPiece>, shared_ptr<ModPiece> >();
#endif

  
}

