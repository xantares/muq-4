#include "MUQ/Modelling/python/ModGraphOperationsPython.h"
#include "MUQ/Modelling/ModGraphOperators.h"


using namespace muq::Modelling;
using namespace std;
using namespace muq::Utilities;

///Add graph
std::shared_ptr<ModGraphPython> muq::Modelling::PyAdd(const std::shared_ptr<ModGraph>& a,
                                                      const std::shared_ptr<ModGraph>& b)
{
  return make_shared<ModGraphPython>(a + b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyAdd(const std::shared_ptr<ModPiece>& a,
                                                      const std::shared_ptr<ModGraph>& b)
{
  return make_shared<ModGraphPython>(a + b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyAdd(const std::shared_ptr<ModGraph>& a,
                                                      const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraphPython>(a + b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyAdd(const std::shared_ptr<ModPiece>& a,
                                                      const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraphPython>(a + b);
}

///Subtract graph
std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModGraph>& a,
                                                      const std::shared_ptr<ModGraph>& b)
{
  return make_shared<ModGraphPython>(a - b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModPiece>& a,
                                                      const std::shared_ptr<ModGraph>& b)
{
  return make_shared<ModGraphPython>(a - b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModGraph>& a,
                                                      const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraphPython>(a - b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModPiece>& a,
                                                      const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraphPython>(a - b);
}

///Component-wise multiply graphs
std::shared_ptr<ModGraphPython> muq::Modelling::PyMul(const std::shared_ptr<ModGraph>& a,
                                                      const std::shared_ptr<ModGraph>& b)
{
  return make_shared<ModGraphPython>(a * b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyMul(const std::shared_ptr<ModPiece>& a,
                                                      const std::shared_ptr<ModGraph>& b)
{
  return make_shared<ModGraphPython>(a * b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyMul(const std::shared_ptr<ModGraph>& a,
                                                      const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraphPython>(a * b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyMul(const std::shared_ptr<ModPiece>& a,
                                                      const std::shared_ptr<ModPiece>& b)
{
  return make_shared<ModGraphPython>(a * b);
}

///The following batch are all just linear muq::Modelling::operators, where we promote scalars to matrices or vectors

///Negate graph
std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModGraph>& a)
{
  return make_shared<ModGraphPython>(-a);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModPiece>& a)
{
  return make_shared<ModGraphPython>(-a);
}

///Add a constant
std::shared_ptr<ModGraphPython> muq::Modelling::PyAdd(const std::shared_ptr<ModGraph>& a, const double& b)
{
  return make_shared<ModGraphPython>(a + b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyAdd(const std::shared_ptr<ModPiece>& a, const double& b)
{
  return make_shared<ModGraphPython>(a + b);
}

///Subtract a constant
std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModGraph>& a, const double& b)
{
  return make_shared<ModGraphPython>(a - b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModPiece>& a, const double& b)
{
  return make_shared<ModGraphPython>(a - b);
}

///Multiply by a constant
std::shared_ptr<ModGraphPython> muq::Modelling::PyMul(const std::shared_ptr<ModGraph>& a, const double& b)
{
  return make_shared<ModGraphPython>(a * b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyMul(const std::shared_ptr<ModPiece>& a, const double& b)
{
  return make_shared<ModGraphPython>(a * b);
}

///Divide by a constant
std::shared_ptr<ModGraphPython> muq::Modelling::PyDiv(const std::shared_ptr<ModGraph>& a, const double& b)
{
  return make_shared<ModGraphPython>(a / b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyDiv(const std::shared_ptr<ModPiece>& a, const double& b)
{
  return make_shared<ModGraphPython>(a / b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyAdd(const std::shared_ptr<ModGraph>& x,  boost::python::list const& b)
{
  Eigen::VectorXd rhs = muq::Utilities::GetEigenVector<Eigen::VectorXd>(b);

  return make_shared<ModGraphPython>(x + rhs);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModGraph>& x,  boost::python::list const& b)
{
  Eigen::VectorXd rhs = muq::Utilities::GetEigenVector<Eigen::VectorXd>(b);

  return make_shared<ModGraphPython>(x - rhs);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyRSub(const std::shared_ptr<ModGraph>& b, const double& a)
{
  return make_shared<ModGraphPython>(a - b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyRSub(const std::shared_ptr<ModGraph>& b, boost::python::list const& a)
{
  Eigen::VectorXd lhs = muq::Utilities::GetEigenVector<Eigen::VectorXd>(a);

  return make_shared<ModGraphPython>(lhs - b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyRMul(const std::shared_ptr<ModGraph>& x, boost::python::list const& A)
{
  Eigen::MatrixXd lhs = muq::Utilities::GetEigenMatrix(A);

  return make_shared<ModGraphPython>(lhs * x);
}

std::shared_ptr<ModGraphPython> muq::Modelling::GraphCompose_graph_impl_py(std::shared_ptr<ModGraph> const& modToBind,
                                                                           boost::python::list              collectingVector_py,
                                                                           boost::python::list              inputOrder_py,
                                                                           boost::python::list              inputDims_py)
{
  const unsigned int N1 = boost::python::len(collectingVector_py);

  std::vector < std::shared_ptr < ModGraph >> collectingVector(N1);

  for (unsigned int i = 0; i < N1; ++i) {
    boost::python::extract < std::shared_ptr < ModGraph >> isGraph(collectingVector_py[i]);
    boost::python::extract < std::shared_ptr < ModPiece >> isPiece(collectingVector_py[i]);

    if (isGraph.check()) {
      collectingVector[i] = isGraph();
    } else if (isPiece.check()) {
      collectingVector[i] = make_shared<ModGraph>(isPiece());
    }
  }


  const unsigned int N2 = boost::python::len(inputOrder_py);
  std::vector<std::string> inputOrder(N1);

  for (unsigned int i = 0; i < N2; ++i) {
    inputOrder[i] = boost::python::extract<string>(inputOrder_py[i]);
  }

  const unsigned int N3 = boost::python::len(inputDims_py);
  std::vector<int>   inputDims(N1);

  for (unsigned int i = 0; i < N3; ++i) {
    inputDims[i] = boost::python::extract<int>(inputDims_py[i]);
  }


  return make_shared<ModGraphPython>(GraphCompose_graph_impl(modToBind, collectingVector, inputOrder, inputDims));
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyAdd(const std::shared_ptr<ModPiece>& x,  boost::python::list const& b)
{
  Eigen::VectorXd rhs = GetEigenVector<Eigen::VectorXd>(b);

  return make_shared<ModGraphPython>(x + rhs);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PySub(const std::shared_ptr<ModPiece>& x,  boost::python::list const& b)
{
  Eigen::VectorXd rhs = GetEigenVector<Eigen::VectorXd>(b);

  return make_shared<ModGraphPython>(x - rhs);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyRSub(const std::shared_ptr<ModPiece>& b, const double& a)
{
  return make_shared<ModGraphPython>(a - b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyRSub(const std::shared_ptr<ModPiece>& b, boost::python::list const& a)
{
  Eigen::VectorXd lhs = GetEigenVector<Eigen::VectorXd>(a);

  return make_shared<ModGraphPython>(lhs - b);
}

std::shared_ptr<ModGraphPython> muq::Modelling::PyRMul(const std::shared_ptr<ModPiece>& x, boost::python::list const& A)
{
  Eigen::MatrixXd lhs = GetEigenMatrix(A);

  return make_shared<ModGraphPython>(lhs * x);
}

std::shared_ptr<ModGraphPython> muq::Modelling::GraphCompose_piece_impl_py(std::shared_ptr<ModPiece> const& modToBind,
                                                                           boost::python::list              collectingVector_py)
{
  const unsigned int N = boost::python::len(collectingVector_py);

  std::vector < std::shared_ptr < ModGraph >> collectingVector(N);

  for (unsigned int i = 0; i < N; ++i) {
    boost::python::extract < std::shared_ptr < ModGraph >> isGraph(collectingVector_py[i]);
    boost::python::extract < std::shared_ptr < ModPiece >> isPiece(collectingVector_py[i]);

    if (isGraph.check()) {
      collectingVector[i] = isGraph();
    } else if (isPiece.check()) {
      collectingVector[i] = make_shared<ModGraph>(isPiece());
    }
  }
  auto graph = GraphCompose_piece_impl(modToBind, collectingVector);
  return make_shared<ModGraphPython>(graph);
}

