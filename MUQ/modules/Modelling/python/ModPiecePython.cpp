#include "MUQ/Modelling/python/ModPiecePython.h"
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/ModGraphOperators.h"
#include "MUQ/Modelling/python/ModGraphPython.h"
#include "MUQ/Modelling/python/ModGraphOperationsPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

ModPiecePython::ModPiecePython(boost::python::list const& inputSizes,
                               int const                  outputSize,
                               bool const                 hasDirectGradient,
                               bool const                 hasDirectJacobian,
                               bool const                 hasDirectJacobianAction,
                               bool const                 hasDirectHessian,
                               bool const                 isRandom,
                               string const             & name) : ModPiece(GetEigenVector<Eigen::VectorXi>(
                                                                             inputSizes), outputSize, hasDirectGradient, hasDirectJacobian, hasDirectJacobianAction, hasDirectHessian, isRandom,
                                                                           name)
{}

Eigen::VectorXd ModPiecePython::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & sensitivity,
                                             int const                           inputDimWrt)
{
  if (!hasDirectGradient) { // if there is no direct gradient implemented don't bother with vector conversion
    return this->ModPiece::GradientByFD(input, sensitivity, inputDimWrt);
  }

  boost::python::list pySens  = GetPythonVector<Eigen::VectorXd>(sensitivity);
  boost::python::list pyInput = VectorToPythonList(input);

  return GetEigenVector<Eigen::VectorXd>(PyGradientImpl(pyInput, pySens, inputDimWrt));
}

boost::python::list ModPiecePython::PyGradientImpl(boost::python::list const& input,
                                                   boost::python::list const& sens,
                                                   int const                  inputDimWrt)
{
  if (boost::python::override PyGradientImpl = this->get_override("GradientImpl")) {
    return PyGradientImpl(input, sens, inputDimWrt);               // if the user implemented
  }
  return this->ModPiece::PyGradientByFD(input, sens, inputDimWrt); // if not do FD
}

boost::python::list ModPiecePython::PyEvaluateImpl(boost::python::list const& input)
{
  return this->get_override("EvaluateImpl") (input);
}

Eigen::VectorXd ModPiecePython::EvaluateImpl(std::vector<Eigen::VectorXd> const& input)
{
  boost::python::list inputPython = VectorToPythonList(input);

  return GetEigenVector<Eigen::VectorXd>(PyEvaluateImpl(inputPython));
}

Eigen::MatrixXd ModPiecePython::JacobianImpl(vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  if (!hasDirectJacobian) { // if there is no direct Jacobian implented don't bother with vector conversion
    return this->ModPiece::AssembleJacobian(input, inputDimWrt);
  }

  boost::python::list pyInput = VectorToPythonList(input);

  return GetEigenMatrix(PyJacobianImpl(pyInput, inputDimWrt));
}

boost::python::list ModPiecePython::PyJacobianImpl(boost::python::list const& inputs, int const inputDimWrt)
{
  if (boost::python::override PyJacobianImpl = this->get_override("JacobianImpl")) {
    return PyJacobianImpl(inputs, inputDimWrt); // user implemented in python
  }

  return PyJacobianImplDefault(inputs, inputDimWrt);
}

boost::python::list ModPiecePython::PyJacobianImplDefault(boost::python::list const& inputs, int const inputDimWrt)
{
  return GetPythonMatrix(this->ModPiece::AssembleJacobian(PythonListToVector(inputs), inputDimWrt));
}

Eigen::VectorXd ModPiecePython::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                   Eigen::VectorXd const             & target,
                                                   int const                           inputDimWrt)
{
  if (!hasDirectJacobianAction) {
    return this->ModPiece::Jacobian(input, inputDimWrt) * target;
  }

  boost::python::list inputPython  = VectorToPythonList(input);
  boost::python::list targetPython = GetPythonVector<Eigen::VectorXd>(target);

  return GetEigenVector<Eigen::VectorXd>(PyJacobianActionImpl(inputPython, targetPython, inputDimWrt));
}

boost::python::list ModPiecePython::PyJacobianActionImpl(boost::python::list const& inputs,
                                                         boost::python::list const& target,
                                                         int const                  inputDimWrt)
{
  if (boost::python::override PyJacobianActionImpl = this->get_override("JacobianActionImpl")) {
    return PyJacobianActionImpl(inputs, target, inputDimWrt); // user implemented in python
  }

  return PyJacobianActionImplDefault(inputs, target, inputDimWrt);
}

boost::python::list ModPiecePython::PyJacobianActionImplDefault(boost::python::list const& inputs,
                                                                boost::python::list const& target,
                                                                int const                  inputDimWrt)
{
  return GetPythonVector<Eigen::VectorXd>(Jacobian(PythonListToVector(inputs),
                                                   inputDimWrt) * GetEigenVector<Eigen::VectorXd>(target));
}

Eigen::MatrixXd ModPiecePython::HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                            Eigen::VectorXd const             & sensitivity,
                                            int const                           inputDimWrt)
{
  if (!hasDirectHessian) { // if there is no direct gradient implemented don't bother with vector conversion
    return this->ModPiece::HessianByFD(input, sensitivity, inputDimWrt);
  }

  boost::python::list pySens  = GetPythonVector<Eigen::VectorXd>(sensitivity);
  boost::python::list pyInput = VectorToPythonList(input);

  return GetEigenMatrix(PyHessianImpl(pyInput, pySens, inputDimWrt));
}

boost::python::list ModPiecePython::PyHessianImplDefault(boost::python::list const& inputs,
                                                         boost::python::list const& sensIn,
                                                         int const                  inputDimWrt)
{
  return PyHessianByFD(inputs, sensIn, inputDimWrt);
}

boost::python::list ModPiecePython::PyHessianImpl(boost::python::list const& input,
                                                  boost::python::list const& sens,
                                                  int const                  inputDimWrt)
{
  if (boost::python::override PyHessianImpl = this->get_override("HessianImpl")) {
    return PyHessianImpl(input, sens, inputDimWrt); // if the user implemented
  }
  return PyHessianByFD(input, sens, inputDimWrt);   // if not do FD
}

boost::python::list ModPiecePython::PyGradientImplDefault(boost::python::list const& inputs,
                                                          boost::python::list const& sensIn,
                                                          int const                  inputDimWrt)
{
  return PyGradientByFD(inputs, sensIn, inputDimWrt);
}



void muq::Modelling::ExportModPiece()
{
  boost::python::class_<ModPiecePython, std::shared_ptr<ModPiecePython>, boost::noncopyable> exportModPiecePython(
    "ModPiece",
    boost::python::init<boost::python::list const&, int const, bool const, bool const, bool const, bool const,
                        bool const>());

  // another constructor
  exportModPiecePython.def(boost::python::init<boost::python::list const&, int const, bool const, bool const,
                                               bool const, bool const, bool const, std::string const&>());

  // evalaute methods
  exportModPiecePython.def("Evaluate", &ModPiece::PyEvaluate);
  exportModPiecePython.def("EvaluateImpl", boost::python::pure_virtual(&ModPiecePython::PyEvaluateImpl));

  // gradient methods
  exportModPiecePython.def("Gradient", &ModPiece::PyGradient);
  exportModPiecePython.def("GradientImpl", &ModPiecePython::PyGradientImpl, &ModPiecePython::PyGradientImplDefault);
  exportModPiecePython.def("GradientByFD", &ModPiece::PyGradientByFD);

  // jacobian methods
  exportModPiecePython.def("Jacobian", &ModPiece::PyJacobian);
  exportModPiecePython.def("JacobianImpl", &ModPiecePython::PyJacobianImpl, &ModPiecePython::PyJacobianImplDefault);
  exportModPiecePython.def("JacobianByFD", &ModPiece::PyJacobianByFD);
  
  // jacobian action methods
  exportModPiecePython.def("JacobianAction", &ModPiece::PyJacobianAction);
  exportModPiecePython.def("JacobianActionImpl",
                           &ModPiecePython::PyJacobianActionImpl,
                           &ModPiecePython::PyJacobianActionImplDefault);

  // hessian methods
  exportModPiecePython.def("Hessian", &ModPiece::PyHessian);
  exportModPiecePython.def("HessianImpl", &ModPiecePython::PyHessianImpl, &ModPiecePython::PyHessianImplDefault);
  exportModPiecePython.def("HessianByFD", &ModPiece::PyHessianByFD);

  // naming methods
  exportModPiecePython.def("GetName", &ModPiece::GetName);
  exportModPiecePython.def("SetName", &ModPiece::SetName);

  // timing methods
  exportModPiecePython.def("GetRunTime", &ModPiece::GetRunTime);
  exportModPiecePython.def("GetNumCalls", &ModPiece::GetNumCalls);
 
  // expose the 
  exportModPiecePython.def("TurnCachingOff", &ModPiece::TurnCachingOff);
  exportModPiecePython.def("TurnCachingOn", &ModPiece::TurnCachingOn);
  
  // operators
  exportModPiecePython.def("__add__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          &,
                                                                                          const std::shared_ptr<ModPiece>
                                                                                          &)>(&muq::Modelling::PyAdd));
  exportModPiecePython.def("__add__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const std::shared_ptr<ModGraph>
                                                                                          & b)>(&muq::Modelling::PyAdd));
  exportModPiecePython.def("__sub__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const std::shared_ptr<ModGraph>
                                                                                          & b)>(&muq::Modelling::PySub));
  exportModPiecePython.def("__sub__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const std::shared_ptr<ModPiece>
                                                                                          & b)>(&muq::Modelling::PySub));
  exportModPiecePython.def("__mul__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const std::shared_ptr<ModGraph>
                                                                                          & b)>(&muq::Modelling::PyMul));
  exportModPiecePython.def("__mul__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const std::shared_ptr<ModPiece>
                                                                                          & b)>(&muq::Modelling::PyMul));
  exportModPiecePython.def("__neg__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a)>(&muq::Modelling::
                                                                                                PySub));

  //constant ops
  exportModPiecePython.def("__add__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const double& b)>(&muq::
                                                                                                            Modelling::
                                                                                                            PyAdd));
  exportModPiecePython.def("__radd__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const double& b)>(&muq::
                                                                                                            Modelling::
                                                                                                            PyAdd));
  exportModPiecePython.def("__sub__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const double& b)>(&muq::
                                                                                                            Modelling::
                                                                                                            PySub));
  exportModPiecePython.def("__rsub__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & b,
                                                                                          const double& a)>(&muq::
                                                                                                            Modelling::
                                                                                                            PyRSub));
  exportModPiecePython.def("__mul__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const double& b)>(&muq::
                                                                                                            Modelling::
                                                                                                            PyMul));
  exportModPiecePython.def("__rmul__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const double& b)>(&muq::
                                                                                                            Modelling::
                                                                                                            PyMul));
  exportModPiecePython.def("__div__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & a,
                                                                                          const double& b)>(&muq::
                                                                                                            Modelling::
                                                                                                            PyDiv));


  //vector ops
  exportModPiecePython.def("__rmul__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & x,
                                                                                          const  boost::python::list& A)>(
                             &muq::Modelling::PyRMul));
  exportModPiecePython.def("__radd__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & x,
                                                                                          const boost::python::list& b)>(
                             &muq::Modelling::PyAdd));
  exportModPiecePython.def("__add__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & x,
                                                                                          const boost::python::list& b)>(
                             &muq::Modelling::PyAdd));
  exportModPiecePython.def("__sub__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & x,
                                                                                          const boost::python::list& b)>(
                             &muq::Modelling::PySub));
  exportModPiecePython.def("__rsub__",
                           static_cast<std::shared_ptr<muq::Modelling::ModGraphPython>(*)(const std::shared_ptr<ModPiece>
                                                                                          & x,
                                                                                          const boost::python::list& b)>(
                             &muq::Modelling::PyRSub));

  boost::python::def("GraphCompose", &muq::Modelling::GraphCompose_piece_impl_py);


  //will need converters from ModGraph to ModGraphPython ptrs (and pieces, I guess - hopefully no!). then convert all
  // the ops and r-ops, and that's it
  //force a few template copies of the compose operators?
  // public atribuites
  exportModPiecePython.add_property("inputSizes", &ModPiece::GetInputSizes);
  exportModPiecePython.def_readonly("outputSize", &ModPiece::outputSize);
  exportModPiecePython.def_readonly("hasDirectGradient", &ModPiece::hasDirectGradient);
  exportModPiecePython.def_readonly("hasDirectJacobian", &ModPiece::hasDirectJacobian);
  exportModPiecePython.def_readonly("hasDirectJacobianAction", &ModPiece::hasDirectJacobianAction);
  exportModPiecePython.def_readonly("hasDirectHessian", &ModPiece::hasDirectHessian);
  exportModPiecePython.def_readonly("isRandom", &ModPiece::isRandom);
  exportModPiecePython.def_readonly("FdRelTolDefault", &ModPiece::FdRelTolDefault);
  exportModPiecePython.def_readonly("FdMinTolDefault", &ModPiece::FdMinTolDefault);


  // one input adjoint
  boost::python::class_<OneInputAdjointModPiece, std::shared_ptr<OneInputAdjointModPiece>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportOneInputAdjointModPiece(
    "OneInputAdjointModPieceBase",
    boost::python::no_init);

  exportOneInputAdjointModPiece.def("Gradient", &OneInputAdjointModPiece::PyGradientDefaultInputDim);

  // one input jacobian
  boost::python::class_<OneInputJacobianModPiece, std::shared_ptr<OneInputJacobianModPiece>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportOneInputJacobianModPiece(
    "OneInputJacobianModPieceBase",
    boost::python::no_init);

  exportOneInputJacobianModPiece.def("Jacobian", &OneInputJacobianModPiece::PyJacobianDefaultInputDim);

  // one input adjoint jacobian
  boost::python::class_<OneInputAdjointJacobianModPiece, std::shared_ptr<OneInputAdjointJacobianModPiece>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportOneInputAdjointJacobianModPiece(
    "OneInputAdjointJacobianModPieceBase",
    boost::python::no_init);

  exportOneInputAdjointJacobianModPiece.def("Gradient", &OneInputAdjointJacobianModPiece::PyGradientDefaultInputDim);
  exportOneInputAdjointJacobianModPiece.def("Jacobian", &OneInputAdjointJacobianModPiece::PyJacobianDefaultInputDim);
  exportOneInputAdjointJacobianModPiece.def("JacobianAction",
                                            &OneInputAdjointJacobianModPiece::PyJacobianActionDefaultInputDim);

  // one input full
  boost::python::class_<OneInputFullModPiece, std::shared_ptr<OneInputFullModPiece>, boost::python::bases<ModPiece>,
                        boost::noncopyable> exportOneInputFullModPiece("OneInputFullModPieceBase",
                                                                       boost::python::no_init);

  exportOneInputFullModPiece.def("Gradient",  &OneInputFullModPiece::PyGradientDefaultInputDim);
  exportOneInputFullModPiece.def("Jacobian", &OneInputFullModPiece::PyJacobianDefaultInputDim);
  exportOneInputFullModPiece.def("JacobianAction", &OneInputFullModPiece::PyJacobianActionDefaultInputDim);
  exportOneInputFullModPiece.def("Hessian",   &OneInputFullModPiece::PyHessianDefaultInputDim);

  // register ModPiece pointer to boost::python
  boost::python::register_ptr_to_python<std::shared_ptr<ModPiece> >();

  // allow for conversion between wrapper and parent
  boost::python::implicitly_convertible<std::shared_ptr<ModPiecePython>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OneInputNoDerivModPiece>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OneInputJacobianModPiece>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OneInputAdjointModPiece>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OneInputAdjointJacobianModPiece>, std::shared_ptr<ModPiece> >();
  boost::python::implicitly_convertible<std::shared_ptr<OneInputFullModPiece>, std::shared_ptr<ModPiece> >();
}

