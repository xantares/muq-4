#include "MUQ/Modelling/python/PointCachePython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

PointCache::PointCache(py::list const& inputs, py::list const& outputs) :
  PointCache(GetEigenMatrix(inputs).transpose(), GetEigenMatrix(outputs).transpose()) {}

void muq::Modelling::ExportPointCache() 
{
  py::class_<PointCache, shared_ptr<PointCache>, py::bases<CachedModPiece>, boost::noncopyable> 
    exportPnt("PointCache", py::init<py::list const&, py::list const&>());

  py::implicitly_convertible<shared_ptr<PointCache>, shared_ptr<ModPiece> >();
  py::implicitly_convertible<shared_ptr<PointCache>, shared_ptr<CachedModPiece> >();
}
