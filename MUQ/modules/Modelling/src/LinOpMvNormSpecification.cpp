
#include "MUQ/Modelling/LinOpMvNormSpecification.h"

using namespace muq::Modelling;
using namespace muq::Utilities;
using namespace std;

LinOpMvNormSpecification::LinOpMvNormSpecification(const Eigen::VectorXd                                 & MeanIn,
                                                   std::shared_ptr<muq::Utilities::SymmetricOpBase> Ain,
                                                   ConstantMVNormSpecification::SpecificationMode          modeIn) :
  ConstantMVNormSpecification(MeanIn, 0),
  A(Ain), solver(std::make_shared<muq::Utilities::CG>(A))
{
    assert((modeIn == ConstantMVNormSpecification::PrecisionMatrix) || (modeIn == ConstantMVNormSpecification::CovarianceMatrix));
  covarianceMode = modeIn;
  
  logDeterminant = 0.0; // this is a hack, I should fill this in somehow
  
}

Eigen::MatrixXd LinOpMvNormSpecification::ApplyCovariance(Eigen::MatrixXd const& input) const
{
    assert(input.rows() == dim);

  Eigen::MatrixXd output(dim, input.cols());

  // apply the inverse to each column
  for (int col = 0; col < input.cols(); ++col) {
    output.col(col) = ApplyCov(input.col(col));
  }
  return output;
}

Eigen::MatrixXd LinOpMvNormSpecification::ApplyInverseCovariance(Eigen::MatrixXd const& input) const
{
    assert(input.rows() == dim);

  Eigen::MatrixXd output(dim, input.cols());

  // apply the inverse to each column
  for (int col = 0; col < input.cols(); ++col) {
    output.col(col) = ApplyCovInverse(input.col(col));
  }
  return output;
}

Eigen::MatrixXd LinOpMvNormSpecification::LinOpMvNormSpecification::ApplyCovarianceSqrt(Eigen::MatrixXd const& input)
const
{
    assert(input.rows() == dim);

  Eigen::MatrixXd output(dim, input.cols());

  // apply the inverse to each column
  for (int col = 0; col < input.cols(); ++col) {
    output.col(col) = ApplyCovSqrt(input.col(col));
  }
  return output;
}

Eigen::VectorXd LinOpMvNormSpecification::ApplyCovSqrt(Eigen::VectorXd const& input) const
{
  if (covarianceMode == ConstantMVNormSpecification::PrecisionMatrix) {
    // return A^{-0.5}*input
    return solver->samplePrec(input);
  } else if (covarianceMode == ConstantMVNormSpecification::CovarianceMatrix) {
    // return A^{0.5}*input
    return solver->sampleCov(input);
  } else {
    assert((covarianceMode == ConstantMVNormSpecification::CovarianceMatrix) ||
           ((covarianceMode == ConstantMVNormSpecification::PrecisionMatrix)));
    return Eigen::VectorXd();
  }
}

// Modified conjugate gradient method for sampling a gaussian, based on "SAMPLING GAUSSIAN DISTRIBUTIONS IN KRYLOV
// SPACES WITH CONJUGATE GRADIENTS" by Parker and Fox
Eigen::VectorXd LinOpMvNormSpecification::ApplyCovInverse(Eigen::VectorXd const& input) const
{
  if (covarianceMode == ConstantMVNormSpecification::PrecisionMatrix) {
    // return A^{-0.5}*input
    return A->apply(input);
  } else if (covarianceMode == ConstantMVNormSpecification::CovarianceMatrix) {
    return solver->solve(input);
  } else {
    assert((covarianceMode == ConstantMVNormSpecification::CovarianceMatrix) ||
           ((covarianceMode == ConstantMVNormSpecification::PrecisionMatrix)));
    return Eigen::VectorXd();
  }
}

Eigen::VectorXd LinOpMvNormSpecification::ApplyCov(Eigen::VectorXd const& input) const
{
  if (covarianceMode == ConstantMVNormSpecification::PrecisionMatrix) {
    // return A^{-0.5}*input
    return solver->solve(input);
  } else if (covarianceMode == ConstantMVNormSpecification::CovarianceMatrix) {
    return A->apply(input);
  } else {
    assert((covarianceMode == ConstantMVNormSpecification::CovarianceMatrix) ||
           ((covarianceMode == ConstantMVNormSpecification::PrecisionMatrix)));
    return Eigen::VectorXd();
  }
}

