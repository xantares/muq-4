
#include "MUQ/Modelling/ParallelCachedModPiece.h"


#include <chrono>


#include <MUQ/Utilities/EigenUtils.h>
#include <MUQ/Utilities/LogConfig.h>

#include <boost/mpi.hpp>
#include <boost/serialization/vector.hpp>


namespace mpi = boost::mpi;
using namespace muq::Modelling;
using namespace Eigen;
using namespace std;


struct SharedEvalTypes {
  static const int evaluation     = 101;
  static const int destroy        = 102;
  static const int jacobian       = 103;
  static const int jacobianAction = 104;
  static const int gradient       = 105;
  static const int hessian        = 106;
};


class SharedEvaluation {
public:

  SharedEvaluation() {}

  virtual ~SharedEvaluation() = default;

  std::vector<Eigen::VectorXd> input;

  VectorXd output;
  MatrixXd matOut;
  VectorXd sens;
  int      dim = 0;

  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version)
  {
    //just do the base object
    ar& input;
    ar& output;
    ar& matOut;
    ar& sens;
    ar& dim;
  }
};

BOOST_CLASS_EXPORT(SharedEvaluation);
BOOST_CLASS_TRACKING(SharedEvaluation, boost::serialization::track_never);

ParallelCachedModPiece::ParallelCachedModPiece(std::shared_ptr<ModPiece> sourceModPiece) : CachedModPiece(sourceModPiece)
{
  LOG(INFO) << "Creating parallel cached modpiece";
}

ParallelCachedModPiece::~ParallelCachedModPiece() {}

void BroadcastSharedEvaluation(SharedEvaluation const& eval, int type)
{
  static std::list<mpi::request> allSends;

  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);

  //don't start with rank 1, it's the job manager!
  for (int i = 1; i < worldComm->size(); ++i) {
    if (i == worldComm->rank()) {
      continue;
    }

    //hang onto the requests so that their internal buffers holding the data don't get deallocated before the send
    // happens!
    allSends.push_back(worldComm->isend(i, type, eval));
  }
}

Eigen::VectorXd ParallelCachedModPiece::EvaluateImpl(std::vector<Eigen::VectorXd> const& input)
{
  ProcessIncomingMessages();

  auto cachedResult = FetchCachedResult(input);

  if (cachedResult->GetCachedEvaluate()) {
    return *(cachedResult->GetCachedEvaluate());
  } else {
    VectorXd result = sourceModPiece->Evaluate(input);

    if (!std::isfinite(result.sum())) { //throw away the whole result if the result is infinite
      LOG(INFO) << "Result is infinite, destroying cache";
      SharedEvaluation toShare;
      toShare.input = input;

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::destroy);


      RemoveFromCache(input);
    } else {
      cachedResult->SetCachedEvaluate(result);


      SharedEvaluation toShare;
      toShare.input  = input;
      toShare.output = result;

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::evaluation);
    }
    return result;
  }
}

Eigen::VectorXd ParallelCachedModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                                     Eigen::VectorXd const             & sensitivity,
                                                     int const                           inputDimWrt)
{
  auto cachedResult = FetchCachedResult(input);

  auto cachedGradient = cachedResult->GetCachedGradient(sensitivity, inputDimWrt);

  if (cachedGradient) {
    return *cachedGradient;
  } else {
    VectorXd result = sourceModPiece->Gradient(input, sensitivity, inputDimWrt);
    if (!std::isfinite(result.sum())) { //throw away the whole result if the derivative is infinite
      LOG(INFO) << "Gradient is infinite, destroying cache";
      SharedEvaluation toShare;
      toShare.input = input;

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::destroy);


      RemoveFromCache(input);
    } else {
      cachedResult->AddCachedGradient(sensitivity, inputDimWrt, result);

      SharedEvaluation toShare;
      toShare.input  = input;
      toShare.sens   = sensitivity;
      toShare.dim    = inputDimWrt;
      toShare.output = result;

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::gradient);
    }
    return result;
  }
}

Eigen::MatrixXd ParallelCachedModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  auto cachedResult = FetchCachedResult(input);

  auto cachedJacobian = cachedResult->GetCachedJacobian(inputDimWrt);

  if (cachedJacobian) {
    return *cachedJacobian;
  } else {
    MatrixXd result = sourceModPiece->Jacobian(input, inputDimWrt);
    if (!std::isfinite(result.sum())) { //throw away the whole result if the derivative is infinite
      LOG(INFO) << "Jacobian is infinite, destroying cache";
      SharedEvaluation toShare;
      toShare.input = input;

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::destroy);


      RemoveFromCache(input);
    } else {
      cachedResult->SetCachedJacobian(inputDimWrt, result);

      SharedEvaluation toShare;
      toShare.input  = input;
      toShare.dim    = inputDimWrt;
      toShare.matOut = result;
      toShare.sens   = VectorXd::Zero(1);

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::jacobian);
    }
    return result;
  }
}

Eigen::VectorXd ParallelCachedModPiece::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                           Eigen::VectorXd const             & target,
                                                           int const                           inputDimWrt)
{
  auto cachedResult = FetchCachedResult(input);

  auto cachedJacobianAction = cachedResult->GetCachedJacobianAction(target, inputDimWrt);

  if (cachedJacobianAction) {
    return *cachedJacobianAction;
  } else {
    VectorXd result = sourceModPiece->JacobianAction(input, target, inputDimWrt);
    if (!std::isfinite(result.sum())) { //throw away the whole result if the derivative is infinite
      LOG(INFO) << "JacobianAction is infinite, destroying cache";
      SharedEvaluation toShare;
      toShare.input = input;

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::destroy);


      RemoveFromCache(input);
    } else {
      cachedResult->AddCachedJacobianAction(target, inputDimWrt, result);

      SharedEvaluation toShare;
      toShare.input  = input;
      toShare.sens   = target;
      toShare.dim    = inputDimWrt;
      toShare.output = result;

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::jacobianAction);
    }
    return result;
  }
}

Eigen::MatrixXd ParallelCachedModPiece::HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                                    Eigen::VectorXd const             & sensitivity,
                                                    int const                           inputDimWrt)
{
  auto cachedResult = FetchCachedResult(input);

  auto cachedHessian = cachedResult->GetCachedHessian(sensitivity, inputDimWrt);

  if (cachedHessian) {
    return *cachedHessian;
  } else {
    Eigen::MatrixXd result = sourceModPiece->Hessian(input, sensitivity, inputDimWrt);
    if (!std::isfinite(result.sum())) { // throw away the whole result if the hessian is infinite
      LOG(INFO) << "Hessian is infinite, destroying cache";
      SharedEvaluation toShare;
      toShare.input = input;

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::destroy);


      RemoveFromCache(input);
    } else {
      cachedResult->AddCachedHessian(sensitivity, inputDimWrt, result);

      SharedEvaluation toShare;
      toShare.input  = input;
      toShare.sens   = sensitivity;
      toShare.dim    = inputDimWrt;
      toShare.matOut = result;

      BroadcastSharedEvaluation(toShare, SharedEvalTypes::hessian);
    }
    return result;
  }
}

void ParallelCachedModPiece::ProcessIncomingMessages()
{
  unique_ptr<mpi::communicator> worldComm(new mpi::communicator);

  boost::optional<mpi::status> status;


  static std::chrono::steady_clock::time_point lastUpdate;

  std::chrono::duration<double> timeSinceLastRead = std::chrono::steady_clock::now() - lastUpdate;

  //the mpi interface is slow, so don't hit it too often
  if (timeSinceLastRead.count() > 0.01) {
    lastUpdate = std::chrono::steady_clock::now();

    //pull all the messages
    while (status = worldComm->iprobe(mpi::any_source, SharedEvalTypes::evaluation)) {
      LOG(INFO) << "reading evaluation";
      SharedEvaluation recvEval;
      worldComm->recv(status->source(), status->tag(), recvEval);
      auto cachedResult = FetchCachedResult(recvEval.input);
      cachedResult->SetCachedEvaluate(recvEval.output);
    }

    while (status = worldComm->iprobe(mpi::any_source, SharedEvalTypes::destroy)) {
      LOG(INFO) << "reading destroy";
      SharedEvaluation recvEval;
      worldComm->recv(status->source(), status->tag(), recvEval);
      RemoveFromCache(recvEval.input);
    }

    while (status = worldComm->iprobe(mpi::any_source, SharedEvalTypes::jacobian)) {
      LOG(INFO) << "reading jacobian";
      SharedEvaluation recvEval;
      worldComm->recv(status->source(), status->tag(), recvEval);
      auto cachedResult = FetchCachedResult(recvEval.input);
      cachedResult->SetCachedJacobian(recvEval.dim, recvEval.matOut);
    }

    while (status = worldComm->iprobe(mpi::any_source, SharedEvalTypes::jacobianAction)) {
      LOG(INFO) << "reading jacobianAction";
      SharedEvaluation recvEval;
      worldComm->recv(status->source(), status->tag(), recvEval);
      auto cachedResult = FetchCachedResult(recvEval.input);
      cachedResult->AddCachedJacobianAction(recvEval.sens, recvEval.dim, recvEval.output);
    }


    while (status = worldComm->iprobe(mpi::any_source, SharedEvalTypes::gradient)) {
      LOG(INFO) << "reading gradient";
      SharedEvaluation recvEval;
      worldComm->recv(status->source(), status->tag(), recvEval);
      auto cachedResult = FetchCachedResult(recvEval.input);
      cachedResult->AddCachedGradient(recvEval.sens, recvEval.dim, recvEval.output);
    }

    while (status = worldComm->iprobe(mpi::any_source, SharedEvalTypes::hessian)) {
      LOG(INFO) << "reading hessian";
      SharedEvaluation recvEval;
      worldComm->recv(status->source(), status->tag(), recvEval);
      auto cachedResult = FetchCachedResult(recvEval.input);
      cachedResult->AddCachedHessian(recvEval.sens, recvEval.dim, recvEval.matOut);
    }
  }
}

