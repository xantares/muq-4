#include "MUQ/Modelling/MultiplicationModPiece.h"

using namespace muq::Modelling;


MultiplicationModPiece::MultiplicationModPiece(const Eigen::Vector2i &inputDims) : ModPiece(inputDims, inputDims.maxCoeff(), true, true, true, true, false, "MultiplicationModPiece")
{
	assert((inputDims(0)==inputDims(1)) || (inputDims(0)==1) || (inputDims(1)==1) );
}

Eigen::VectorXd MultiplicationModPiece::EvaluateImpl(std::vector<Eigen::VectorXd> const& input){
	
	if(inputSizes(0)==1){
		return input.at(0)(0)*input.at(1);
	}else if(inputSizes(1)==1){
		return input.at(1)(0)*input.at(0);	
	}else{
	    return (input.at(0).array()*input.at(1).array()).matrix();
	}
};


Eigen::VectorXd MultiplicationModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                                     Eigen::VectorXd const             & sensitivity,
                                                     int const                           inputDimWrt){                     	
 	if(inputSizes(0)==1){
 		if(inputDimWrt==0){
 		  return sensitivity.transpose()*input.at(1);	
 		}else{
 		  return input.at(0)(0)*sensitivity;
 		}
 	}else if(inputSizes(1)==1){
 		if(inputDimWrt==0){
 		  return input.at(1)(0)*sensitivity;	
 		}else{
 		  return sensitivity.transpose()*input.at(0);
 		}	
 	}else{
 	    return (input.at(3%(inputDimWrt+2)).array()*sensitivity.array()).matrix();
 	}								
}


Eigen::MatrixXd MultiplicationModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt){
	
	if(inputSizes(0)==1){
		if(inputDimWrt==0){
		  return input.at(1);	
		}else{
		  return input.at(0)(0)*Eigen::MatrixXd::Identity(outputSize,outputSize);
		}
	}else if(inputSizes(1)==1){
		if(inputDimWrt==0){
		  return input.at(1)(0)*Eigen::MatrixXd::Identity(outputSize,outputSize);	
		}else{
		  return input.at(0);
		}	
	}else{
	    return input.at(3%(inputDimWrt+2)).asDiagonal();
	}
}


Eigen::VectorXd MultiplicationModPiece::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
	                                                       Eigen::VectorXd const             & target,
	                                                       int const                           inputDimWrt){

	if(inputSizes(0)==1){
		if(inputDimWrt==0){
		  return input.at(1)*target;	
		}else{
		  return input.at(0)(0)*target;
		}
	}else if(inputSizes(1)==1){
		if(inputDimWrt==0){
		  return input.at(1)(0)*target;	
		}else{
		  return input.at(0)*target;
		}	
	}else{
	    return (input.at(3%(inputDimWrt+2)).array()*target.array()).matrix();
	}                                                   	
}

Eigen::MatrixXd MultiplicationModPiece::HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                                    Eigen::VectorXd const             & sensitvity,
                                                    int const                           inputDimWrt){
                                                    	
														
	return Eigen::MatrixXd::Zero(inputSizes(inputDimWrt),inputSizes(inputDimWrt));
}