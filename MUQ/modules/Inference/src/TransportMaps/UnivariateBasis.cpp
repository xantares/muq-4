#include "MUQ/Inference/TransportMaps/UnivariateBasis.h"

using namespace muq::Inference;

double UnivariateBasis::Evaluate(Eigen::Ref<const Eigen::VectorXd> const& xVec) const{
  return Evaluate(xVec(dim));
};

double UnivariateBasis::Derivative(Eigen::Ref<const Eigen::VectorXd> const& xVec, const int wrt) const{
  if(wrt==dim){
    return Derivative(xVec(dim));
  }else{
    return 0.0;
  }
};

double UnivariateBasis::SecondDerivative(Eigen::Ref<const Eigen::VectorXd> const& xVec, const int wrt1, const int wrt2) const{
  if((wrt1==dim)&&(wrt2==dim)){
    return SecondDerivative(xVec(dim));
  }else{
    return 0.0;
  }
};