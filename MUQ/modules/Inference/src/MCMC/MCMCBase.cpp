
#include "MUQ/Inference/MCMC/MCMCBase.h"

#include <iostream>
#include <chrono>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Utilities/HDF5Logging.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"

using namespace std;
using boost::property_tree::ptree;

using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

///Set up for performing inference on the input density, based on the settings in the input property tree.
std::shared_ptr<MCMCBase> MCMCBase::ConstructMCMC(std::shared_ptr<AbstractSamplingProblem> density,
                                                  boost::property_tree::ptree            & properties)
{
  std::string mcmcMethod = ReadAndLogParameter<string>(properties, "MCMC.Method", "SingleChainMCMC");

  return GetFactoryMap()->at(mcmcMethod) (density, properties);
}

///As the other version, but read the property list from an xml file first.
std::shared_ptr<MCMCBase> MCMCBase::ConstructMCMC(std::shared_ptr<AbstractSamplingProblem> density, std::string xmlFile)
{
  // check to make sure the file exists
  std::ifstream ifile(xmlFile.c_str());
  ptree pt;

  if (ifile.good()) {
    ifile.close();

    // read the file
    read_xml(xmlFile, pt);
  } else {
    std::cerr << "\nWARNING: Not able to open  " << xmlFile <<
    "  please make sure the file exists.  This may cause the program to crash.\n\n";
  }

  return ConstructMCMC(density, pt);
}

muq::Modelling::EmpRvPtr MCMCBase::Sample(Eigen::VectorXd const& xStart)
{
  //prepare with start state if it's the first iteration
  if (currIteration == 1) {
    SetupStartState(xStart);
  }

  std::chrono::steady_clock::time_point startChain = std::chrono::steady_clock::now();
  for (; currIteration <= totalIterations;) {
    //make the new sample happen
    SampleOnce();

    //print as needed
    PrintProgress();
    std::chrono::duration<double> chainDuration = std::chrono::steady_clock::now() - startChain;
    if (std::chrono::duration_cast<std::chrono::seconds>(chainDuration).count() > timeLimit) {
      LOG(INFO) << "Terminating MCMC early";
      break;
    }
  }

  return GetResults();
}

#if MUQ_PYTHON == 1
std::shared_ptr<EmpiricalRandVar> MCMCBase::PySample(boost::python::list const& xStart)
{
  return Sample(GetEigenVector<Eigen::VectorXd>(xStart));
}

#endif // if MUQ_PYTHON == 1

MCMCBase::MCMCBase(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                   boost::property_tree::ptree            & properties) : samplingProblem(samplingProblem)
{
  // get total number of samples
  totalIterations = ReadAndLogParameter(properties, "MCMC.Steps", 10000);
    assert(totalIterations >= 10); //it gets cranky if you don't do 10 steps

  // get number of burn in samples
  burnInLength = ReadAndLogParameter(properties, "MCMC.BurnIn", int(totalIterations / 10));

  // get verbosity level
  verbose = ReadAndLogParameter(properties, "Verbose", 1);

  // how often to store a sample
  saveSampleFrequency = ReadAndLogParameter(properties, "MCMC.ThinChain", 1);

  timeLimit = ReadAndLogParameter(properties, "MCMC.timeLimit", timeLimit);

  if (burnInLength >= totalIterations) {
    std::cerr <<
    "\nWARNING: The number of burn-in steps is greater than the the total number of MCMC steps.  This will result in an empty posterior.\n\n";
    assert(false);
  }
}

MCMCBase::~MCMCBase() {}

void MCMCBase::PrintProgress()
{
  //subtract 1 so that 10 total steps gives 10,20,...100%, even though it counts 1..10 and this algorithm sees 2...11.
  if ((currIteration - 1) % (totalIterations / 10) == 0) {
    if (verbose > 0) {
      cout << double((currIteration - 1) * 100) / double(totalIterations) << "% Complete" << endl;
    }
    LOG(INFO) << double((currIteration - 1) * 100) / double(totalIterations) << "% Complete";
  }
}

bool MCMCBase::IsCurrentIterationSaved() const
{
  //check burn in and sample rate
  return (currIteration > burnInLength) && (currIteration % saveSampleFrequency) == 0;
}

int MCMCBase::GetDim() const
{
  return samplingProblem->samplingDim;
}

void MCMCBase::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "Total Steps", totalIterations);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "Burn In Length", burnInLength);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "Save Step Frequency", saveSampleFrequency);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "Time Limit (secs)", timeLimit);
}

