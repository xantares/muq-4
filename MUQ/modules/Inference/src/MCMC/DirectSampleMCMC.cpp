
#include "MUQ/Inference/MCMC/DirectSampleMCMC.h"

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Modelling/RandVar.h"

using namespace muq::Inference;

DirectSampleMCMC::DirectSampleMCMC(std::shared_ptr<AbstractSamplingProblem> density,
                                   boost::property_tree::ptree            & properties,
                                   std::shared_ptr<muq::Modelling::RandVar> randVar) : SingleChainMCMC(density,
                                                                                                       properties),
                                                                                       randVar(randVar)
{
  LOG(INFO) << "Using a direct sampler";
}

DirectSampleMCMC::~DirectSampleMCMC() {}

SingleChainMCMC::ProposalResult DirectSampleMCMC::MakeProposal()
{
  Eigen::VectorXd sampledState = randVar->Sample();

  proposedState = samplingProblem->ConstructState(sampledState);

  //accept the move it as long as it's valid, since it's a direct prior sample
  if (proposedState) {
    return accept;
  } else {
    return reject;
  }
}

