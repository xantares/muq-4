
#include "MUQ/Inference/MCMC/TemperedMCMC.h"

#include <iostream>

#include <boost/property_tree/ptree.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Inference/MCMC/DirectSampleMCMC.h"
#include "MUQ/Inference/ProblemClasses/GaussianFisherInformationMetric.h"
#include "MUQ/Inference/ProblemClasses/EuclideanMetric.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"

using namespace std;
using namespace Eigen;

using namespace muq::Utilities;
using namespace muq::Inference;
using namespace muq::Modelling;

REGISTER_MCMC_DEF_TYPE(TemperedMCMC) TemperedMCMC::TemperedMCMC(
  std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
  boost::property_tree::ptree                            & properties) : ParallelChainMCMC(TemperedMCMC::ConstructChildChains(
                                                                                             inferenceProblem,
                                                                                             properties),
                                                                                           inferenceProblem,
                                                                                           properties)
{
  LOG(INFO) << "Constructing Tempered MCMC";


  int numChains = childrenMCMC.size();

  //Skewed temperatures - note that ConstructChildChains replicates this computation - be careful to change both
  // together
  temperatures = VectorXd::LinSpaced(numChains, 0.0, 1.0).array().pow(5.0);
  temperatures.reverseInPlace();

  proposedSwaps = VectorXd::Zero(numChains);
  acceptedSwaps = VectorXd::Zero(numChains);
}

std::vector < std::shared_ptr < SingleChainMCMC >>
TemperedMCMC::ConstructChildChains(std::shared_ptr<muq::Inference::AbstractSamplingProblem> samplingProblem,
                                   boost::property_tree::ptree & properties)
{
  int numChains = properties.get("MCMC.ParallelChain.NumChains", 5);

  LOG(INFO) << "MCMC.ParallelChain.NumChains = " << numChains;
  assert(numChains >= 2); //need at least two chains

  std::vector < std::shared_ptr < SingleChainMCMC >> newChildrenMCMC(numChains);

  //Skewed temperatures - note that TemperedMCMC() replicates this computation - be careful to change both together
  //also, CopyStateComputations call below assumes that at(0) has temperature 1
  Eigen::VectorXd temperatures = VectorXd::LinSpaced(numChains, 0.0, 1.0).array().pow(5.0);
  temperatures.reverseInPlace();
      LOG(INFO) << "Using temperatures " << temperatures.transpose() << endl;

  //grab the type for the children, move it to the top level so it gets used.
  properties.put("MCMC.Method", properties.get("MCMC.ParallelChain.ChildMethod", "MH"));
      LOG(INFO) << "Parallel subchains of type: MCMC.ParallelChain.ChildMethod=" << properties.get(
    "MCMC.ParallelChain.ChildSampler",
    "MH");

  boost::optional<string> hdf5Basename = properties.get_optional<string>("MCMC.ChainHDF5Output");
  if (hdf5Basename) {
      LOG(INFO) << "Writing parallel children to HDF5 in variations of " << hdf5Basename;
  }

  //prepare the child chains
  for (int i = 0; i < numChains; ++i) {
    if (hdf5Basename) {
      properties.put("MCMC.ChainHDF5Output", *hdf5Basename + boost::lexical_cast<string>(i));
    }
    auto temperedProblem = make_shared<TemperedSamplingProblem>(samplingProblem, temperatures(i));

    auto inferenceProblem = dynamic_pointer_cast<InferenceProblem>(samplingProblem);

    //try to use the direct sample MCMC if it's zero temp and there's a prior RV available
    if ((temperatures(i) == 0.0) && inferenceProblem && inferenceProblem->priorRV) {
      LOG(INFO) << "Creating direct sampler ";
      auto childChainTemp = make_shared<DirectSampleMCMC>(temperedProblem, properties, inferenceProblem->priorRV);

      //copy the state computation settings
      assert(temperatures(0) == 1.0); //the next line won't work if 0 isn't the full temp, and therefore the real
                                      // sampler
      temperedProblem->CopyStateComputations(newChildrenMCMC.at(0)->samplingProblem);

      newChildrenMCMC.at(i) = dynamic_pointer_cast<SingleChainMCMC>(childChainTemp);

      assert(newChildrenMCMC.at(i)); //the child MCMC must be a single chains
    } else {                         //else just construct the child chain as ususal
      auto childChainTemp = MCMCBase::ConstructMCMC(temperedProblem, properties);

      newChildrenMCMC.at(i) = dynamic_pointer_cast<SingleChainMCMC>(childChainTemp);
      assert(newChildrenMCMC.at(i)); //the child MCMC must be a single chain
    }
  }

  return newChildrenMCMC;
}

TemperedMCMC::~TemperedMCMC() {}

void TemperedMCMC::PrintProgress()
{
  //print overall state
  MCMCBase::PrintProgress();

  //then anything more specific

  if (currIteration % (totalIterations / 10) == 0) {
    // print iteration progess to screen
    if (verbose > 1) {
      PrintChildStats();
    }
  }
}

void TemperedMCMC::PrintChildStats()
{
  int i = 0;

  for (auto chain : childrenMCMC) {
    cout << "Chain " << i << ": Acceptance: " << chain->GetAccept() <<  endl;
    LOG(INFO) << "Chain " << i << ": Acceptance: " << chain->GetAccept() << endl;

    ++i;
  }
  cout << "Swap rates: " << endl << acceptedSwaps.array() / proposedSwaps.array() << endl;
    LOG(INFO) << "Swap rates: " << endl << acceptedSwaps.array() / proposedSwaps.array() << endl;
}

muq::Modelling::EmpRvPtr TemperedMCMC::GetResults()
{
  //full temperature one
  return childrenMCMC.front()->GetResults();
}

void TemperedMCMC::ProposeSwaps()
{
  for (int k = 0; k < 3; ++k) {
    //loop over all but the last
    for (unsigned i = 0; i < childrenMCMC.size() - 1; ++i) {
      unsigned j = i + 1;
      ++proposedSwaps(i);
      ++proposedSwaps(j);
      double oldLikelihood = childrenMCMC.at(i)->currentState->GetLikelihood() +
                             childrenMCMC.at(j)->currentState->GetLikelihood();
      double newLikelihood = childrenMCMC.at(i)->currentState->GetLikelihoodOtherTemp(temperatures(j)) +
                             childrenMCMC.at(j)->currentState->GetLikelihoodOtherTemp(temperatures(i));

      double gamma = exp(newLikelihood - oldLikelihood);
      double alpha = min(1.0, gamma);

      double u = RandomGenerator::GetUniform();

      if (u < alpha) {
        ++acceptedSwaps(i);
        ++acceptedSwaps(j);
        SwapChains(i, j);
      }
    }
  }
}

void TemperedMCMC::SwapChains(unsigned i, unsigned j)
{
  ParallelChainMCMC::SwapChains(i, j); //perform the swap

  //set their temperatures correctly
  childrenMCMC.at(i)->currentState->SetTemperature(temperatures(i));
  childrenMCMC.at(j)->currentState->SetTemperature(temperatures(j));
}

