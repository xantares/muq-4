/*
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 *  USA.
 *
 *  MIT UQ Library
 *  Copyright (C) 2013 MIT
 */

#include "MUQ/Inference/MCMC/MixtureMCMC.h"

// standard library includes
#include <string>

// boost includes
#include <boost/property_tree/ptree.hpp>

// other MUQ includes
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Modelling/RandVarBase.h"


// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

REGISTER_MCMC_DEF_TYPE(MixtureMCMC)
MixtureMCMC::MixtureMCMC(std::shared_ptr<AbstractSamplingProblem> inferenceProblem,
                         boost::property_tree::ptree            & properties) : SingleChainMCMC(inferenceProblem,properties),
						 localProp(make_shared<GaussianPair>(Eigen::VectorXd::Zero(inferenceProblem->samplingDim),properties.get("MCMC.MixtureMCMC.LocalVar", 0.5))),
						 globalProp(make_shared<GaussianPair>(Eigen::VectorXd::Zero(inferenceProblem->samplingDim),properties.get("MCMC.MixtureMCMC.GlobalVar", 1.0))),
						 weight(properties.get("MCMC.MixtureMCMC.LocalWeight", 0.5))
{
	assert(weight>=0);
	assert(weight<=1.0);
	
  if (verbose > 0) {
    cout << "Constructing Gaussian Mixture  MCMC with initial local weight = " << weight << endl;
  }

  samplingProblem->SetStateComputations(false, false, false);
}

void MixtureMCMC::setLocalWeight(double w1)
{
    assert(w1>=0);
    assert(w1<=1.0);
    weight = w1;
}

/** Function performs one MCMC step using the fixed proposal and simple accept/reject stage through the
 *  Metropolis-Hastings Rule */
muq::Inference::SingleChainMCMC::ProposalResult MixtureMCMC::MakeProposal()
{
	// first, randomly select if we want to use the local or global proposal
	double u1 = RandomGenerator::GetUniform();
	bool useLocal = false;
	if(u1<weight)
		useLocal = true;
	
	Eigen::VectorXd prop;
	if(useLocal){
	    prop = currentState->state + localProp->rv->Sample();
	}else{
		prop = globalProp->rv->Sample();
	}
	

  proposedState = samplingProblem->ConstructState(prop);

  //if the proposed state does not have numeric likelihood or prior, reject it immediately
  if (!proposedState) {
    return SingleChainMCMC::ProposalResult::reject;
  }

  double forwardDens  = log(weight*exp(localProp->density->LogDensity(proposedState->state-currentState->state)) + (1-weight)*exp(globalProp->density->LogDensity(proposedState->state)));
  double backwardDens = log(weight*exp(localProp->density->LogDensity(currentState->state-proposedState->state)) + (1-weight)*exp(globalProp->density->LogDensity(currentState->state)));

  // compute the Metropolis-Hastings acceptance ratio from the log-density evaluations
  double gamma = exp((proposedState->GetDensity()) - (currentState->GetDensity()) + (backwardDens - forwardDens));

  // accept or reject the proposed sample
  double u = RandomGenerator::GetUniform();
  if (u < gamma) {
    // propMu = backMu
    return SingleChainMCMC::ProposalResult::accept;
  } else {
    return SingleChainMCMC::ProposalResult::reject;
  }
}

