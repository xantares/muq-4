#include "MUQ/Inference/python/MarginalSamplingProblemPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;

MarginalSamplingProblem::MarginalSamplingProblem(shared_ptr<Density> const& joint, shared_ptr<GaussianPair> const& pair, py::dict const& para) :
  MarginalSamplingProblem(joint, pair, PythonDictToPtree(para)) {}

MarginalSamplingProblem::MarginalSamplingProblem(shared_ptr<Density> const& joint, shared_ptr<GaussianPair> const& pair, py::dict const& para, shared_ptr<AbstractMetric> const& metricObject) :
  MarginalSamplingProblem(joint, pair, PythonDictToPtree(para), metricObject) {}

void muq::Inference::ExportMarginalSamplingProblem()
{
  py::class_<MarginalSamplingProblem, shared_ptr<MarginalSamplingProblem>, boost::noncopyable> exportMarginalSampling("MarginalSamplingProblem", py::init<shared_ptr<Density> const&, shared_ptr<GaussianPair> const&, py::dict const&>());
  
  exportMarginalSampling.def(py::init<shared_ptr<Density> const&, shared_ptr<GaussianPair> const&, py::dict const&, shared_ptr<AbstractMetric> const&>());

  exportMarginalSampling.def("GetApproximator", &MarginalSamplingProblem::GetApproximator);

  py::implicitly_convertible<shared_ptr<MarginalSamplingProblem>, shared_ptr<AbstractSamplingProblem> >();
}

