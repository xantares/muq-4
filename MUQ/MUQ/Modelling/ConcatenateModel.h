#ifndef ConcatenateExpr_h_
#define ConcatenateExpr_h_

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include "MUQ/Modelling/ModPiece.h"

namespace muq {
namespace Modelling {
/** @ingroup Modelling
 *   @class ConcatenateModel
 *   @brief Adds functionality to concatenate the output of two or more Models
 *  @details ConcatenateModel is a class used to lump expression outputs together.  It takes the input arguments, and
 *     copies them into a common vector that becomes the State.
 */
class ConcatenateModel : public ModPiece {
public:

  /** Create a concatenate expression from a vector of the input dimensions. */
  ConcatenateModel(Eigen::VectorXi const& inputSizes) : ModPiece(inputSizes,
                                                                 inputSizes.sum(), true, true, true, false,
                                                                 false)
  {}

#if MUQ_PYTHON == 1
  ConcatenateModel(boost::python::list const& inputSizes);
#endif // MUQ_PYTHON == 1


  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;

  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0) override;
};

// class ConcatenateModel
} //namesapce Modelling
} //namespace muq

#endif // ifndef ConcatenateExpr_h_
