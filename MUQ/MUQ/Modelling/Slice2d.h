
#ifndef _Slice2d_h
#define _Slice2d_h


#include "MUQ/Modelling/ModPieceTemplates.h"

namespace muq {
namespace Modelling {
/**@ingroup Modelling
 *  @class Slice2dModel
 *  @author Matthew Parno
 *  @brief Extract a subset of a 2d field unrolled in a 1d vector.
 *  @details This expression extracts a continguous section of a two dimensional array.
 */
class Slice2dModel : public OneInputFullModPiece {
public:

  /** Construct from a start index and end index, in each direction, as well as the Leading dimension of the vector,
   *  i.e. the dimension of the input in the x direction.  The starting index is inclusive but the end is not.
   */
  Slice2dModel(int inputSize, int StartIndInX, int EndIndInX, int StartIndInY, int EndIndInY, int LDAIN);

  /** Fill in the update function -- this function just copies values from the input to this expressions state. */
  virtual Eigen::VectorXd EvaluateImpl(const Eigen::VectorXd& InputVec) override;

  /** This funtion just stamps the SensIn vector into a longer vector for the input size, and returns the results in the
   *  GradVec vector.
   */
  virtual Eigen::VectorXd GradientImpl(const Eigen::VectorXd& InputVec, const Eigen::VectorXd& SensIn) override;

  virtual Eigen::MatrixXd JacobianImpl(const Eigen::VectorXd& InputVec) override;
  virtual Eigen::VectorXd JacobianActionImpl(const Eigen::VectorXd& InputVec, const Eigen::VectorXd& target) override;
  virtual Eigen::MatrixXd HessianImpl(const Eigen::VectorXd& Loc, const Eigen::VectorXd& SensIn) override;

protected:

  /** keep the start, end, and skip values */
  int StartIndX, StartIndY, EndIndX, EndIndY, LDA;
};

// Slice2d
} // namespace Modelling
} // namespace muq


#endif // ifndef _Slice2d_h
