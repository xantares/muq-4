#ifndef GAUSSIANSPECIFICATION_H_
#define GAUSSIANSPECIFICATION_H_

#include <Eigen/Core>
#include <Eigen/Dense>

#include "MUQ/Modelling/Specification.h"

namespace muq {
namespace Modelling {
/// Specification to generate Gaussian random variables and densities
class GaussianSpecification : public Specification {
public:

  enum SpecificationMode { ScaledIdentity, CovarianceMatrix, PrecisionMatrix, DiagonalCovariance, DiagonalPrecision };

  /// Construct a zero mean Gaussian with identity covarinace
  /**
   *  @param[in] dimIn The density dimension
   */
  GaussianSpecification(unsigned int const dimIn);

  /// Construct a zero mean Gaussian with identity covariance and inputs
  /**
   *  Construct a zero mean Gaussian with identity covariance.  Also have inputs.  The inputs do not nothing but in the
   *case of a conditional sometimes it is required to have the conditioned variables as input.
   *  @param[in] dimIn The density dimension
   * @param[in] inputSizes The input sizes
   */
  GaussianSpecification(unsigned int const dimIn, Eigen::VectorXi const& inputs);

  /// Construct a zero mean Gaussian with identity covariance
  /**
   *  Rather than having one state input, it has many (joint distribution).  They are concatinated into one state.
   *  @param[in] dimIn The dimension of each state
   */
  GaussianSpecification(Eigen::VectorXi const& dimIn);

  /// Construct a zero mean Gaussian with scaled identity covarinace
  /**
   *  @param[in] dimIn The density dimension
   *  @param[in] scaledCov The scaling on the covariance
   */
  GaussianSpecification(unsigned int const dimIn, double const scaledCov);

  /// Construct a nonzero mean Gaussian with scaled identity covarinace
  /**
   *  @param[in] mu The mean
   *  @param[in] scaledCov The scaling on the covariance
   */
  GaussianSpecification(Eigen::VectorXd const& mu, double const scaledCov);

  /// Construct a nonzero mean Gaussian with diagonal covarinace or precision
  /**
   *  @param[in] mu The mean
   *  @param[in] diag The diagonal of the covariance or precision matrix
   *  @param[in] mode Is the diagonal of the covariance or precision matrix?
   */
  GaussianSpecification(Eigen::VectorXd const  & mu,
                        Eigen::VectorXd const  & diag,
                        SpecificationMode const& mode = DiagonalCovariance);

  /// Construct a nonzero mean Gaussian with full covarinace or precision
  /**
   *  @param[in] mu The mean
   *  @param[in] mat The covariance or precision matrix
   *  @param[in] mode Is the matrix the covariance or precision matrix?
   */
  GaussianSpecification(Eigen::VectorXd const  & mu,
                        Eigen::MatrixXd        & mat,
                        SpecificationMode const& mode = CovarianceMatrix);


  /// Construct a nonzero mean Gaussian with full covarinace or precision
  /**
   *  Rather than having one state input, it has many (joint distribution).  They are concatinated into one state.
   *  @param[in] dimIn The dimension of each state
   *  @param[in] mu The mean
   *  @param[in] diag The diagonal of the covariance or precision matrix
   *  @param[in] mode Is the matrix the covariance or precision matrix?
   */
  GaussianSpecification(Eigen::VectorXi const  & dimIn,
                        Eigen::VectorXd const  & mu,
                        Eigen::VectorXd const  & diag,
                        SpecificationMode const& mode = DiagonalCovariance);

  /// Construct a nonzero mean Gaussian with full covarinace or precision
  /**
   *  Rather than having one state input, it has many (joint distribution).  They are concatinated into one state.
   *  @param[in] dimIn The dimension of each state
   *  @param[in] mu The mean
   *  @param[in] mat The covariance or precision matrix
   *  @param[in] mode Is the matrix the covariance or precision matrix?
   */
  GaussianSpecification(Eigen::VectorXi const  & dimIn,
                        Eigen::VectorXd const  & mu,
                        Eigen::MatrixXd        & mat,
                        SpecificationMode const& mode = CovarianceMatrix);

  /// Construct a nonzero mean conditional Gaussian given full covaraince or precision
  /**
   *  @param[in] dim The dimension of the conditional we are interested in
   *  @param[in] mu The mean of the joint Gaussian
   *  @param[in] mat The covariance or precision matrix of the joint Gaussian
   *  @param[in] mode Is the matrix the covariance or precision matrix?
   */
  GaussianSpecification(unsigned int const       dim,
                        Eigen::VectorXd const  & mu,
                        Eigen::MatrixXd        & mat,
                        SpecificationMode const& mode = CovarianceMatrix);

  GaussianSpecification(std::shared_ptr<ModGraph> const& graph,
                        SpecificationMode const        & mode,
                        std::string const              & meanName = "mean",
                        std::string const              & covName = "cov");

  /// Get the mean for this gaussian
  /**
   *  @param[in] inputs The inputs for both the mean and the covariance (or precision)
   *  \return The mean
   */
  Eigen::VectorXd Mean(std::vector<Eigen::VectorXd> const& inputs = std::vector<Eigen::VectorXd>());

  /// Apply the inverse covariance matrix (the precision matrix) to a vector
  /**
   *  Given a matrix, the inverse covariance is applied to each column
   *  @param[in] vec A matrix, the inverse covariance is applied to each column
   *  @param[in] inputs The inputs for both the mean and the covariance (or precision)
   *  \return The result of applying the inverse covariance to each column
   */
  Eigen::MatrixXd ApplyInverseCovariance(Eigen::MatrixXd const& vec,
	                                     std::vector<Eigen::VectorXd> const& inputs = std::vector<Eigen::VectorXd>());

  /// Apply the square root of the covariance matrix.
  /**
   *  @param[in] mat Apply covariance sqrt to each column
   *  @param[in] inputs The inputs for both the mean and the covariance (or precision)
   *  \return The result of apply the covariance square root to each column
   */
  Eigen::MatrixXd ApplyCovarianceSqrt(Eigen::MatrixXd const             & mat,
                                      std::vector<Eigen::VectorXd> const& inputs = std::vector<Eigen::VectorXd>());

  /// The covariance matrix log determinant
  /**
   *  @param[in] inputs The inputs for both the mean and the covariance (or precision)
   *  \return The log determinant
   */
  double LogDeterminant(std::vector<Eigen::VectorXd> const& inputs = std::vector<Eigen::VectorXd>());

  /// Can the inverse covariance matrix (precision matrix) be applied without a linear solve?
  /**
   *  \return True if the inverse covariance matrix is available, false if not.
   */
  bool HasInverseCovariance() const;

  /// Return the inverse covariance matrix
  /**
   *   This functions returns the inverse covariance matrix.  If the covariance matrix is given, this function will compute the inverse.
   *  @param[in] inputs The inputs for both the mean and the covariance (or precision)
   *  \return The inverse covariance matrix (the precision matrix)
   */
  Eigen::MatrixXd GetPrecisionMatrix(std::vector<Eigen::VectorXd> const& inputs = std::vector<Eigen::VectorXd>());
  
  /// Return the covariance matrix
  /**
   *   This functions returns the covariance matrix.  If the precision matrix is given instead, this function will evaluate the matrix inverse.
   *  @param[in] inputs The inputs for both the mean and the covariance (or precision)
   *  \return The covariance matrix
   */
  Eigen::MatrixXd GetCovarianceMatrix(std::vector<Eigen::VectorXd> const& inputs = std::vector<Eigen::VectorXd>());
  
  /** Apply the covariance matrix to a given matrix
   *  @param[in] x The matrix we want to apply the covariance matrix to.
   *  @return Sigma*x where Sigma is the covariance
   */
  Eigen::MatrixXd ApplyCovariance(Eigen::MatrixXd const& x, std::vector<Eigen::VectorXd> const& inputs = std::vector<Eigen::VectorXd>());
  
private:

  static std::shared_ptr<ModGraph> ScaledCovGraph(unsigned int const dimIn, double const scaledCov = 1.0);

  static std::shared_ptr<ModGraph> ScaledCovNonzeroMeanGraph(Eigen::VectorXd const mu, double const scaledCov);

  static std::shared_ptr<ModGraph> DiagonalGraph(Eigen::VectorXd const  & mu,
                                                 Eigen::VectorXd const  & diag,
                                                 SpecificationMode const& mode = DiagonalCovariance);

  static std::shared_ptr<ModGraph> FullGraph(Eigen::VectorXd const  & mu,
                                             Eigen::MatrixXd        & mat,
                                             SpecificationMode const& mode = CovarianceMatrix);

  static std::shared_ptr<ModGraph> ConditionalGraph(unsigned int const       dim,
                                                    Eigen::VectorXd const  & mu,
                                                    Eigen::MatrixXd        & mat,
                                                    SpecificationMode const& mode = CovarianceMatrix);

  /// Compute the Cholesky decomposition of covariance or precision matrix
  /**
   *  @param[in] inputs The inputs for only the covariance (or precision)
   */
  void ComputeCholesky(std::vector<Eigen::VectorXd> const& inputs = std::vector<Eigen::VectorXd>());

  /// Make a vector of input names
  /**
   *  @param[in] meanName The name of the mean
   *  @param[in] covName The name of the covariance
   */
  static std::vector<std::string> MakeNameVector(std::string const& meanName, std::string const& covName);

  /// Specification mode determines how the covariance (or precision) matrix is stored.
  const SpecificationMode mode;

  /// The Cholesky decomposision of the covariance or precision matrix
  /**
   *  Only necessary for GaussianSpecification::CovarianceMatrix or GaussianSpecification::PrecisionMatrix mode.  The
   *decomposition is precomputed if the muq::Modelling::ModGraphPiece has no inputs.
   */
  Eigen::MatrixXd cholDecomp;
  Eigen::LLT<Eigen::MatrixXd> CholSolver;
  
  bool choleskyComputed = false;
};
} // namespace Modelling
} // namespace muq

#endif // ifndef GAUSSIANSPECIFICATION_H_
