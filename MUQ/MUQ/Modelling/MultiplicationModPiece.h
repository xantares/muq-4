
#ifndef MultiplicationPiece_h_
#define MultiplicationPiece_h_

#include "MUQ/Modelling/ModPiece.h"




namespace muq {
namespace Modelling {
	

    /** @class MultiplicationModPiece
	 *  @ingroup Modelling
	 *  @brief Multplies a vector by a scalar, or componentwise multiplication of two vectors.
	 *  @details This child of ModPiece allows us to multiply a vector by a scalar, a scalar by a vector, or to perform componentwise multiplication of two vectors.
	 *           The sizes of the inputs are set in the constructor.  An assert will be thrown when the input sizes are not the same AND one of the input sizes is not equal to one.  Assume the first input is a scalar \f$a\f$ and the second input is a vector \f$V\f$.  This ModPiece then returns \f$aV\f$.  Similarly, if the first input is a vector \f$Y\f$ and the second input is a scalar \f$b\f$, then this ModPiece returns \f$Yb\f$.  If the first input is \f$Y\f$ and the second input is \f$V\f$, this ModPiece returns a vector \f$Z\f$ where component \f$i\f$ of \f$Z\f$ is given by \f$Z_i = Y_iV_i\f$.
	 */ 
	class MultiplicationModPiece : public ModPiece{
	
	public:
	  MultiplicationModPiece(const Eigen::Vector2i &inputDims);
	  
	  virtual ~MultiplicationModPiece() = default;
		
	private:
	
	  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override;
							
	  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
	                                       Eigen::VectorXd const             & sensitivity,
	                                       int const                           inputDimWrt) override;

	  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;

	  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
	                                             Eigen::VectorXd const             & target,
	                                             int const                           inputDimWrt = 0) override;

	  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
	                                      Eigen::VectorXd const             & sensitvity,
	                                      int const                           inputDimWrt) override;			 									 
	};
	
	
}//namespace Modelling
}// namespace muq


#endif //MultiplicationPiece_h_