
#ifndef _MeshEleObserver_h
#define _MeshEleObserver_h

#include <vector>

#include <Eigen/Core>

#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Utilities/mesh/Mesh.h"

namespace muq {
namespace Modelling {
/** \author Matthew Parno
 *   \class MeshEleObserver
 *   \brief Observation model for arbitrary mesh
 *  This class provides a way to evaluate a spatial field defined on a mesh.  This class (as well as the mesh class)
 * will
 *     eventually be extended to handle general unstructure meshes with shape functions describing the geometry of each
 *     element.  However, currently this class only supports p_0 (i.e. elementwise constant) fields and p_1 (i.e. linear
 *     or bilinear nodal basis functions).  The polynomial order must be passed to the observer constructor.  Once
 *     constructed with an appropriate porder, mesh, and observation locations, this class will take a vector describing
 *     the basis function weights and compute the value of the field at the observation locations.
 */
template<unsigned int dim>
class MeshObserver : public ModPiece<> {
public:

  /** Construct the observer using a perscribed polynomial order and pescribed mesh.
   *  @param[in] porder (p=0 for elementwise quantities and p=1 for linear interpolation based on nodal values)
   *  @param[in] MeshPtr A shared pointer to the mesh.
   *  @param[in] ObsLocs A Nxdim matrix holding the observation points.  Each row corresponds to an observation.
   */
  MeshObserver(unsigned int porder,
               const std::shared_ptr < muq::utilities::Mesh < dim >>& MeshPtr,
               const Eigen::Matrix<double, Eigen::Dynamic, dim>& ObsLocs);

  /** Update the state by extracting and possibly weighting the entries of VectorIn
   *  @param[in] VectorIn The spatial field we wish to "observe"
   */
  void UpdateBase(const Eigen::VectorXd& VectorIn);

  /** Evaluate the gradient of the "observed" values to the entries in the input matrix.
   *  @param[in] VectorIn The spatially distributed field we are "observing"
   *  @param[in] SensIn The sensitivity of some objective to the output of this ModPiece
   *  @param[out] GradVec The computed gradient.
   *  @param[in] dim Must be zero for this class.
   */
  void GradBase(const Eigen::VectorXd& VectorIn, const Eigen::VectorXd& SensIn, Eigen::VectorXd& GradVec, int dimIn);

private:

  // for each observaton, keep a list of the entries in the input vector that we need to extract.
  std::vector < std::vector < unsigned int >> Nodes;

  // also for each observation, keep a list of weights for the input entries -- allows for linear interpolation
  std::vector<Eigen::VectorXd> Weights;
};
} // namespace Modelling
} // namespace muq

#endif // ifndef _MeshEleObserver_h
