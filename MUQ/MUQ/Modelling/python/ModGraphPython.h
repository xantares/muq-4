#ifndef MODGRAPHPYTHON_H_
#define MODGRAPHPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/python/ModPiecePython.h"
#include "MUQ/Modelling/Density.h"

namespace muq {
namespace Modelling {
/// A python wrapper around muq::Modelling::ModGraph
class ModGraphPython : public ModGraph, public boost::python::wrapper<ModGraph> {
public:

  ModGraphPython() = default;

  ModGraphPython(std::shared_ptr<ModPiece> inputPiece) : ModGraph(inputPiece) {}

  virtual ~ModGraphPython() = default;

  ///Essentially a copy constructor, copies the graph into this wrapper
  ModGraphPython(std::shared_ptr<ModGraph> base_ptr);

  /// Allow the user to pass a model node via a boost shared pointer

  /**
   * Override a python version of this to use the default argument for the "newName" argument
   */
  void PySwapNode(std::string const& name, std::shared_ptr<ModPiece> const& input);

  /// Bind node python wrapper
  void PyBindNode(std::string const& name, boost::python::list const& in);

  /// Bind edge python wrapper
  void PyBindEdge(std::string const& name, int inputDim,
                  boost::python::list const& in);

  /// Python only needs to know about the isCostant(string)
  bool                                  PyIsConstant(std::string const& name);

  /// Same as ModGraph::DependentCut but returns a pointer, which is required by boost.python
  std::shared_ptr<ModGraphPython>       PyDependentCut(const std::string& nameOut);

  /// Python only needs to know about NodesConnected(string,string)
  bool                                  PyNodesConnected(const std::string& name1, const std::string& name2) const;

  static std::shared_ptr<ModGraphPiece> PyConstructModPiece(std::shared_ptr<ModGraph> graph,
                                                            const std::string       & outNode);
  static std::shared_ptr<ModGraphPiece> PyConstructModPieceOrder(std::shared_ptr<ModGraph>  graph,
                                                                 const std::string        & outNode,
                                                                 const boost::python::list& inputOrder);

  static std::shared_ptr<ModPieceDensity> PyConstructDensity(std::shared_ptr<ModGraph> graph,
							     const std::string       & outNode);
  static std::shared_ptr<ModPieceDensity> PyConstructDensityOrder(std::shared_ptr<ModGraph>  graph,
								  const std::string        & outNode,
								  const boost::python::list& inputOrder);

  void PyWriteGraphViz0(const std::string& filename) const;
  void PyWriteGraphViz1(const std::string& filename,
                        colorOptionsType   colorOption) const;
  void PyWriteGraphViz2(const std::string& filename,
                        colorOptionsType   colorOption,
                        bool               addDerivLabel) const;

private:
};

void ExportModGraph();
} // namespace modelling
} // namespace muq

#endif // ifndef MODGRAPHPYTHON_H_
