
#ifndef VECTORPASSTHROUGHMODELPYTHON_H_
#define VECTORPASSTHROUGHMODELPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Modelling/VectorPassthroughModel.h"

namespace muq {
namespace Modelling {
inline void ExportVectorPassthroughModel()
{
  boost::python::class_<VectorPassthroughModel, std::shared_ptr<VectorPassthroughModel>,
                        boost::python::bases<OneInputFullModPiece>, boost::noncopyable> exportVecPass(
    "VectorPassthroughModel",
    boost::python::init<int const>());

  boost::python::implicitly_convertible<std::shared_ptr<VectorPassthroughModel>, std::shared_ptr<ModPiece> >();
  
    boost::python::class_<VectorPassthroughDensity, std::shared_ptr<VectorPassthroughDensity>,
                        boost::python::bases<OneInputFullModPiece>, boost::noncopyable> exportVecPassDens(
    "VectorPassthroughDensity",
    boost::python::init<>());

  boost::python::implicitly_convertible<std::shared_ptr<VectorPassthroughDensity>, std::shared_ptr<Density> >();
  boost::python::implicitly_convertible<std::shared_ptr<VectorPassthroughDensity>, std::shared_ptr<ModPiece> >();
  
}
} // namespace Modelling
} // namespace muq

#endif // ifndef VECTORPASSTHROUGHMODELPYTHON_H_
