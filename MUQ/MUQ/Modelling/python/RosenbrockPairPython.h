#ifndef ROSENBROCKPAIRPYTHON_H_
#define ROSENBROCKPAIRPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/RosenbrockPair.h"

namespace muq {
  namespace Modelling {

    class RosenbrockPairPython : public RosenbrockPair, public boost::python::wrapper<RosenbrockPair> {
    public:
      
      RosenbrockPairPython(double const a, double const b);

      std::shared_ptr<RosenbrockDensity> GetDensity() const;

      std::shared_ptr<RosenbrockRV> GetRV() const;

    private:
    };

    void ExportRosenbrockPair();
  } // namespace Modelling 
} // namespace muq

#endif
