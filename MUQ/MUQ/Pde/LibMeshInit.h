#ifndef MUQLIBMESHCONFIG_H_
#define MUQLIBMESHCONFIG_H_

#include <memory>

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include "libmesh/libmesh.h"

namespace muq {
  namespace Pde {
    /// Initialize libMesh
    struct LibMeshInit {
    public:
      /// Initalize libMesh
      /**
	 @param[in] argc Input to executable 
	 @param[in] argv Input to executable 
       */
      LibMeshInit(int argc, char **argv);

#if MUQ_PYTHON == 1
      LibMeshInit(boost::python::list const& pyArgv);
#endif // MUQ_PYTHON == 1

      /// Destructor needs to be implemented to prevent memory leaks
      ~LibMeshInit();

      /// Libmesh initializer
      static std::shared_ptr<libMesh::LibMeshInit> libmeshInit;
    };
  }
}

#endif
