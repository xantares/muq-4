
#ifndef TransportMap_h_
#define TransportMap_h_

// standard library includes
#include <vector>
#include <memory>
#include <utility> // needed for std::pair

// Eigen includes
#include <Eigen/Core>

// other MUQ includes
#include "MUQ/Modelling/ModPieceTemplates.h"

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

/** @defgroup TransportMaps
 @ingroup Inference
 @brief Tools for constructing and using lower triangular transport maps.
 @details
 */

namespace muq {
  namespace Inference {
    
    class MapFactory;
    class MapUpdateManager;
    
    /** @class TransportMap
     @ingroup TransportMaps
     @brief Class defining lower triangular transport maps using a multivariate polynomial basis.
     */
    class TransportMap : public muq::Modelling::OneInputJacobianModPiece {
      friend class MapUpdateManager;
      friend class MapFactory;
      
    public:
      
      TransportMap(int inputDim,
                   const std::vector<Eigen::VectorXd>&                                coeffsIn);
      
      // default virtual destructor
      virtual ~TransportMap() = default;
      
      // split this map into two maps
      std::pair<std::shared_ptr<TransportMap>, std::shared_ptr<TransportMap> > Split(int splitDim) const{return make_pair(segment(0,splitDim),segment(splitDim,outputSize-splitDim));}
      
      // grab a segment of the map (based on the output dimension)
      virtual std::shared_ptr<TransportMap> segment(int startDim, int length) const = 0;
      std::shared_ptr<TransportMap> head(int length) const{return segment(0, length);}
      std::shared_ptr<TransportMap> tail(int length) const{return segment(outputSize - length, length);}
      
      // evaluate the inverse map using either Newton's method or a bisection method build from Sturm sequences (Default)
      virtual Eigen::VectorXd EvaluateInverse(Eigen::VectorXd const& input, Eigen::VectorXd const& x0) const = 0;
      
      
      virtual std::shared_ptr<TransportMap> CreateFixedMap(int startInd, Eigen::VectorXd const& fixedInputIn) const = 0;
      
      
      virtual double GetSecondDerivative(Eigen::VectorXd const& input,
                                         int                    outputDim,
                                         int                    wrt1,
                                         int                    wrt2) const = 0;
      
      
      
      const std::vector<Eigen::VectorXd>& GetCoeffs() const{return coeffs;}
      
      void SetCoeffs(std::vector<Eigen::VectorXd> const& coeffsIn);
      
      int GetNumTerms(int dim){return coeffs.at(dim).size();}
      
      //#if MUQ_PYTHON == 1
      //  static std::shared_ptr<TransportMap> PyBuildSampsToStdSerial0(boost::python::list const& pySamps,
      //                                                                boost::python::list const& pyBases,
      //                                                                boost::python::list        pyOptLevelsIn,
      //                                                                boost::python::list        pyCoeffGuess);
      //
      //  static std::shared_ptr<TransportMap> PyBuildSampsToStdSerial1(boost::python::list const& pySamps,
      //                                                                boost::python::list const& pyBases,
      //                                                                boost::python::list        pyOptLevelsIn);
      //
      //  static std::shared_ptr<TransportMap> PyBuildSampsToStdSerial2(boost::python::list const& pySamps,
      //                                                                boost::python::list const& pyBases);
      //
      //  static std::shared_ptr<TransportMap> PyCreateIdentity(boost::python::list const& bases);
      //
      //  boost::python::list                  PyJacobian(boost::python::list const& inputs);
      //
      //  boost::python::list                  PySplit(int splitDim) const;
      //
      //  boost::python::list                  PyEvaluateInverse(boost::python::list const& input,
      //                                                         boost::python::list const& x0) const;
      //
      //#endif // if MUQ_PYTHON == 1
      
    protected:
      
      virtual void FillForConstruction(const Eigen::Ref<const Eigen::MatrixXd>& newSamps,
                                       std::vector < std::shared_ptr < Eigen::MatrixXd >> &fs,
                                       std::vector < std::shared_ptr < Eigen::MatrixXd >> &gs,
                                       int oldNumSamps,
                                       int newNumSamps) const =0;
      
      virtual Eigen::MatrixXd GetBasisValues(const Eigen::Ref<const Eigen::MatrixXd>& samps, int dim) const = 0;
      virtual Eigen::MatrixXd GetDerivValues(const Eigen::Ref<const Eigen::MatrixXd>& samps, int dim) const = 0;
      
      /// coefficients for the expansion, one vector for each output dimension
      std::vector<Eigen::VectorXd> coeffs;
      
      bool isLowerTriangular;
      
      virtual Eigen::MatrixXd EvaluateMultiImpl(Eigen::MatrixXd const& input) = 0;
      virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override{return EvaluateMultiImpl(input);};
      virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) = 0;
      
      //#if MUQ_PYTHON == 1
      //  static std::vector<BasisSet> GetBasisSetVec(boost::python::list const& pyBasis);
      //
      //#endif // if MUQ_PYTHON == 1
    }; // class TransportMap
    
    //#if MUQ_PYTHON == 1
    //void ExportTransportMap();
    //#endif
  } // namespace Inference
} // namespace muq

#endif // ifndef _TransportMap_h
