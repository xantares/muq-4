#ifndef UnivariateBasis_h_
#define UnivariateBasis_h_

#include <Eigen/Core>
#include <memory>

namespace muq {
  namespace Inference{
    
    class UnivariateBasis{
      
    public:
      UnivariateBasis(int dimIn) : dim(dimIn){};
      virtual ~UnivariateBasis() = default;
      
      // create a deep copy of this basis
      virtual std::shared_ptr<UnivariateBasis> Clone() const = 0;
      
      double Evaluate(Eigen::Ref<const Eigen::VectorXd> const& xVec) const;
      double Derivative(Eigen::Ref<const Eigen::VectorXd> const& xVec, const int wrt) const;
      double SecondDerivative(Eigen::Ref<const Eigen::VectorXd> const& xVec, const int wrt1, const int wrt2) const;
      
      virtual double Evaluate(const double x) const = 0;
      virtual double Derivative(const double x) const = 0;
      virtual double SecondDerivative(const double x) const = 0;
      
      int GetDim() const{return dim;};
      void SetDim(int newDim){dim=newDim;};
      
    protected:
      int dim;
    
    }; // class UnivariateBasis
  
  }
}

#endif
