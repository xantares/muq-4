#ifndef MARGINALSAMPLINGPROBLEM_H_
#define MARGINALSAMPLINGPROBLEM_H_

#include <math.h>

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/GaussianPair.h"
#endif // MUQ_PYTHON == 1

#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/UniformRandVar.h"

#include "MUQ/Approximation/Incremental/PolynomialApproximator.h"

#include "MUQ/Inference/ImportanceSampling/ImportanceSampler.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"

namespace muq {
namespace Inference {
class MarginalSamplingProblem : public SamplingProblem {
public:

  template<typename rv, typename dens, typename specs>
    /**
       <ol>
         <li> The index of the parameter being marginalized (<EM>MarginalSamplingProblem.MarginalizedIndex</EM>)
	 <ul>
	   <li> Defaults to \f$0\f$
	 </ul>
	 <li> The number of importance samples (<EM>MarginalSamplingProblem.NumImportanceSamples</EM>)
	 <ul>
	   <li> Defaults to \f$100\f$
	 </ul>
	 <li> Use pseudo marginal or noisy regression? (<EM>MarginalSamplingProblem.PseudoMarginal</EM>)
	 <ul>
	   <li> TRUE: Use pseudo marginal MCMC
	   <li> FALSE: Use noisy regression MCMC (Default)
	   <li> If true; the ptree must also have all the options for muq::Approximation::PolynomialRegressor, muq::Approximation::PolynomialApproximator, and optimization.
	 </ul>
	 <li> Initial number of nearest neighbors to use in the approximation (<EM>MarginalSamplingProblem.NumNearestNeighborsBase</EM>)
	 <ul> 
	   <li> Number of nearest neighbors \f$k_0\f$ at step \f$0\f$.  
	   <li> Must be positive
	   <li> Defaults to \f$5\f$ 
	   <li> Only used in the noisy regression case; not used in the pseudo marginal case.
	 </ul>
	 <li> Rate of growth for the number of nearest neighbors (<EM>MarginalSamplingProblem.NumNearestNeighborsExp</EM>)
	 <ul> 
	   <li> The number of nearest neighbors after \f$M\f$ refinements is \f$k_M = k_0 + M^\eta\f$
	   <li> Must be positive
	   <li> Defaults to \f$0.0\f$ (never grow)
	   <li> MUST grow if the function is noisy
	   <li> Only used in the noisy regression case; not used in the pseudo marginal case.
	 </ul>
	 <li> Initial probability of random refinement \f$\beta_0\f$ (<EM>MarginalSamplingProblem.RandomRefinementBase</EM>)
	 <ul> 
	   <li> Probability of refinement at step \f$0\f$.  
	   <li> Defaults to \f$0.0\f$ (never randomly refine)
	   <li> Only used in the noisy regression case; not used in the pseudo marginal case.
	 </ul>
	 <li> Rate of decay for the probability of random refinement (<EM>MarginalSamplingProblem.RandomRefinementExp</EM>
	 <ul>
	   <li> Refine at step \f$N\f$ with probability \f$\beta_N = \beta_0 N^{\beta_n}\f$ 
	   <li> Defaults to \f$0.0\f$ (do not decay)
	 </ul>
	 <li> Initial threshold for refinement due to error indicator \f$\gamma_0\f$ (<EM>MarginalSamplingProblem.ErrorIndicatorRefinementBase</EM>)
	 <ul> 
	   <li> Threshold for refinement at step \f$0\f$.  
	   <li> Defaults to \f$0.0\f$ (always refine)
	   <li> Only used in the noisy regression case; not used in the pseudo marginal case.
	 </ul>
	 <li> Rate of decay for the threshold of refinement due to error indicator (<EM>MarginalSamplingProblem.ErrorIndicatorRefinementExp</EM>
	 <ul>
	   <li> Refine at step \f$N\f$ with probability \f$\gamma_N = \gamma_0 N^{\gamma_n}\f$ 
	   <li> Defaults to \f$0.0\f$ (do not decay)
	 </ul>
       </ol>
       @param[in] joint The joint density (must have two inputs)
       @param[in] pair The density/random variable pair for importance sampling 
       @param[in] pt The parameter tree of the options 
       @param[in] metricObject The metric 
     */
    MarginalSamplingProblem(std::shared_ptr<muq::Modelling::Density> const& joint,
			    std::shared_ptr<muq::Modelling::DensityRVPair<rv, dens, specs> > const& pair,
			    boost::property_tree::ptree const& pt,
			    std::shared_ptr<AbstractMetric> const& metricObject = std::make_shared<EuclideanMetric>()) :
  SamplingProblem(joint, metricObject), numRefined(0)
    {
      const int inputOfInterest = pt.get("MarginalSamplingProblem.MarginalizedIndex", 0);
      const int Nimportance     = pt.get("MarginalSamplingProblem.NumImportanceSamples", 1e1);

      const bool usePM = pt.get("MarginalSamplingProblem.PseudoMarginal", false);

      // build the importance sampler
      densityEstimator = std::make_shared<ImportanceSampler>(joint, pair, inputOfInterest, Nimportance);

      // use noisy regression?
      if( !usePM ) { 
	approximator = std::make_shared<muq::Approximation::PolynomialApproximator>(densityEstimator, pt);

	// the initial points to build the approximations with
	Eigen::MatrixXd initialPoints(approximator->outputSize, approximator->NumNearestNeighbors());

	// loop through the number of nearest neighbors to use
	for( unsigned int i=0; i<approximator->NumNearestNeighbors(); ++i ){
	  // sample the initial points from the importance sampling distribution
	  initialPoints.col(i) = densityEstimator->Evaluate(pair->rv->Sample());
	}

	// add the initial points to the cache
	approximator->AddToCache(initialPoints);

	// probability of random refinement 
	betaBase = pt.get("MarginalSamplingProblem.RandomRefinementBase", 0.0);
	betaExp  = pt.get("MarginalSamplingProblem.RandomRefinementExp", 0.0);
	assert(betaExp <= 0.0);

	// refinement due to error indicator
	gammaBase = pt.get("MarginalSamplingProblem.ErrorIndicatorRefinementBase", 0.0);
	gammaExp  = pt.get("MarginalSamplingProblem.ErrorIndicatorRefinementExp", 0.0);
	assert(gammaExp <= 0.0);

	// number of nearest neighbors 
	knBase = pt.get("MarginalSamplingProblem.NumNearestNeighborsBase", 5);
	knExp  = pt.get("MarginalSamplingProblem.NumNearestNeighborsExp", 0.0);
	assert(knExp >= 0.0);

	// one dimensional uniform random variable on the unit box 
	uniform = std::make_shared<muq::Modelling::UniformRandVar>(std::make_shared<muq::Modelling::UniformBox>(Eigen::VectorXd::Zero(1), Eigen::VectorXd::Ones(1)));
      }
    }

#if MUQ_PYTHON == 1
  MarginalSamplingProblem(std::shared_ptr<muq::Modelling::Density> const& joint, std::shared_ptr<muq::Modelling::GaussianPair> const& pair, boost::python::dict const& para);
								      
  MarginalSamplingProblem(std::shared_ptr<muq::Modelling::Density> const& joint, std::shared_ptr<muq::Modelling::GaussianPair> const& pair, boost::python::dict const& para, std::shared_ptr<muq::Inference::AbstractMetric> const& metricObject);
#endif // MUQ_PYTHON == 1

  
  virtual ~MarginalSamplingProblem() = default;

  /// Get the approximator 
  /**
     \return The polynomial approximator 
   */
  std::shared_ptr<muq::Approximation::PolynomialApproximator> GetApproximator() const;

private:

  /// Compute the log-likelihood
  /**
     Either approximate the log likelihood by taking the log of the importance sampling approximation (pseudo marginal) or compute the log likelihood using the polynomial approximator.  If the polynomial approximator is used, this function also decides whether or not to refine and refines if needed.
     @param[in] state The current state
     @param[in] currIteration The current iteration 
     @param[in] logEntry A log for HDF5
   */
  double LogLikelihood(Eigen::VectorXd const& state, int const currIteration, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry = nullptr);

  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  /// Importance sampler to estimate the likelihood
  std::shared_ptr<ImportanceSampler> densityEstimator;

  /// Polynomial approximator of the likelihood
  std::shared_ptr<muq::Approximation::PolynomialApproximator> approximator;

  /// initial probability of random refinement 
  /**
     Used in the regression case; not in the pseudo marginal case
  */
  double betaBase;

  /// Rate of decay for the probability of random refinement
  /**
     Used in the regression case; not in the pseudo marginal case
  */
  double betaExp;

  /// initial refinement due to error indicator threshold
  /**
     Used in the regression case; not in the pseudo marginal case
  */
  double gammaBase;

  /// Rate of decay for refinement due to error indicator
  /**
     Used in the regression case; not in the pseudo marginal case
  */
  double gammaExp;

  /// initial number of nearest neighbors 
  /**
     Used in the regression case; not in the pseudo marginal case
  */
  int knBase;

  /// Rate of growth for the number of nearest neighbors
  /**
     Used in the regression case; not in the pseudo marginal case
  */
  double knExp;

  /// one dimensional uniform random variable on the unit box 
  std::shared_ptr<muq::Modelling::UniformRandVar> uniform;

  /// The number of times this sampling problem has been refined
  int numRefined;
};
} // namespace Inference
} // namespace muq

#endif // ifndef MARGINALSAMPLINGPROBLEM_H_
