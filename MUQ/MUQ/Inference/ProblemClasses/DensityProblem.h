
#ifndef _DensityProblem_h
#define _DensityProblem_h

#include "MUQ/Inference/ProblemClasses/InferenceProblemBase.h"
#include "MUQ/Modelling/Density.h"


namespace muq {
namespace Inference {
/** @class DensityProblem
 *   @ingroup Inference
 *   @brief InferenceProblem wrapper around the Density class
 *   @detail This class provides a translation from the Density object in the Modelling module to an InferenceProblem in
 *      the InferenceModule that the MAP and MCMC solvers can utilize.
 */
class DensityProblem : public InferenceProblemBase {
public:

  /** Create the problem from a specified Density.
   *  @param[in] DensIn The input Density
   */
  DensityProblem(muq::Modelling::Density DensIn) : Dens(DensIn) {}

  /** Evaluate the log density at the point xc
   *  @param[in] xc The location to evaluate the density.
   *  @return The log-density evaluated at xc
   */
  virtual double LogEval(const Eigen::VectorXd& xc);

  /** Evaluate the gradient of the log density at xc
   *  @param[in] xc The location we want to evaluate the density.
   *  @param[out] grad The gradient of the log-density evaluated at xc
   *  @return The log-density evaluated at xc
   */
  virtual double GradLogEval(const Eigen::VectorXd& xc, Eigen::VectorXd& grad);

  /** Return the parameter dimension.
   *  @return An integer containing the number of parameters in this problem
   */
  virtual int    GetDim();

protected:

  muq::Modelling::Density Dens;
};

// class DensityProblem
} // namespace Inference
} // namespace muq

#endif // ifndef _DensityProblem_h
