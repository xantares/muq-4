
#ifndef MH_H_
#define MH_H_

#include "MUQ/Inference/MCMC/MCMCKernel.h"
#include "MUQ/Inference/MCMC/MCMCProposal.h"

namespace muq {
namespace Inference {
class TransportMapKernel;

/**
 *         \class MH
 *         \author Matthew Parno
 *         \brief Simple MH algorithm with isotropic proposal
 *         This class implements a basic Metropolis-Hastings style MCMC algorithms using a simple isotropic Gaussian
 * proposal density.
 */
class MHKernel : public MCMCKernel {
  friend class TransportMapKernel;

public:

  /** Read in parameters a from parsed xml document.
   *             @param[in] paramList Parameter list containing the MCMC parameters such as number of steps, burnin, and
   * isotropic variance
   */
  MHKernel(std::shared_ptr<AbstractSamplingProblem> inferenceProblem, boost::property_tree::ptree& properties);

  virtual ~MHKernel() {}

  virtual void                                       PrintStatus() const override;


  virtual std::shared_ptr<muq::Inference::MCMCState> ConstructNextState(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    int const                                     iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  /** Get the acceptance rate.
   *             @return The acceptance rate in [0,1]
   */
  virtual double GetAccept() const;


  /** Write important information about the settings in this kernel as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName, std::string prefix = "") const override;

protected:

  ///Combine the proposal density with the target density to compute the acceptance probability
  virtual double ComputeAcceptanceProbability(std::shared_ptr<muq::Inference::MCMCState> currentStateIn,
                                              std::shared_ptr<muq::Inference::MCMCState> proposedStateIn);

  /// pointer to proposal
  std::shared_ptr<muq::Inference::MCMCProposal> proposal;

  /// keep track of the number of accepted steps
  int numAccepts = 0;

  /// keep track of the total number of times ConstructNextState was called
  int totalSteps = 0;

private:
};
} // namespace inference
} // namespace muq

#endif /* MH_H_ */
