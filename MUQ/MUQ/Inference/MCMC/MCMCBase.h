
#ifndef MCMCBASE_H
#define MCMCBASE_H

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
# include "MUQ/Utilities/python/PythonTranslater.h" // This needs to be the first python include to avoid a weird
                                                    // "toupper" macro error on osx

# include <boost/python.hpp>
# include <boost/python/def.hpp>
# include <boost/python/class.hpp>

#endif // if MUQ_PYTHON == 1

#include <map>

#include <Eigen/Core>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <boost/property_tree/ptree_fwd.hpp>
#include <boost/function.hpp>

namespace muq {
namespace Modelling {
class Density;
class EmpiricalRandVar;
#if MUQ_PYTHON == 1
class EmpiricalRandVarPython;
#endif // if MUQ_PYTHON == 1
}

namespace Inference {
class AbstractSamplingProblem;

template<typename T>
struct DerivedMCMCRegister;

///A base interface for MCMC that provides all the necessary functions for most users.

/**
 * Provides a common core for MCMC, and a the broad user interface. Most users will call ConstructMCMC to build the
 * particular solver of interest; different solvers may be available based on the type of the Density or whether they
 * were built into the library. Then call sample to perform sampling, and getAccept or getReject for some chain
 * statistics.
 *
 * This base implementation assumes that MCMC is performed by calling the sample(xStart) method, which starts the chain
 * at xStart. MCMC proceeds by repeatedly calling the method SampleOnce. Some of the samples are saved - inheriting
 * classes must use the IsCurrentIterationSaved method to determine whether a particular sample generated by SampleOnce
 * should be saved. The saved entries should be present in the EmpiricalRandVar provided by the abstract method
 * GetResults.
 *
 * The iteration count tracks the number of steps taken, some of which are saved. States are saved if they occur after
 * the burnInLength period, then every Nth (stored in saveSampleFrequency) thereafter, until totalIterations is reached.
 *
 * See the constructor for the parameter list and their default values. Relies on MCMCKernels and MCMCProposals to
 *accomplish MCMC.
 *
 * Child classes register themselves so that the user need only call the ConstructMCMC method and the parameter list
 * need only name the MCMC method to use in MCMC.Method. They'll be available if and only if the MCMC type has actually
 * been compiled in.
 *
 * In order to make this happen, you need to call macro, REGISTER_MCMCBASE in the cpp with the class name.
 * Based on MUQ/Utilities/RegisterClassNameHelper.h, see for more details.
 *
 * SingleChainMCMC is the canonical example.
 *
 **/
class MCMCBase {
public:

  ///Initialize data members. ConstructMCMC is the prefered user initialization interface.

  /**
   * Anything initialized from input properties should be echoed to LOG(INFO).
   **/
  MCMCBase(std::shared_ptr<muq::Inference::AbstractSamplingProblem> density, boost::property_tree::ptree& properties);

  virtual ~MCMCBase();


  ///Set up for performing inference on the input density, based on the settings in the input property tree.

  /**
   * This method will allow you to ask for any of the MCMC types available, merely by specifying their name in the
   * property list.
   *
   **/
  static std::shared_ptr<MCMCBase> ConstructMCMC(std::shared_ptr<muq::Inference::AbstractSamplingProblem> density,
                                                 boost::property_tree::ptree                            & properties);

  ///As the other version, but read the property list from an xml file first.
  static std::shared_ptr<MCMCBase> ConstructMCMC(std::shared_ptr<muq::Inference::AbstractSamplingProblem> density,
                                                 std::string                                              xmlFile);

  ///Sample a MCMC chain beginning at xStart.

  /**
   * Can interrupt sampling early by setting a time limit or an iteration to stop at. Simply call it again to continue,
   * until the total has been reached.
   **/
  virtual std::shared_ptr<muq::Modelling::EmpiricalRandVar> Sample(Eigen::VectorXd const& xStart);

#if MUQ_PYTHON == 1
  std::shared_ptr<muq::Modelling::EmpiricalRandVar>         PySample(boost::python::list const& xStart);
  std::shared_ptr<muq::Modelling::EmpiricalRandVar>         PySampleTime(boost::python::list const& xStart,
                                                                         double                     time);
  std::shared_ptr<muq::Modelling::EmpiricalRandVar>         PySampleTimeIter(boost::python::list const& xStart,
                                                                             double                     time,
                                                                             int                        iterationToStop);
#endif // if MUQ_PYTHON == 1

  /** Function to get posterior samples
   * @return A smart point (of type EmpRvPtr) to the posterior EmpiricalRandVar instance generated by the sample
   * function.
   **/
  virtual std::shared_ptr<muq::Modelling::EmpiricalRandVar> GetResults() = 0;


  int                                                       GetCurrentChainLength()
  {
    return currIteration;
  }

  /** This function has the same purpose as the function with groupName argument.  However, this function uses the
   * internal hdf5Group object. */
  virtual void WriteAttributes(std::string const& groupName, std::string prefix = "") const;
  virtual void WriteAttributes() const {}

protected:

  /// print progress to screen for given verbosity level

  /**
   * Progress should always be put into the log, and should be printed to cout if verbosity > 0 (or some other number
   *>0). Be sure to call the parent version.
   **/
  virtual void PrintProgress();

  ///Used by inheriting classes to decide whether to save the current iteration for the results.
  bool         IsCurrentIterationSaved() const;


  virtual int  GetDim() const final;

  ///Number of samples to take
  int totalIterations = 0;

  double timeLimit = std::numeric_limits<double>::infinity();

  ///Current sample number
  int currIteration = 1;

  ///Length of burn in
  int burnInLength = 0;

  ///Frequency of samples to save after burn-in elapses.
  int saveSampleFrequency = 1;

  /// Should I write output to screen?
  int verbose = 0;

  ///The density to sample from.
  std::shared_ptr<muq::Inference::AbstractSamplingProblem> samplingProblem;

private:

  /**
   * @brief Prepare for the first call of SampleOnce.
   * Gets called once by Sample before SampleOnce is called. Used by child methods to capture the start state and
   * prepare storage for the length of the chain.
   *
   **/
  virtual void SetupStartState(Eigen::VectorXd const& xStart) = 0;


  /**
   * The method repeatedly called that actually does the sampling and populates whatever storage allows GetResults() to
   * provide a useful result.
   *
   * Must perform ++currIteration so that parallel chains get incremented even though they only have SampleOnce called a
   * lot, not Sample.
   **/
  virtual void SampleOnce() = 0;

public:

  //start of registration boilerplate from MUQ/Utilities/RegisterClassNameHelper.h, should be public
  //  ************************************
  typedef boost::function<std::shared_ptr<MCMCBase>(std::shared_ptr<muq::Inference::AbstractSamplingProblem> density,
                                                    boost::property_tree::ptree& properties)> FactorySignature;

  typedef std::map<std::string, FactorySignature> FactoryMapType;


  ///Boilerplate code to have a safe, static copy of a map from names to constructors.
  static std::shared_ptr<FactoryMapType> GetFactoryMap()
  {
    static std::shared_ptr<FactoryMapType> map;

    // if the map doesn't exist yet, create it
    if (!map) {
      map = std::make_shared<FactoryMapType>();
    }

    return map;
  }

  //end of registration boilerplate ************************************
};

#define REGISTER_MCMCBASE(NAME) static auto reg = \
  muq::Inference::MCMCBase::GetFactoryMap()->insert(std::make_pair(# NAME, boost::shared_factory<NAME>()));
}
}


#endif // MCMCBASE_H
