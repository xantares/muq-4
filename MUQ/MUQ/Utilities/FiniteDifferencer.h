
#ifndef _FiniteDifferencer_h
#define _FiniteDifferencer_h

#include <Eigen/Core>

namespace muq {
namespace Utilities {
/** This is a general templated function for computing the finite difference gradient of a scalar valued, vector input
 *  function.  The function should be defined as a unary function taking an Eigen::VectorXd as input, and returning a
 *  double.  It is possible to use the bind routine found in the functional header of c++11 to take a function with more
 *  arguments and create a function with a single vector input.
 *  @param[in] f The function object to take the gradient of.  It should be possible to call this function as "double
 *     fout = f(Eigen::VectorXd xc)"
 *  @param[in] xc A vector holding the position we wish to evaluate the gradient at
 *  @param[out] grad An approximation to the gradient of the function evaluated at xc.
 *  @param[in] absScale The absolute scaling of the steps used for the finite difference.  The step size will be step_i
 * =
 *     min(absScale,relScale*abs(xc_i)).
 *  @param[in] relScale The relative scaling of the variables
 *  @return The function evaluated at xc f(xc).
 */
template<typename FuncType>
double computeFdGrad(FuncType             & f,
                     const Eigen::VectorXd& xc,
                     Eigen::VectorXd      & grad,
                     double                 absScale = 1e-6,
                     double                 relScale = 1e-6)
{
  // resize the gradient
  grad = Eigen::VectorXd::Zero(xc.size());

  // create a vector for the perturbed position
  Eigen::VectorXd xshift(xc);

  // hold the value of the objective at xc
  double fc;

  // hold the value of the objective at the shifted position
  double fshift;

  // first, evaluate the objective at xc
  fc = f(xc);

  // now, perturb each dimension and fill in the gradient
  for (int i = 0; i < xc.size(); ++i) {
    // compute the step length
    double step = fmin(absScale, relScale * fabs(xc(i)));

    // find the new position
    xshift(i) += step;

    // evaluate the objective at the new location
    fshift = f(xshift);

    // compute the gradient in this direction
    grad(i) = (fshift - fc) / step;

    // reset the shifted position
    xshift(i) = xc(i);
  }

  return fc;
}

/** This is a general templated function for computing the finite difference hessian of a scalar valued, vector input
 *  function.  The function should be defined as a unary function taking an Eigen::VectorXd as input, and returning a
 *  double.  It is possible to use the bind routine found in the functional header of c++11 to take a function with more
 *  arguments and create a function with a single vector input.
 *  @param[in] f The function object to take the gradient of.  It should be possible to call this function as "double
 *     fout = f(Eigen::VectorXd xc)"
 *  @param[in] g A function object that takes a vector as input and returns a vector with the gradient of the function
 * at
 *     that point.
 *  @param[in] xc A vector holding the position we wish to evaluate the gradient at
 *  @param[out] hess An approximation to the Hessian of the function evaluated at xc.
 *  @param[in] absScale The absolute scaling of the steps used for the finite difference.  The step size will be step_i
 * =
 *     min(absScale,relScale*abs(xc_i)).
 *  @param[in] relScale The relative scaling of the variables
 *  @return An approximation to the gradient of the function evaluated at xc..
 */
template<typename FuncType, typename GradFuncType>
Eigen::VectorXd computeFdHess(FuncType             & f,
                              GradFuncType         & g,
                              const Eigen::VectorXd& xc,
                              Eigen::MatrixXd      & hess,
                              double                 absScale = 1e-6,
                              double                 relScale = 1e-6)
{
  // resize the gradient and Hessian
  hess = Eigen::MatrixXd::Zero(xc.size(), xc.size());
  Eigen::VectorXd gc = Eigen::VectorXd::Zero(xc.size());

  // create the shifted gradient vectors
  Eigen::VectorXd gshift = Eigen::VectorXd::Zero(xc.size());

  // create a vector for the perturbed position
  Eigen::VectorXd xshift(xc);

  // first, evaluate the gradient at xc
  gc = g(xc);


  // now, perturb each dimension and fill in the gradient
  for (int i = 0; i < xc.size(); ++i) {
    // compute the step length
    double step = fmin(absScale, relScale * fabs(xc(i)));

    // find the new position
    xshift(i) += step;

    // evaluate the objective at the new location
    gshift = g(xshift);

    // compute the gradient in this direction
    hess.col(i) = (gshift - gc) / step;

    // reset the shifted position
    xshift(i) = xc(i);
  }

  // solve the linear system
  return gc;
}
} // namespace Utilities
} // namespace muq


#endif // ifndef _FiniteDifferencer_h
