
#ifndef HDF5TYPES_H_
#define HDF5TYPES_H_

#include <hdf5.h>
#include <hdf5_hl.h>


namespace muq {
  namespace Utilities {
	
	herr_t H5LTset_attribute(hid_t loc_id, const char *obj_name, const char *attr_name, hid_t mem_type_id, void *buffer, size_t size);
  	
    template<typename scalarType>
    struct HDF5_Type{
      static int GetFlag();
    };
	
    template<>
    struct HDF5_Type<double>{
      static int GetFlag(){return H5T_NATIVE_DOUBLE;};
    };
    template<>
    struct HDF5_Type<long double>{
      static int GetFlag(){return H5T_NATIVE_LDOUBLE;};
    };
    template<>
    struct HDF5_Type<int>{
      static int GetFlag(){return H5T_NATIVE_INT;};
    };
    template<>
    struct HDF5_Type<long>{
      static int GetFlag(){return H5T_NATIVE_LONG;};
    };
    template<>
    struct HDF5_Type<unsigned long>{
      static int GetFlag(){return H5T_NATIVE_ULONG;};
    };
    template<>
    struct HDF5_Type<unsigned>{
      static int GetFlag(){return H5T_NATIVE_UINT;};
    };
	
    template<>
    struct HDF5_Type<float>{
      static int GetFlag(){return H5T_NATIVE_FLOAT;};
    };
	
    template<>
    struct HDF5_Type<unsigned short>{
      static int GetFlag(){return H5T_NATIVE_USHORT;};
    };
    template<>
    struct HDF5_Type<short>{
      static int GetFlag(){return H5T_NATIVE_SHORT;};
    };
    
    template<>
    struct HDF5_Type<char>{
      static int GetFlag(){return H5T_NATIVE_CHAR;};
    };
    
    template<>
    struct HDF5_Type<bool>{
		
      static int GetFlag();
    };
  }
}



#endif // HDF5TYPES_H_
