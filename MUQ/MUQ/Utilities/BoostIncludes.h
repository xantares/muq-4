
#ifndef _BoostIncludes_h
#define _BoostIncludes_h

// I don't want this used right now - Patrick
#ERROR

// first, include the config header so we know if Boost is available or not
#include "config.h"

#if HAVE_BOOST == 1

// smart pointer header


// property trees used in ParameterList
# include <boost/property_tree/ptree.hpp>
# include <boost/property_tree/xml_parser.hpp>

// random number generation
# include <boost/random.hpp>


// headers related to the graph library
# include <utility> // std::pair
# include <boost/graph/graph_traits.hpp>
# include <boost/graph/adjacency_list.hpp>
# include <boost/graph/topological_sort.hpp>
# include <boost/utility.hpp>
# include <boost/graph/graph_utility.hpp>
# include <boost/graph/random.hpp>
# include <boost/pending/indirect_cmp.hpp>
# include <boost/graph/reverse_graph.hpp>
# include <boost/graph/filtered_graph.hpp>

// preprocessor libraries
# include <boost/preprocessor/repetition/enum_params.hpp>
# include <boost/preprocessor/repetition.hpp>
# include <boost/preprocessor/arithmetic/sub.hpp>
# include <boost/preprocessor/punctuation/comma_if.hpp>
# include <boost/preprocessor/iteration/local.hpp>

// necessary special functions
# include <boost/math/special_functions/gamma.hpp>
# include <boost/math/special_functions/bessel.hpp>

#endif // HAVE_BOOST==1


#endif // ifndef _BoostIncludes_h
