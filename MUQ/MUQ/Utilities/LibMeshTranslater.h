
#include "MUQ/config.h"

#if HAVE_LIBMESH == 0
# ERROR
#endif // if HAVE_LIBMESH == 0

#ifndef LibMeshTranslater_h_
#define LibMeshTranslater_h_

# include "libmesh/numeric_vector.h"

# include "MUQ/Utilities/VectorTranslater.h"

namespace muq {
namespace Utilities {
/// Translate a libMesh::NumericVector<double> to a T2
template<typename T2>
void VectorTranslate(const libMesh::NumericVector<libMesh::Number>& Input, T2& Output, int size)
{
  // resize the output vector
  Output.resize(size);

  // copy the information
  //std::cout << "Printing iteration:\n";
  for (int i = 0; i < size; ++i) {
    //  std::cout << i << " / " << size << std::endl;
    Output[i] =
      ConvertScalar<typename std::remove_const<typename std::remove_reference<decltype(Output[0])>::type>::type,
                    typename std::remove_const<typename std::remove_reference<decltype(Input(0))>::type>::type>(Input(i));
  }
}

/// Specfic Translater class for libMesh::NumericVector<double>->T2
template<typename T2>
class Translater<libMesh::NumericVector<libMesh::Number>, T2> : public EigenTranslater {
public:

  ///  Const Constructor
  /**
   *  @param[in] vec1 A vector of type libMesh::NumericVector<libMesh::Number> that we wish to translate to a vector of
   *type T2.
   */
  Translater(const libMesh::NumericVector<libMesh::Number>& vec1)
  {
    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = const_cast<libMesh::NumericVector<libMesh::Number> *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
      ForwardTranslate(); // translate from libMesh::NumericVector<libMesh::Number> to T2
  }

  /// Const Constructor
  /**
   *  @param[in] vec1 A vector of type libMesh::NumericVector<libMesh::Number> that we wish to translate to a vector of
   *type T2.
   */
  Translater(const libMesh::NumericVector<libMesh::Number>& vec1, int VecSizeIn)
  {
    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = const_cast<libMesh::NumericVector<libMesh::Number> *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
      ForwardTranslate(); // translate from libMesh::NumericVector<libMesh::Number> to T2
  }

  /// Usual Constructor
  /**
   *  @param[in] vec1 A vector of type libMesh::NumericVector<libMesh::Number> that we wish to translate to a vector of
   *type T2.
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(libMesh::NumericVector<libMesh::Number>& vec1)
  {
    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = &vec1;

    if (std::is_same<libMesh::NumericVector<libMesh::Number>, T2>::value) {
      vec2ptr = reinterpret_cast<T2 *>(vec1ptr);
    } else {
      ForwardTranslate(); // translate from libMesh::NumericVector<libMesh::Number> to T2
    }
  }

  /// More general constructor that also works for double pointer inputs but requires vector size
  /**
   *  @param[in] vec1 A vector of type libMesh::NumericVector<libMesh::Number> that we wish to translate to a vector of
   *type T2.
   *  @param[in] VecSizeIn The number of elements in the vector
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(libMesh::NumericVector<libMesh::Number>& vec1, int VecSizeIn)
  {
    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = &vec1;

    if (std::is_same<libMesh::NumericVector<libMesh::Number>, T2>::value) {
      vec2ptr = reinterpret_cast<T2 *>(vec1ptr);
    } else {
      ForwardTranslate(); // translate from libMesh::NumericVector<libMesh::Number> to T2
    }
  }

  /// Destructor.
  ~Translater() {}

  /// Return a pointer to the translated vector.
  /**
   *  @return A pointer to a T2 vector translated from constructor input
   */
  T2* GetPtr()
  {
    return vec2ptr;
  }

  virtual Eigen::VectorXd* GetEigenPtr()
  {
    if (std::is_same<T2, Eigen::VectorXd>::value) {
      return reinterpret_cast<Eigen::VectorXd *>(vec2ptr);
    } else {
      std::cerr << "ERROR: Attempt to access Eigen::VectorXd pointer from different type.\n";
      throw("Invalid template parameter.");
      return nullptr;
    }
    return nullptr;
  }

private:

  /// Translate the libMesh::NumericVector<libMesh::Number> type vector to an internally stored T2 type vector.
  void ForwardTranslate() // strange to native
  {
    VectorTranslate(*vec1ptr, this->vec2, this->VecSize);
    this->vec2ptr = &(this->vec2);
  }

  /// Translate the potentially changed T2 type vector to a libMesh::NumericVector<libMesh::Number> type vector.
  void BackwardTranslate() // native to strange
  {
    VectorTranslate(vec2, *vec1ptr, VecSize);
  }

  /// keep a pointer to the original untranslated vector
  libMesh::NumericVector<libMesh::Number> *vec1ptr;

  /// keep a pointer tot he translated vector
  T2 *vec2ptr;

  /// in some cases we will create a vector of type T2 to hold the new vector
  T2 vec2;

  /// store the size of the vector -- this is important for handling double pointers
  unsigned int VecSize;
};
} // namespace GeneralUtilties
} // namespace muq


#endif // ifndef VectorTranslater_h_
