#ifndef LEGENDREPOLYNOMIALS1DRECURSIVE_H_
#define LEGENDREPOLYNOMIALS1DRECURSIVE_H_


#include <boost/serialization/access.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * @class LegendrePolynomials1DRecursive
 * @ingroup Polynomials1D
 * @brief Implements the Legendre Polynomials.
 * @details Subclasses the numerically stable RecursivePolynomialFamily1D with the three-term
 * recurrence necessary to generate the Legendre polynomials.
 **/
class LegendrePolynomials1DRecursive : public Static1DPolynomial<LegendrePolynomials1DRecursive> {
public:

  friend class Static1DPolynomial<LegendrePolynomials1DRecursive>;
  
  typedef std::shared_ptr<LegendrePolynomials1DRecursive> Ptr;

  LegendrePolynomials1DRecursive() = default;
  virtual ~LegendrePolynomials1DRecursive() = default;

  ///Implements normalization.
  static double GetNormalization_static(unsigned int n);

  static Eigen::VectorXd GetMonomialCoeffs(const int order);
  
  static double gradient_static(unsigned int order, double x);
  static double secondDerivative_static(unsigned int order, double x);

private:

  ///Grant access to serialization
  friend class boost::serialization::access;

  ///Serialization method
  template<class Archive>
  void   serialize(Archive& ar, const unsigned int version);

  //implement methods defining recurrence
  static double alpha(double x, unsigned int k);
  static double beta(double x, unsigned int k);
  static double phi0(double x);
  static double phi1(double x);
};
}
}


BOOST_CLASS_EXPORT_KEY(muq::Utilities::Static1DPolynomial<muq::Utilities::LegendrePolynomials1DRecursive>)
BOOST_CLASS_EXPORT_KEY(muq::Utilities::LegendrePolynomials1DRecursive)

#endif /* LEGENDREPOLYNOMIALS1DRECURSIVE_H_ */
