
#ifndef _UnstructuredSimplicialMesh_h
#define _UnstructuredSimplicialMesh_h

#include "MUQ/Utilities/mesh/Mesh.h"


namespace muq {
namespace Utilities {
template<unsigned int dim>
class UnstructuredSimplicialMesh : public Mesh<dim> {
public:

  /** Create an empty mesh with no nodes or elements. */
  UnstructuredSimplicialMesh() {}

  /** Read the mesh in Triangle format from an element file and a node file. */
  UnstructuredSimplicialMesh(const std::string& EleFile, const std::string& NodeFile);

  /** Construct the mesh on a rectangle -- mostly used for debugging. This constructor builds the quad mesh and then
   *  divides each quadrilateral element into two simplicial elements */
  UnstructuredSimplicialMesh(const Eigen::Matrix<double, dim, 1>& lbIn,
                             const Eigen::Matrix<double, dim, 1>& ubIn,
                             const Eigen::Matrix<unsigned int, dim, 1>& NIn);

  /** Get the position of the a node in the mesh
   *  @param[in] node The node number
   *  @return A vector holding the spatial coordinates of the node.
   */
  virtual Eigen::Matrix<double, dim, 1>                               GetNodePos(unsigned int node) const;

  /** Get the geometric center of an element in the mesh.
   *  @param[in] ele The element number
   *  @return A vector holding the spatial coordinates of the element center.
   */
  virtual Eigen::Matrix<double, dim, 1>                               GetElePos(unsigned int ele) const;

  /** Return a vector of unsigned integers for the center nodes around an element.
   *  @param[in] ele Element of interest
   *  @return List of nodes for element ele.
   */
  virtual Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> GetNodes(unsigned int ele) const;

  /** Get the area or volume of an element.
   *  @param[in] ele Element of interest.
   *  @return The length/area/volume of element ele.
   */
  virtual double                                                      EleSize(unsigned int ele) const;

  /** Get the number of nodes in the mesh. */
  virtual unsigned int                                                NumNodes() const
  {
    return NodePos.size();
  }

  /** Get the number of elements in the mesh. */
  virtual unsigned int NumEles() const
  {
    return Eles.size();
  }

protected:

  // store the elements by storing the 1,3, or 4 nodes defining the simplex
  std::vector < Eigen::Matrix < double, dim + 1, 1 >> Eles;

  // store the nodes by position
  std::vector < Eigen::Matrix < double, dim, 1 >> NodePos;
};

// class UnstructuredSimplicialMesh
} // namespace Utilities
} // namespace muq

#endif // ifndef _UnstructuredSimplicialMesh_h
