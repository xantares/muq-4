
#ifndef _Mesh_h
#define _Mesh_h

// standard library includes
#include <memory>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

// other external library includes
#include <Eigen/Core>
#include <Eigen/LU>

//#include "MUQ/Geostats/CovKernel.h"
//#include "MUQ/Utilities/Hmatrix/ElementTree.h"
using std::atan2;

namespace muq {
namespace Utilities {
// Necessary forward declarations
template<unsigned int dim>
class ElementPairTree;

template<unsigned int dim>
class ElementTree;

/** exception class used when a line search fails in the LineSearch class */
class FindElementFailure : public std::exception {
  virtual const char* what() const throw()
  {
    return "Failed to find an element in this mesh that contains the input position";
  }
};


/** \author Matthew Parno
 *  \brief An abstract mesh class for structured and unstructured meshes to be be used for finite element computations
 * or
 *     covariance matrix constructions, etc...
 *  \details This class provides the minimal interface needed by general finite element codes and covariance kernels.
 *  It
 *     ensures all children classes provide functionality to:
 *  <ul>
 *  <li> List the number of nodes and number of elements </li>
 *  <li> Get nodes for each element. </li>
 *  <li> Get the center position of each element. </li>
 *  </ul>
 */
template<unsigned int dim>
class Mesh {
public:

  typedef std::shared_ptr<Mesh> Ptr;

  /** Default constructor setting the separable variable to false. */
  Mesh(bool quad = false, bool sep = false) : separable(sep), hasquad(quad) {}

  /** Get the position of the a node in the mesh
   *  @param[in] node The node number
   *  @return A vector holding the spatial coordinates of the node.
   */
  virtual Eigen::Matrix<double, dim, 1> GetNodePos(unsigned int node) const = 0;

  /** Get the geometric center of an element in the mesh.
   *  @param[in] ele The element number
   *  @return A vector holding the spatial coordinates of the element center.
   */
  virtual Eigen::Matrix<double, dim, 1> GetElePos(unsigned int ele) const = 0;

  /** Given some location stored in the vector, return the element containing this point.  The current implementation of
   *  this function is general, but super slow.  A more sophisticated implementation could use some sort of KD-tree
   *  representation of the mesh for efficient searching or if the mesh stored connectivity information, that could be
   *  used to traverse the mesh efficiently.
   *  @param[in] pos The dimx1 position vector
   *  @return The element containing pos
   */
  unsigned int                          GetEle(const Eigen::Matrix<double, dim, 1>& pos);


  /** Return a vector of unsigned integers for the center nodes around an element.
   *  @param[in] ele Element of interest
   *  @return List of nodes for element ele.
   */
  virtual Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> GetNodes(unsigned int ele) const = 0;

  virtual unsigned int                                                GetNodeZone(unsigned int node)
  {
    return NodeZone(node);
  }

  virtual double GetNodeAtt(unsigned int node, unsigned int att = 0)
  {
    return NodeValue(node, att);
  }

  /** Get the area or volume of an element.
   *  @param[in] ele Element of interest.
   *  @return The length/area/volume of element ele.
   */
  virtual double       EleSize(unsigned int ele) const = 0;

  /** Get the number of nodes in the mesh. */
  virtual unsigned int NumNodes() const = 0;

  /** Get the number of elements in the mesh. */
  virtual unsigned int NumEles() const = 0;

  /** Can this mesh be represented as the tensor product of one dimensional meshes.  If so, return true so we can take
   *  advantage of this structure.  If not, return false and a more general (computationally more expensive) method will
   *  be used to compute KL modes. */
  virtual bool         IsSeparable() const
  {
    return separable;
  }

  /** Does this mesh have enough structure to provide an efficient quadrature rule over its domain?  Return true if yes.
   */
  virtual bool HasQuad() const
  {
    return hasquad;
  }

  /** When the mesh is separable, this function will return a matrix containing the locations of the 1d meshes.  If the
   *  mesh is not separable, an exception will be thrown.*/
  virtual std::vector<Eigen::VectorXd> GetSeparableMeshes() const
  {
    throw("ERROR: Tried to get Separable mesh definitions from non-Separable mesh.");
  }

  /** Return a matrix holding all of the element positions. */
  virtual Eigen::Matrix<double, dim, Eigen::Dynamic> GetAllElePos() const;

  virtual Eigen::Matrix<double, dim, 1>              GetLowerBounds() const
  {
    throw("ERROR: Tried to access a structured mesh function for a non-structured mesh");
  }

  virtual Eigen::Matrix<double, dim, 1> GetUpperBounds() const
  {
    throw("ERROR: Tried to access a structured mesh function for a non-structured mesh");
  }

  /** Write elementwise values to a file with the legacy VTK format for reading into Paraview, Visit, etc... */
  template<typename Derived>
  void writeScalarVtk(const std::string              & outfile,
                      const Eigen::DenseBase<Derived>& values,
                      const std::string              & DataName,
                      unsigned int                     porder = 0)
  {
    // creat the output stream for the vtk file
    std::ofstream vtkOut;

    vtkOut.open(outfile.c_str());

    // check to make sure the file opened correctly
    if (!vtkOut.is_open()) {
      std::cerr << "ERROR: Unable to open output file:\n\t" << outfile <<
          "\nin Mesh<dim>::writeScalarVtk(string outfile, const vector<double> &values, unsigned int porder).\n";
      throw("Unable to open output file.");
    }


    vtkOut << "# vtk DataFile Version 1.6\n";
    vtkOut << "2D Unstructured Grid\n";
    vtkOut << "ASCII\n\n";
    vtkOut << "DATASET UNSTRUCTURED_GRID\n";
    vtkOut << "POINTS " << NumNodes() << " float\n";
    vtkOut << std::fixed << std::setprecision(9);

    // write nodes
    for (unsigned int i = 0; i < NumNodes(); ++i) {
      Eigen::Matrix<double, dim, 1> CurrNodeLoc = GetNodePos(i);
      for (unsigned int d = 0; d < dim; ++d) {
        vtkOut << CurrNodeLoc(d, 0) << "    ";
      }

      if (dim == 1) {
        vtkOut << 0.0 << "    " << 0 << "\n";
      } else if (dim == 2) {
        vtkOut << 0.0 << "\n";
      }
    }


    // write elements
    Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> Ele = GetNodes(0);
    unsigned int EleSize                                            = Ele.size();
    vtkOut << "CELLS " << NumEles() << "  " << (EleSize + 1) * NumEles() << std::endl;

    for (unsigned int i = 0; i < NumEles(); ++i) {
      Ele = GetNodes(i);
      vtkOut << Ele.size() << "    ";

      for (unsigned int d = 0; d < EleSize; ++d) {
        vtkOut << Ele(d, 0) << "    ";
      }
      vtkOut << std::endl;
    }

    // tell paraview that cells are quadrilaterals
    vtkOut << "CELL_TYPES " << NumEles() << std::endl;
    unsigned int celltype = 7; // for general polygon
    if (EleSize == 3) {
      celltype = 5;            // for general vtk triangle
    } else if (EleSize == 4) {
      celltype = 9;            // for general vtk quadrilateral
    }

    for (unsigned int i = 0; i < NumEles(); ++i) {
      vtkOut << celltype << "\n";
    }

    // If the order is zero, the input data should be a constant over each element
    if (porder == 0) {
      assert(values.rows() == NumEles());

      vtkOut << "CELL_DATA " << NumEles() << std::endl;
      vtkOut << "SCALARS " << DataName << " float " << values.cols() << "\n";
      vtkOut << "LOOKUP_TABLE default\n";
      for (unsigned int i = 0; i < NumEles(); ++i) {
        for (unsigned int d = 0; d < values.cols(); ++d) {
          vtkOut << values(i, d) << "    ";
        }
        vtkOut << std::endl;
      }

      // in this case, the data are nodal quantities
    } else if (porder == 1) {
      assert(values.rows() == NumNodes());

      vtkOut << "POINT_DATA " << NumNodes() << std::endl;
      vtkOut << "SCALARS " << DataName << " float " << values.cols() << "\n";
      vtkOut << "LOOKUP_TABLE default\n";
      for (unsigned int i = 0; i < NumNodes(); ++i) {
        for (unsigned int d = 0; d < values.cols(); ++d) {
          vtkOut << values(i, d) << "    ";
        }
        vtkOut << std::endl;
      }
    }


    vtkOut.close();
  }

protected:

  bool separable;
  bool hasquad;


  // keep an element tree of this mesh -- once created, this allows for super fast searches and efficient Hierarchical
  // matrix approximations
  std::shared_ptr < ElementTree < dim >> EleTreePtr;


  // in cases where we have other information (such as boundary conditions), we need to store the nodes, the zone, and
  // value of the attribute
  Eigen::MatrixXd NodeValue;
  Eigen::Matrix<unsigned int, Eigen::Dynamic, 1> NodeZone;
};

// class Mesh
} // namespace Utilities
} // namespace muq

#endif // ifndef _Mesh_h
