#ifndef QUADRATUREFAMILY1D_H_
#define QUADRATUREFAMILY1D_H_

#include <boost/property_tree/ptree.hpp> // needed here to avoid weird toupper bug on osx

#include "MUQ/config.h"
#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1


#include <memory>
#include <vector>

#include <boost/serialization/access.hpp>
#include <Eigen/Core>

namespace muq {
namespace Utilities {
/**
 * An abstract class that defines a family of quadrature points in 1D. Subclasses actually
 * implement some family of nodes and weights, e.g. Gauss-Legendre integration.
 *
 * Subclasses must provide a way of deriving the nodes and weights, ComputeNodesAndWeights()
 * and a function that provides the precise polynomial order a particular rule is useful
 * for, GetPrecisePolyOrder(). This measure may be approximate if desired, but the idea
 * is that users can ensure that they're selecting a quadrature order sufficient for
 * the order of their particular polynomial, and use this function as a guide. This
 * metric should be monotonically increasing with order of the rules, but need not follow
 * any other pattern.
 *
 * This class provides caching of the rules and a common access interface, through the
 * GetWeights() and GetNodes() functions. This way, code using 1D quadrature rules
 * can easily swap between different types of rules. Coding particular quadrature rules is
 * simple because you merely need to implement the rule-specific method for deriving the
 * nodes and weights.
 *
 * This caching means that instances of these subclasses, in general, should be reused
 * as much as possible.
 *
 *  Expected implementations include Gaussian integration, Clenshaw Curtis, Gauss-Patterson, etc.
 */
class QuadratureFamily1D {
public:

  typedef std::shared_ptr<QuadratureFamily1D> Ptr;

  QuadratureFamily1D();
  virtual ~QuadratureFamily1D();

  /**
   * Returns the weights for 1D quadrature of the input order. Computes the weights as needed.
   * @param order
   * @return A pointer to rowvec of length order, contains the weights for the quadrature rule.
   */
  std::shared_ptr<Eigen::RowVectorXd> GetWeights(unsigned int const order);

  /**
   * Returns the nodes for 1D quadrature of the input order. Computes the weights as needed.
   * @param order
   * @return A pointer to rowvec of length order, contains the nodes for the quadrature rule.
   */
  std::shared_ptr<Eigen::RowVectorXd> GetNodes(unsigned int const order);

  ///Get the order of the polynomial this order integration rule is good for.
  virtual unsigned int                GetPrecisePolyOrder(unsigned int const order) const = 0;
  
  /// Get the maximum level that this quadrature rule is defined for.  Defaults to infinity.
  virtual unsigned int                GetMaximumPolyOrder() const{return std::numeric_limits<unsigned int>::max();};

#if MUQ_PYTHON == 1
  boost::python::list PyGetWeights(unsigned int const order);
  boost::python::list PyGetNodes(unsigned int const order);
#endif
  
private:

  ///Make class serializable
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);

  /**
   * A private function, called if a Get method tries a cache lookup and misses.
   * Calls the virtual method ComputeNodesAndWeights, and stores the result in the cache.
   */
  void AddOrderToCache(unsigned int const order);

  ///Cache of nodes for various orders quadrature rules
  std::vector<std::shared_ptr<Eigen::RowVectorXd> > nodes;

  ///Cache of weights for various order quadrature rules
  std::vector<std::shared_ptr<Eigen::RowVectorXd> > weights;

  ///Implementations must provide a way of deriving the nth rule.

  /**
   * Compute the rule of the input order, and return the nodes and weights in the rowvecs.
   */
  virtual void ComputeNodesAndWeights(unsigned int const                   order,
                                      std::shared_ptr<Eigen::RowVectorXd>& nodes,
                                      std::shared_ptr<Eigen::RowVectorXd>& weights) const = 0;
};
}
}
#endif /* QUADRATUREFAMILY1D_H_ */
