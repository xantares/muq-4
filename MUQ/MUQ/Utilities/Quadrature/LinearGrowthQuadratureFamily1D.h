#ifndef LINEARGROWTHQUADRATUREFAMILY1D_H_
#define LINEARGROWTHQUADRATUREFAMILY1D_H_

#include <boost/serialization/access.hpp>
#include <boost/serialization/base_object.hpp>

#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

namespace muq {
namespace Utilities {
/**
 * This class is designed to provide a different growth rule for 1D
 * quadrature rules. For example, Gaussian quadrature rules progress
 * linearly in size and order, but we might prefer to skip some, and
 * grow a bit more quickly.
 *
 * The constructor takes the other quadrature rule to rescale. Simply
 * calls that rule with rescaled order.
 *
 * New order progression, for example: 1 3 5 7 ...
 * in general, 1, 1+n, 1+2n ...
 */
class LinearGrowthQuadratureFamily1D : public QuadratureFamily1D {
public:

  ///A smart pointer
  typedef std::shared_ptr<LinearGrowthQuadratureFamily1D> Ptr;

  ///Takes the input rule and the number to skip between them.

  /**
   *
   * @param inputRule The base rule to build from.
   * @param inputGrowthInterval The number of orders to skip
   * @return
   */
  LinearGrowthQuadratureFamily1D(QuadratureFamily1D::Ptr inputRule, unsigned int inputGrowthInterval);

  virtual ~LinearGrowthQuadratureFamily1D();

  ///Get the order of the polynomial this order integration rule is good for.
  virtual unsigned int GetPrecisePolyOrder(unsigned int const order) const;

private:

  LinearGrowthQuadratureFamily1D();

  ///Make class serializable
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);


  ///Implementations must provide a way of deriving the nth rule.

  /**
   * Compute the order rule, and return the nodes and weights in the rowvecs.
   */
  virtual void ComputeNodesAndWeights(unsigned int const                   order,
                                      std::shared_ptr<Eigen::RowVectorXd>& nodes,
                                      std::shared_ptr<Eigen::RowVectorXd>& weights) const;

  ///The number of levels to skip.
  unsigned int growthInterval;

  ///The quadrature rule this is based on
  QuadratureFamily1D::Ptr baseQuadrature1DRule;
};
}
}

#endif /* EXPGROWTHQUADRATUREFAMILY1D_H_ */
