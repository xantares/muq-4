
#ifndef TWOWAYPYTHONCONVERTER_H_
#define TWOWAYPYTHONCONVERTER_H_

#include <boost/python.hpp>
#include <boost/static_assert.hpp>

namespace boost {
namespace python {
/// specialization of boost::python::pointee
template<typename T>
struct pointee<std::shared_ptr<T> > {
  typedef T type;
};
} // namespace python
} // namespace boost

//namespace muq {
//namespace Utilities {
/// Construct std::shared_ptr<Derived> from std::shared_ptr<Base>
template<typename Derived, typename Base>
std::shared_ptr<Derived> Construct(std::shared_ptr<Base> const& base, boost::type<std::shared_ptr<Derived> > *)
{
  auto ptr = std::dynamic_pointer_cast<Derived>(base);

  assert(ptr);

  return ptr;
}

/// Construct Source from Target
template<typename Source, typename Target>
std::shared_ptr<Source> ConstructHelper(std::shared_ptr<Target>& target)
{
  return Construct(target, static_cast<boost::type<std::shared_ptr<Source> > *>(NULL));
}

/// Enable implicit conversions between std::shared_ptr<Source> and std::shared_ptr<Target> types using boost::python

/**
 * The conversion of std::shared_ptr<Source> to std::shared_ptr<Target> for boost::python conversion.  Required if
 * Source inherits from Target and we want to call a function foo(std::shared_ptr<Target>) with argument A of type
 * std::shared_ptr<Source>.
 */
template<typename Source, typename Target>
struct TwoWayPythonConverter {
  TwoWayPythonConverter()
  {
    // Enable source->target conversion
    boost::python::implicitly_convertible<std::shared_ptr<Source>, std::shared_ptr<Target> >();

    // Enable target->source conversion, uses muq::Utilities::ConstructHelper
    boost::python::converter::registry::push_back(&TwoWayPythonConverter::Convertible,
                                                  &TwoWayPythonConverter::Construct,
                                                  boost::python::type_id<std::shared_ptr<Source> >());
  }

  /// Check if PyObject contains Source type
  static void* Convertible(PyObject *object)
  {
    std::cout << "Convertible" << std::endl;

    // object is convertible from shared_ptr<Target>->shared_ptr<Source> if: (i) object contains shared_ptr<Target> and
    // (ii) object contains Source.  We must be able to extract the Source pointee type.  Extracting Source directly
    // would result in infinite recursion.
    typedef typename boost::python::pointee<std::shared_ptr<Source> >::type pointee;

    //assert(boost::python::extract<std::shared_ptr<Target> >(object).check());
    //assert(boost::python::extract<pointee>(object).check());

    return boost::python::extract<std::shared_ptr<Target> >(object).check() &&
           boost::python::extract<pointee>(object).check() ? object : NULL;
  }

  /// Convert PyObject to Source type
  static void Construct(PyObject *object, boost::python::converter::rvalue_from_python_stage1_data *data)
  {
    std::cout << "Construct" << std::endl;

    // Obtain a boost::python::handle to the membory block the converter allocated for c++ type
    typedef boost::python::converter::rvalue_from_python_storage<std::shared_ptr<Source> > storageType;
    void *storage = reinterpret_cast<storageType *>(data)->storage.bytes;

    // Extract the target
    std::shared_ptr<Target> target = boost::python::extract<std::shared_ptr<Target> >(object);

    // Allocate the c++ type into the convert's memory block and assign its boost::python::handle to the converter's
    // convertible variable.  The c++ type will be copy constructed from the return of construct function
    data->convertible = new (storage)std::shared_ptr<Source>(ConstructHelper<Source>(target));
  }
};

//} // namespace Utilities
//} // namespace muq

#endif // ifndef TWOWAYPYTHONCONVERTER_H_
