
#ifndef HDF5WRAPPER_H
#define HDF5WRAPPER_H

#include <cxxabi.h>

#include <string>
#include <typeinfo>
#include <iostream>

#include <boost/concept_check.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/filesystem.hpp>

#include <hdf5.h>
#include <hdf5_hl.h>

#include <Eigen/Core>

#include "MUQ/Utilities/VectorTranslater.h"
#include "MUQ/Utilities/HDF5Types.h"
#include "MUQ/Utilities/LogConfig.h"

namespace muq {
namespace Utilities {
///A simple wrapper for a tiny subset of the HDF5 methods, especially for writing Eigen types.

/**
 * \class HDF5Wrapper
 * \ingroup Utilities
 * \brief Provides a high level interface for working with HDF5 files
 * \details This class provides a wrapper around the HDF5 library for working with cross-platform binary HDF5 files.  HDF5 files are structured binary files where multiple datasets are organized in a similar way to filesystems (i.e. folders and files). See the <a href="http://www.hdfgroup.org/HDF5/">HDF5 website</a> for more details.  Another handy utility for working with HDF5 files is the HDFView application available <a href="http://www.hdfgroup.org/downloads/index.html">here</a>.

In order to facilitate the easy collection of data in a single HDF5 file, this class keeps a global file id and writes/reads all data from that file.  Multiple calls to HDF5Wrapper::OpenFile and HDF5Wrapper::CloseFile can be used to interact with multiple files.  Typical usage for writing into a file is something like:
\code{.cpp}
HDF5Wrapper::OpenFile("MyFile.h5");

HDF5Wrapper::CreateGroup("/testGroup");

Eigen::MatrixXd A = Eigen::MatrixXd::Ones(10,10);
HDF5Wrapper::WriteMatrix("/testGroup/MatrixA",A);
HDF5Wrapper::WriteStringAttribute("/testGroup/MatrixA","Creation Date","01-20-2015");

HDF5Wrapper::CloseFile();
\endcode
 *
 * When used with MPI, each node makes its own file and records there, then the rank 0 node combines them into a single file at close.
 **/
class HDF5Wrapper {
public:

  ///Opens the existing file under "HDF5.GlobalFilename" or creates it.
  HDF5Wrapper() = default;
  
  ///Flush on destruction of the wrapper, that is, on program exit
  virtual ~HDF5Wrapper()
  {
    FlushFile();
  }

  /** \brief Open an HDF5 file using information in a ptree.
      \details This function simply calls OpenFile(std::string const& filename, bool useExternalLinks) using parameters stored in the ptree.  The fields used from the ptree are "HDF5.GlobalFilename" and "HDF5.UseExternalLinks"  The filename entry is required and the UseExternalLinks entry defaults to false.
  \see OpenFile(std::string const& filename, bool useExternalLinks = false)
  */
  static void            OpenFile(boost::property_tree::ptree const& properties);

  /** \brief Open an HDF5 file from the specified filename.
      \details This function opens an HDF5 file and sets the global HDF5 file id.  If the file exists, it will be appendend.  If the file does not exist, it will be created.  When MUQ uses MPI parallelism, it will create multiple HDF5 files (one for each node) and collect all of them into a single file when CloseFile() is called.  When useExternalLinks is false, the information in each nodes filed is copied to the master file.  However, when useExternalLinks is true, only a link to the other files is created.  This latter approach can be more efficient when very large files are generated.
    @param[in] filename The name of the HDF5 file to open
    @param[in] useExternalLinks A boolean controlling whether data should be copied in CloseFile.  Note that this variable will only have an effect when MUQ is compiled with MPI.
  
    A warning when using useExternalLinks=true, hdfview doesn't show them correctly!
   */
  static void            OpenFile(std::string const& filename, bool useExternalLinks = false);

  /** Close the HDF5File and write any data in the HDF5 buffer to the file. */
  static void            CloseFile();

  /** Merge all of the data in the other file to the current global HDF5 file. */
  static void            MergeFile(const std::string& otherFile);

  /** Used when this wrapper is compiled with MPI to close and merge files from all of the processes. */
  static void            CloseAndMerge();

  /** Flush any data in the HDF5 buffer to the file. */
  static void            FlushFile();

  /// Get the size of a dataset (rows,cols) -- An empty size zero vector is returned if the dataset could not be found
  static Eigen::VectorXi GetDatasetSize(std::string const datasetName);

  ///Check whether group exists, may only safely check one level at a time
  static bool            DoesGroupExist(std::string const groupName);

  ///Check whether dataset exists, may only safely check one level at a time
  static bool            DoesDataSetExist(std::string const datasetName);

  ///Create the group, including any needed parents
  static void            CreateGroup(std::string const groupName);

  /** \brief Write a matrix to the HDF5 file
      \details This function takes a matrix and writes it to the HDF5 file.  If a dataset in the file already exists with the same name, it will be deleted and overwritten with this matrix.
      \param[in] datasetName The name (including the path) to the dataset where the matrix is to be written.  Note that the path must start with a forward slash "/";
      \param[in] matrix The matrix we want to write to the HDF5 file
  */
  template<typename scalarType, int fixedRows, int fixedCols>
  static void WriteMatrix(std::string const& datasetName, Eigen::Matrix<scalarType, fixedRows, fixedCols> const& matrix)
  {
    if (datasetName.at(0) != '/') {
      std::cerr << "\nERROR: Paths in the HDF5 file must start with a forward slash (/)\n\n";
      assert(datasetName.at(0) == '/');
    }
    boost::filesystem::path datasetPath(datasetName);
    boost::filesystem::path parentGroupPath = datasetPath.parent_path();


    CreateGroup(parentGroupPath.string()); //make sure parent group exists

    if (DoesDataSetExist(datasetName)) {
      H5Ldelete(globalFile, datasetName.c_str(), H5P_DEFAULT);
    }
    FlushFile(); //note this flush is necessary for the delete to actually happen.
    //gotta flip to get the size right, since it's assuming row major not column major
    Eigen::Matrix<scalarType, fixedCols, fixedRows> trans = matrix.transpose();
    hsize_t dimsf[2];
    dimsf[0] = matrix.rows();
    dimsf[1] = matrix.cols();
    H5LTmake_dataset(globalFile, datasetName.c_str(), 2, dimsf, HDF5_Type<scalarType>::GetFlag(), trans.data());
  }
  
  /** \brief Add an attribute to an existing dataset or group
      \details This function adds a scalar valued numeric attribute (such as metadata) to an existing dataset of group.  If the given dataset or group does not exist.  This function does nothing.
  \param[in] datasetName The name of the dataset or group (starting with a "/" and including the path) that you want to attach this attribute to.
  \param[in] attributeName The name of the attribute.
  \param[in] attribute The value of the attribute.
  \see HDF5Wrapper::WriteAttribute
  */
  template<typename attType = double>
  static void WriteScalarAttribute(std::string const& datasetName,
                                   std::string const& attributeName,
                                   attType     const& attribute)
  {
    Eigen::Matrix<attType, 1, 1> vec(1);

    vec << attribute;
    WriteVectorAttribute<attType, 1>(datasetName, attributeName, vec);
  }

  /** \brief Add an attribute to an existing dataset or group
      \details This function adds a string valued attribute (such as metadata) to an existing dataset of group.  If the given dataset or group does not exist.  This function does nothing.
  \param[in] datasetName The name of the dataset or group (starting with a "/" and including the path) that you want to attach this attribute to.
  \param[in] attributeName The name of the attribute.
  \param[in] attribute The value of the attribute.
  \see HDF5Wrapper::WriteAttribute
  */
  static void WriteStringAttribute(std::string const& datasetName,
                                   std::string const& attributeName,
                                   std::string const& attribute);

   /** \brief Add an attribute to an existing dataset or group
       \details This function adds a vector-valued numeric attribute (such as metadata) to an existing dataset of group.  If the given dataset or group does not exist.  This function does nothing.
   \param[in] datasetName The name of the dataset or group (starting with a "/" and including the path) that you want to attach this attribute to.
   \param[in] attributeName The name of the attribute.
   \param[in] attribute The value of the attribute.
   \see HDF5Wrapper::WriteAttribute
   */
  template<typename scalarType, int fixedRows>
  static void WriteVectorAttribute(std::string                           const& datasetName,
                                   std::string                           const& attributeName,
                                   Eigen::Matrix<scalarType, fixedRows, 1> const& attribute)
  {
    if (DoesDataSetExist(datasetName)) {
      auto set_result = H5LTset_attribute(globalFile, datasetName.c_str(),
                                          attributeName.c_str(),
                                          HDF5_Type<scalarType>::GetFlag(), (void *)attribute.data(), attribute.rows());
      assert(set_result >= 0);
      FlushFile();
    } else {
      LOG(INFO) << "Dataset " << datasetName << " does not exist for attribute writing " << attributeName << " = " <<
      attribute;
    }
  }
    
  /** Write an attribute on a group or dataset in the given HDF5 file. If the attribute type is not recognized (i.e. is not a scalar c++ type, an Eigen vector type, or a string), a warning will be generated and no attribute will be written.
      \param[in] datasetName Name of the dataset or group where the attribute is located.  This string should follow the standard form for a path in an HDF5 file (i.e. start with a "/" and include the full path in the file).  If the path does not exist, an assert will fail.  
      \param[in] attributeName The name of the attribute (e.g. "Creation Date", or "Author", etc...)
      \param[in] attribute The value of the attribute to attach to the datasetName path.
  */
  template<typename T>
  static void WriteAttribute(std::string const& datasetName,
                             std::string const& attributeName,
                             T const& attribute)
  {
    if((std::is_scalar<T>::value)&&(std::is_arithmetic<T>::value)){
      WriteScalarAttribute(datasetName,attributeName,attribute);
    }else{
      int status = -1;
      std::unique_ptr<char, void(*)(void*)> res{abi::__cxa_demangle(typeid(attribute).name(), NULL, NULL, &status), std::free};  
      std::cerr << "WARNING: Cannot write attribute " << attributeName << " on " << datasetName <<  " because the attribute has type " << res.get() << ", which was not recognized as a std::string, Eigen vector type, or c++ scalar." << std::endl; 
    }            
  }
     
  static void WriteAttribute(std::string const& datasetName,
                             std::string const& attributeName,
                             std::string const& attribute)
  {
    WriteStringAttribute(datasetName,attributeName,attribute);
  }   
  
  template<typename scalarType, int rowType> 
  static void WriteAttribute(std::string                         const& datasetName,
                             std::string                         const& attributeName,
                             Eigen::Matrix<scalarType,rowType,1> const& attribute)
  {
    WriteVectorAttribute(datasetName,attributeName,attribute);
  }
  
   
  /** Reads a string attribute from the HDF5 file.
      \param[in] datasetName Name of the dataset or group that the attribute is attached to (starting with a "/" and including the path).  If the path does not exist, an assert will fail.
      \param[in] attributeName The name of the attribute to read.  If the attribute does not exist, the HDF5 library may throw an error.
  */
  static std::string GetStringAttribute(std::string const& datasetName,
                                        std::string const& attributeName)
  {
    assert(DoesDataSetExist(datasetName));
    
    char tempStr[256];
    H5LTget_attribute_string(globalFile, datasetName.c_str(), attributeName.c_str(), tempStr );

    return std::string(tempStr);
  }
       
  /** Reads a scalar numeric attribute from the HDF5 file.
      \param[in] datasetName Name of the dataset or group that the attribute is attached to (starting with a "/" and including the path).  If the path does not exist, an assert will fail.
      \param[in] attributeName The name of the attribute to read.  If the attribute does not exist, the HDF5 library may throw an error.
  */                              
  template<typename scalarType>
  static scalarType GetScalarAttribute(std::string const& datasetName,
                                       std::string const& attributeName)
  {
    Eigen::Matrix<scalarType, Eigen::Dynamic, 1> att = GetVectorAttribute<scalarType>(datasetName, attributeName);
    assert(att.size() == 1);

    return att(0);
  }

  /** Reads a vector-valued attribute from the HDF5 file.
      \param[in] datasetName Name of the dataset or group that the attribute is attached to (starting with a "/" and including the path).  If the path does not exist, an assert will fail.
      \param[in] attributeName The name of the attribute to read.  If the attribute does not exist, the HDF5 library may throw an error.
  */
  template<typename scalarType>
  static Eigen::Matrix<scalarType, Eigen::Dynamic, 1> GetVectorAttribute(std::string const& datasetName,
                                                                         std::string const& attributeName)
  {
    assert(DoesDataSetExist(datasetName));

    int  rank[1];
    auto get_ndim = H5LTget_attribute_ndims(globalFile, datasetName.c_str(), attributeName.c_str(), rank);
    assert(get_ndim >= 0);

    std::vector<scalarType> data(rank[0]);

    auto get_att = H5LTget_attribute(globalFile, datasetName.c_str(),
                                     attributeName.c_str(), HDF5_Type<scalarType>::GetFlag(), data.data());

    assert(get_att >= 0);

    Translater<std::vector<scalarType>, Eigen::Matrix<scalarType, Eigen::Dynamic, 1> > trans(data);

    return *trans.GetPtr();
  }

  /** Read a matrix from the HDF5 file.  Unless the matrix is full of doubles, the scalarType template parameter should be set to the expected type.  Also, fixed size matrices can be returned by setting the fixedRows and/or fixedCols template parameters to something other than Eigen::Dynamic.
      \param[in] datasetName Name of the dataset or group where the matrix is stored in the HDF5 file.  If the dataset does not exist, an assert will fail.  
      \return An Eigen matrix containing the matrix stored at datasetName
  */
  template<typename scalarType = double, int fixedRows = Eigen::Dynamic, int fixedCols = Eigen::Dynamic>
  static Eigen::Matrix<scalarType, Eigen::Dynamic, Eigen::Dynamic> ReadMatrix(std::string const& datasetName)
  {
    Eigen::Matrix<scalarType, fixedCols, fixedRows> pt;
    hsize_t dims[2];
    dims[0] = 0;
    dims[1] = 0;
    
    assert(DoesDataSetExist(datasetName));

    auto get_info_result = H5LTget_dataset_info(globalFile, datasetName.c_str(), dims, NULL, NULL);
    assert(get_info_result >= 0);

    hid_t datasetId   = H5Dopen2(globalFile, datasetName.c_str(), H5P_DEFAULT);
    hid_t datasetType = H5Dget_type(datasetId);
    H5Dclose(datasetId);

    if (!H5Tequal(datasetType, HDF5_Type<scalarType>::GetFlag())) {
      size_t len = 28;
      char   fileType[28], expType[28];
      H5LTdtype_to_text(datasetType,                      fileType, H5LT_DDL, &len);
      H5LTdtype_to_text(HDF5_Type<scalarType>::GetFlag(), expType,  H5LT_DDL, &len);

      std::cerr <<
      "ERROR in HDF5Wrapper:\nTrying to read dataset with different value than function.  Expected type " <<
      expType << " but dataset has type " << fileType << std::endl;
      assert(H5Tequal(datasetType, HDF5_Type<scalarType>::GetFlag()));
    }


    if (dims[1] > 0) {
      pt.resize(dims[1], dims[0]);
    } else {
      pt.resize(dims[0], 1);
    }
    auto read_result = H5LTread_dataset(globalFile, datasetName.c_str(),
                                        HDF5_Type<scalarType>::GetFlag(), (void *)pt.data());
      assert(read_result >= 0);

    if(dims[1]>0){
      // hdf5 and eigen have different memory convention so we read in the matrix transpose
      return pt.transpose();
    }else{
      return pt;    
    }
  }

  ///Write the input matrix into the dataset at the input row and column. It must already exist and be large enough.

  static void WritePartialMatrix(std::string                                               const& datasetName,
                                 Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic> const& matrix,
                                 int                                                              row,
                                 int                                                              col);

  static hid_t globalFile;

  static std::string filename;
  static bool useExternalLinks;
};
 
 




}
}

#endif // HDF5WRAPPER_H
