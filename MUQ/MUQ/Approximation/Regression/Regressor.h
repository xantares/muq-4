#ifndef REGRESSOR_H_
#define REGRESSOR_H_

#include "MUQ/config.h"

#include <boost/property_tree/ptree.hpp> // this needs to be here to avoid a weird "toupper" bug on osx

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

#include "MUQ/Modelling/ModPieceTemplates.h"

namespace muq {
  namespace Approximation {
    class Regressor : public muq::Modelling::OneInputJacobianModPiece {
    public:

      /**
	 @param[in] inDim The input size.
	 @param[in] outDim The output size.
       */
      Regressor(unsigned int const inDim, unsigned int const outDim);

    virtual ~Regressor() = default;
	
	  /// Fit the approximation (regression) given input points and their outputs 
      /**
	 Can be overriden.  Default is to fit with the center equal to zero.
	 @param[in] xs The points (each column is a point)
	 @param[in] ys The output at each point (each column is an output)
       */
      virtual void Fit(Eigen::MatrixXd const& xs, Eigen::MatrixXd const& ys);

      /// Fit the approximation (regression) given input points and their outputs 
      /**
	 Centered fit must be implemented by children.  The center shifts the origin.  Each regression has a different way of fitting and can use the center differently.
	 @param[in] xs The points (each column is a point)
	 @param[in] ys The output at each point (each column is an output)
	 @param[in] center Shift the origin to this point.
       */
      virtual void Fit(Eigen::MatrixXd const& xs, Eigen::MatrixXd const& ys, Eigen::VectorXd const& center) = 0;

#if MUQ_PYTHON == 1
      /// Python wrapper for fit without centering
      /**
	 @param[in] xs The points (each column is a point)
	 @param[in] ys The output at each point (each column is an output)
       */
      void PyFit(boost::python::list const& xs, boost::python::list const& ys);

      /// Python wrapper for fit with centering
      /**
	 @param[in] xs The points (each column is a point)
	 @param[in] ys The output at each point (each column is an output)
	 @param[in] center Shift the origin to this point.
       */
      void PyCenteredFit(boost::python::list const& xs, boost::python::list const& ys, boost::python::list const& center);

#endif // MUQ_PYTHON == 1

    private:
    };
  } // namespace Approximation
} // namespace muq

#endif
