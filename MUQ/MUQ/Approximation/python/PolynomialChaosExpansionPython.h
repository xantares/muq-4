#ifndef POLYNOMIALCHAOSEXPANSIONPYTHON_H_
#define POLYNOMIALCHAOSEXPANSIONPYTHON_H_

#include "MUQ/Approximation/pce/PolynomialChaosExpansion.h"

namespace muq {
  namespace Approximation {
    void ExportPolynomialChaosExpansion();
  } // namespamce Approximation
} // namespace muq

#endif
