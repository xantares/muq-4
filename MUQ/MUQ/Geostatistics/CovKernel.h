#ifndef CovKernel_h_
#define CovKernel_h_

#include <boost/property_tree/ptree.hpp>
#include <Eigen/Core>

#include "MUQ/config.h"

#if MUQ_PYTHON==1
# include <boost/python.hpp>
# include <boost/python/def.hpp>
# include <boost/python/class.hpp>

# include "MUQ/Utilities/python/PythonTranslater.h"
#endif

#include "MUQ/Utilities/multiIndex/MultiIndexFactory.h"
#include "MUQ/Utilities/multiIndex/MultiIndexSet.h"

#include "MUQ/Utilities/Quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussHermiteQuadrature1D.h"

namespace muq {
namespace Utilities {
template<unsigned int dim>
class Mesh;
}
}


namespace muq {
namespace Geostatistics {
class CovKernel;
typedef std::shared_ptr<CovKernel> CovKernelPtr;


/**
 *  @class CovKernel
 *
 *  @brief This is a basic class for building covariance matrices from a covariance kernel.
 *
 *  Two dimensional Gaussian random fields can be describes just like any other Gaussian distribution, with a mean and
 *     covariance.  However, for random fields, the covariance matrix usually has additional structure.  This class
 *     serves as a base for describing covariance matrices through a covariance kernel.  This base class provides an
 *     interface for other covariance kernels, but implements power law kernels such as a Gaussian or Exponential
 * kernel.
 *      In this abstract base class no assumptions on the covariance kernel are made except that given two locations x1
 *     and x2, the covariance cov(x1,x2) can be computed.
 *
 *
 *  All child classes must provide a kernel function, that takes two points x and y and returns the covariance between
 *     the field at those points.  This is implemented in the virtual kernel function.
 *
 *
 */
class CovKernel : public std::enable_shared_from_this<CovKernel> {
public:

  /** Construct the covariance matrix using the points (x,y) stored in the position vectors.  The result is written into
   *  the Cov matrix.
   *  @param[in] x Vector of x location to evaluate the covariance kernel (size M)
   *  @param[in] y Vector of y locations to evaluate the covariance kernel (size N)
   *  @return MxN matrix filled in with discretized kernel values -- useful for quadrature schemes
   */
  virtual Eigen::MatrixXd BuildCov(const std::vector<Eigen::VectorXd>& x,
                                   const std::vector<Eigen::VectorXd>& y = std::vector<Eigen::VectorXd>()) const;

#if MUQ_PYTHON==1

  /// A wrapper around the BuildCov function

  /**
   * Builds a covariance matrix given one set of points
   */
  boost::python::list PyBuildCovOneInput(boost::python::list const& x);

  /// A wrapper around the BuildCov function

  /**
   * Builds a covariance matrix given two sets of points
   */
  boost::python::list PyBuildCovTwoInputs(boost::python::list const& x, boost::python::list const& y);

#endif


  template<typename T>
  Eigen::MatrixXd BuildCov(std::shared_ptr<T> x) const
  {
    auto ptr1 = std::dynamic_pointer_cast < muq::Utilities::Mesh < 1 >> (x);

    if (ptr1) {
      return BuildCov(ptr1);
    }

    auto ptr2 = std::dynamic_pointer_cast < muq::Utilities::Mesh < 2 >> (x);
    if (ptr2) {
      return BuildCov(ptr2);
    }

    auto ptr3 = std::dynamic_pointer_cast < muq::Utilities::Mesh < 3 >> (x);
    if (ptr3) {
      return BuildCov(ptr3);
    }
  }

  /** Construct the covariance matrix using points (x,y) stored in some form of Eigen matrix.  Each column is a point,
   *  so the number of rows should be the point dimension.  The covariances are written into the Cov matrix.
   *  @param[in] x Vector of x location to evaluate the covariance kernel (size dimxM)
   *  @param[in] y Vector of y locations to evaluate the covariance kernel (size dimxN)
   *  @return MxN matrix filled in with discretized kernel values -- useful for quadrature schemes
   */
  template<typename T>
  Eigen::MatrixXd BuildCov(const T& x, const T& y = T()) const
  {
    // get the size of the covariance
    unsigned int M = x.cols();
    unsigned int N = y.cols();


    // create the output covariance matrix
    Eigen::MatrixXd CovOut;

    // if N is zero, no y was passed and we will create a symmetric covariance based only on x
    if (N == 0) {
      // the output matrix will be square in this case
      N      = M;
      CovOut = Eigen::MatrixXd::Zero(M, N);

      // ////////////
      // Fill in the lower triangular portion

      // column loop
      for (unsigned int j = 0; j < N; ++j) {
        // row loop
        for (unsigned int i = j; i < M; ++i) {
          CovOut(i, j) = Kernel(x.col(i), x.col(j));
        }
      }

      // ////////////
      // Copy lower triangular portion to upper triangular part

      // column loop
      for (unsigned int j = 0; j < N; ++j) {
        // row loop
        for (unsigned int i = 0; i < j; ++i) {
          CovOut(i, j) = CovOut(j, i);
        }
      }


      // compute the covariance between the points in x and the points in y
    } else {
      CovOut = Eigen::MatrixXd::Zero(M, N);

      // column loop
      for (unsigned int j = 0; j < N; ++j) {
        // row loop
        for (unsigned int i = 0; i < M; ++i) {
          CovOut(i, j) = Kernel(x.col(i), y.col(j));
        }
      }
    }

    // add a little nugget to ensure matrix is SPD
    CovOut += 1.0e-10 * Eigen::MatrixXd::Identity(M, N);

    // return the constructed covariance matrix
    return CovOut;
  }

  ///build a matrix of the partials of the covariance between all the xs and x, wrt changes in x
  virtual Eigen::MatrixXd BuildCovSpatialDerivative(Eigen::MatrixXd const& xs, Eigen::VectorXd const& x);

  ///Return the gradient of the covariance of (x,xWrt) against changes in xWrt
  virtual Eigen::VectorXd KernelSpatialDerivative(Eigen::VectorXd const& x, Eigen::VectorXd const& xWrt) = 0;

  /** Construct the covariance matrix using a general mesh
   *  @param[in] MeshPtr A pointer to the mesh
   *  @return Cov covariance matrix coming from covariance kernel discretized at the elements of the mesh. The element
   *     locations and not the node locations are used.
   */
  template<unsigned int dim>
  Eigen::MatrixXd BuildCov(std::shared_ptr < muq::Utilities::Mesh < dim >> &MeshPtr) const;

  /** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh.  Depending on the characteristics
   *  of the covariance kernel and mesh, different approaches are used in an attempt to be as efficient as possible.
   *  @param[in] MeshPtr A pointer to the mesh of interest with N elements
   *  @return NxK matrix holding the weighted KL modes.
   */
  template<unsigned int dim>
  Eigen::MatrixXd BuildKL(std::shared_ptr < muq::Utilities::Mesh < dim >> &MeshPtr, unsigned int NumBasis);

  /** Construct the discretized Karhunen-Loeve modes of the kernel on the given points.  Depending on the
   * characteristics
   *  of the covariance kernel and mesh, different approaches are used in an attempt to be as efficient as possible.
   *  @param[in] pts The points to compute the kernel on
   *  @return NxK matrix holding the weighted KL modes.
   */
  Eigen::MatrixXd BuildKL(std::vector<Eigen::VectorXd> const& pts, unsigned int NumBasis);

  /** This is the general interface all child classes must follow.
   *  Evaluate the base kernel by computing the distance between x and y and then calling DistKernel
   *  @param[in] x Position 1
   *  @param[in] y Position 2
   *  @return covariance kernel evaluated at ||x-y||
   *
   *  Note that use of Eigen::Ref prevents copies from being generated for these columns when we have
   *  to walk pairwise through a sample-set to make a cov matrix. Done because it was a performance dominating issue.
   */
  virtual double  Kernel(const Eigen::Ref<const Eigen::VectorXd>& x,
                         const Eigen::Ref<const Eigen::VectorXd>& y) const = 0;


  /** Set the covariance kernel parameters from a vector of parameters.
   *  @param[in] theta MuqVector or parameter values for the covariance kernel.
   */
  virtual void SetParms(const Eigen::VectorXd& theta) = 0;


  /** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh using a Hierarchical matrix
   *  approximation to the covariance matrix.
   *  @param[in] MeshPtr A pointer to the mesh of interest with N elements
   *  @return NxK matrix holding the weighted KL modes.
   */
  template<unsigned int dim>
  Eigen::MatrixXd BuildKL_Hierarchical(std::shared_ptr < muq::Utilities::Mesh < dim >> &MeshPtr,
                                       unsigned int NumBasis);

  /** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh using the full dense covariance
   *  matrix.
   *  @param[in] MeshPtr A pointer to the mesh of interest with N elements
   *  @return NxK matrix holding the weighted KL modes.
   */
  template<unsigned int dim>
  Eigen::MatrixXd BuildKL_FullCov(std::shared_ptr < muq::Utilities::Mesh < dim >> &MeshPtr,
                                  unsigned int NumBasis) const;

  /** Construct the discretized Karhunen-Loeve modes of the kernel on the given points using the full dense covariance
   *  matrix.
   *  @param[in] pts The points on which we want to build the cov matrix
   *  @return NxK matrix holding the weighted KL modes.
   */
  Eigen::MatrixXd BuildKL_FullCov(std::vector<Eigen::VectorXd> const& pts, unsigned int NumBasis) const;

  /** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh assuming a rectangular mesh but not
   *  a separable kernel.  A tensor product of clenshaw curtis quadrature points is used in a Nystrom approach.
   *  @param[in] MeshPtr A pointer to the mesh of interest with N elements
   *  @return NxK matrix holding the weighted KL modes.
   */
  template<unsigned int dim>
  Eigen::MatrixXd BuildKL_TensorQuad(std::shared_ptr < muq::Utilities::Mesh < dim >> &MeshPtr,
                                     unsigned int NumBasis) const;

  /** Construct the discretized Karhunen-Loeve modes of the kernel on the given mesh assuming both a rectangular mesh
   *  and a separable kernel.  The KL modes are computed in each direction and then tensor products are taken to build
   * up
   *  the 2 or 3 dimensional modes.
   *  @param[in] MeshPtr A pointer to the mesh of interest with N elements
   *  @return NxK matrix holding the weighted KL modes.
   */
  template<unsigned int dim>
  Eigen::MatrixXd BuildKL_Separable(std::shared_ptr < muq::Utilities::Mesh < dim >> &MeshPtr,
                                    unsigned int NumBasis) const;


  /** Regardless of what type of kernel this is, we can try to estimate a "length scale" or something related to the
   *  length.  This function should return an approximation to the shortest distance required to get the covariance to
   *  drop below a small (i.e. 1e-10) threshold.
   *  @return An approximate "lengthscale"
   */
  virtual double EstimateLength() const = 0;


  //            /** When using BuildKL_Separable we need to compute KL modes in one dimension.  This function uses one
  // dimensional Gauss-Legendre quadrature within the Nystrom method to compute the modes.
  //             @param[in] NodePos The 1d node positions where the KL modes need to be evaluated.
  //             @return The one dimensional KL modes
  //             */
  //            virtual Eigen::MatrixXd BuildKL1d(const Eigen::VectorXd &NodePos, unsigned int NumBasis) const;
  //
  //            bool separable;


  template<unsigned int dim>
  void GetQuadLocs(unsigned int MaxOrder,
                   Eigen::Matrix<double, dim, Eigen::Dynamic>& pts,
                   Eigen::Matrix<double, 1, Eigen::Dynamic>& wts,
                   const Eigen::Matrix<double, dim, 1>& lb,
                   const Eigen::Matrix<double, dim, 1>& ub,
                   char quadType = 'L') const
  {
    auto Indices = muq::Utilities::MultiIndexFactory::CreateFullTensor(dim, MaxOrder-1);

    // get the one dimensional nodes and weights
    std::shared_ptr<muq::Utilities::GaussianQuadratureFamily1D> QuadRule;

    if ((quadType == 'h') || (quadType == 'H')) {
      QuadRule = std::shared_ptr<muq::Utilities::GaussianQuadratureFamily1D>(
        new muq::Utilities::GaussHermiteQuadrature1D);
    } else if ((quadType == 'l') || (quadType == 'L')) {
      QuadRule = std::shared_ptr<muq::Utilities::GaussianQuadratureFamily1D>(
        new muq::Utilities::GaussLegendreQuadrature1D);
    } else {
      std::cerr <<
          "\nERROR: invalid quadrature type passed to GetQuadLocs.  Options are H (for Hermite) or L (for Legendre).\n\n";
    }

    // the nodes and weights will be returned as boost smart pointers
    std::shared_ptr<Eigen::RowVectorXd> nodes(QuadRule->GetNodes(MaxOrder));
    std::shared_ptr<Eigen::RowVectorXd> weights(QuadRule->GetWeights(MaxOrder));

    // use the multiindex family to loop through the nodes and weights to compute the multi-dimensional nodes and
    // weights
    unsigned int NumDimPts = Indices->GetNumberOfIndices();
    pts.resize(dim, NumDimPts);
    wts.resize(1, NumDimPts);

    // loop over all the higher dimensional quadrature points
    for (unsigned int i = 0; i < NumDimPts; ++i) {
      // initialize the weight to 1.0
      wts(0, i) = 1.0;

      // grab the current index set
      Eigen::RowVectorXu TempInd = Indices->IndexToMulti(i);

      // loop over the dimensions, computing the weights and nodes
      for (unsigned int d = 0; d < dim; ++d) {
        double L = ub[d] - lb[d];
        pts(d, i)  = lb[d] + 0.5 * (1.0 + (*nodes)[TempInd[d]]) * L;
        wts(0, i) *= (*weights)[TempInd[d]] * L * 0.5;
      }
    }
  }

  virtual ~CovKernel() = default;

protected:

  CovKernel() {}

private:

  typedef std::map < std::string, std::function < std::shared_ptr<CovKernel > (boost::property_tree::ptree&) >>
                                                                  AvailableKernelMap;

public:

///Globally available way to get the map that tells you what MCMC methods are available.
                                                                  static std::shared_ptr<AvailableKernelMap>
                                    GetKernelMap();


  static std::shared_ptr<CovKernel> Create(boost::property_tree::ptree& properties);

  virtual Eigen::VectorXd           GetParms() = 0;

  virtual void                      BuildGradMat(const Eigen::MatrixXd       & xs,
                                                 std::vector<Eigen::MatrixXd>& GradMat) const = 0;
};

// class CovKernel
} // namespace Geostatistics
} //namespace muq


//put this is the class definition (.h)
#define REGISTER_KERNEL_CONSTRUCTOR(NAME) static std::shared_ptr<CovKernel> Create( \
    boost::property_tree::ptree & properties)                                       \
  {                                                                                 \
    return std::make_shared<NAME>(properties);                                      \
  };

//and this in the .cpp NOTE needs a using namespace call
#define REGISTER_KERNEL_DEF_TYPE(NAME)          \
  static auto registered_pair_kernel_ ## NAME = \
    muq::Geostatistics::CovKernel::GetKernelMap()->insert(std::make_pair(# NAME, NAME::Create));


#endif // ifndef CovKernel_h_
