
#ifndef _ConstraintList_h
#define _ConstraintList_h

// std library includes
#include <iostream>
#include <vector>
#include <memory>

#include <Eigen/Core>

#include "MUQ/Optimization/Constraints/ConstraintBase.h"
#include "MUQ/Optimization/Constraints/LinearConstraint.h"
#include "MUQ/Optimization/Constraints/BoundConstraint.h"

namespace muq {
namespace Optimization {
class ConstraintList : public ConstraintBase {
public:

  ConstraintList(int dim) : ConstraintBase(dim, 0) {}

  /** Evaluate all the constraints and put the results in a single vector.  Note that we assume all constraints are of
   * the form g(x)=0 of g(x)<=0.
   *  @param[in] xc The current optimization point we want to evaluate the constraints at.
   *  @return A vector holding the list of constraint values
   */
  virtual Eigen::VectorXd eval(const Eigen::VectorXd& xc);

  /** Evaluate the action of the Jacobian of this constraint on a vector.  Another way of thinking of this is that we
   * want to compute the gradient of some objective with respect to the input to this constraint and vecIn is the
   * sensitivity of that objective to the output of this constraint.  In that setting, this function is one step in a
   * chain rule.  For example, if we want to evaluate df/dx =df/dg dg/dx.  This function would take df/dg as input
   * (vecIn) and compute df/dg*dg/dx
   *  @param[in] xc The location where we want to perform the evaluation -- i.e. where do we take the derivative?
   *  @param[in] vecIn The input vector that may represent df/dg
   *  @return The Jacobian of this constraint applied to vecIn
   */
  virtual Eigen::VectorXd ApplyJacTrans(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn) override;

  virtual Eigen::MatrixXd getJacobian(const Eigen::VectorXd& xc) override;

  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& sensitivity) override;

  /** Add a new constraint to this list.
   *  @param[in] Input A shared pointer to an instance of ConstraintBase
   */
  virtual void            Add(std::shared_ptr<ConstraintBase> Input);


  virtual int             NumBound() const
  {
    return Bounds.size();
  }

  virtual int NumLinear() const
  {
    return Linears.size();
  }

  virtual int NumNonlinear() const
  {
    return Nonlinears.size();
  }

  // keep a vector of the nonlinear constraints
  std::vector<std::shared_ptr<ConstraintBase> > Nonlinears;

  // keep a vector of the linear constraints
  std::vector<std::shared_ptr<LinearConstraint> > Linears;

  // keep a vector of the bound constraints
  std::vector<std::shared_ptr<BoundConstraint> > Bounds;
};

// class ConstraintList
} // namespace Optimization
} // namespace muq


#endif // ifndef _ConstraintList_h
