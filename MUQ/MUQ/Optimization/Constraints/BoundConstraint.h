
#ifndef _BoundConstraint_h
#define _BoundConstraint_h

#include "MUQ/Optimization/Constraints/ConstraintBase.h"

namespace muq {
namespace Optimization {
/** @class BoundConstraints
 *   @ingroup Optimization
 *   @brief Implementation of bound constraints
 *   @details This class provides a specialization of a LinearConstraint for a BoundConstraint.  Many times algorithms
 * can handle bound constraints directly but require less efficient methods for LinearConstraints.  This class provides
 * the necessary structure to handle a Bound constraint directly.
 */
class BoundConstraint : public ConstraintBase {
public:

  /** Construct the bound constraint
   *  @param[in] dimIn How big is the input vector?
   *  @param[in] i What dimension of the input vector are we interested in?
   *  @param[in] val The value of the bound.
   *  @param[in] uplo Is val a lower bound?  uplo=false for upper and true for lower.
   */
  BoundConstraint(int dimIn, int i, double valIn, bool uplo);

  /** Evaluate the linear constraint.
   *  @param[in] xc Input vector
   *  @return A*xc-b
   */
  virtual Eigen::VectorXd eval(const Eigen::VectorXd& xc);

  /** Evaluate the action of the Jacobian of this constraint on a vector.  Another way of thinking of this is that we
   * want to compute the gradient of some objective with respect to the input to this constraint and vecIn is the
   * sensitivity of that objective to the output of this constraint.  In that setting, this function is one step in a
   * chain rule.  For example, if we want to evaluate df/dx =df/dg dg/dx.  This function would take df/dg as input
   * (vecIn) and compute df/dg*dg/dx
   *  @param[in] xc The location where we want to perform the evaluation -- i.e. where do we take the derivative?
   *  @param[in] vecIn The input vector that may represent df/dg
   *  @return The Jacobian of this constraint applied to vecIn
   */
  virtual Eigen::VectorXd ApplyJacTrans(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn);

  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& sensitivity) override;

  /** Return the dimension this bound operates on.
   *  @return An integer containing the index where this bound applies.
   */
  int                     WhichDim() const;

  /** Get the value of this bound.
   *  @return The value of the upper or lower bound
   */
  double                  GetVal() const;

  /** Is this a lower bound?
   *  @return True if this is a lower bound, false if upper bound
   */
  bool                    isLower() const;

protected:

  int dimSpec; // what dimension of the input vector is this constraint on?
  double val;
  bool   isLow;
};
} // namespace Optimization
} // namespace muq

#endif // ifndef _BoundConstraint_h
