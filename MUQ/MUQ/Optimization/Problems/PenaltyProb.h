
#ifndef _PenaltyProb_h
#define _PenaltyProb_h

#include "MUQ/Optimization/Problems/OptProbBase.h"

namespace muq {
namespace Optimization {
/** @class PenaltyProb
 *  @ingroup Optimization
 *  @brief An unconstrained penalized subproblem
 *  @details This class provides the definition of an unconstrained penalized objective that is used inside the
 * muq::Optimization::Penalty solver to solve generally constrained optimization problems using an arbitrary solver.
 */
class PenaltyProb : public OptProbBase {
public:

  /** Construct the penalized subproblem using the original objective and constraints defined in prob, as well as the
   * penalty.
   *  @param[in] prob The original constrained optimization problem.
   *  @param[in] penaltyCoeffIn penalty parameter set in Penalty method's outer iteration
   */
  PenaltyProb(std::shared_ptr<OptProbBase> prob, double penaltyCoeffIn);

  /** Evaluate the penalized objective.
   *  @param[in] xc The location to evaluate the objective.
   *  @return The value of the penalized objective at xc.
   */
  virtual double eval(const Eigen::VectorXd& xc);

  /** Evaluate the penalized objective gradient.
   *  @param[in] xc The location to compute the objective and gradient.
   *  @param[out] gradient A vector that will be filled with the gradient.
   *  @return xc The value of the penalized objective at xc.
   */
  virtual double grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient);

private:

  double penaltyCoeff;

  // the underlying objective function and constraints
  std::shared_ptr<OptProbBase> baseProb;
};

// class PenaltyProb
} //namespace Optimization
} // namespace muq


#endif // ifndef _PenaltyProb_h
