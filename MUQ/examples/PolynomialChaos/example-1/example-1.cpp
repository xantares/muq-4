/** \example polynomialchaos-example-1.cpp
 *  <h1>Introduction</h1>
 *  <p> This example will demonstrate the basic construction of a Polynomial Chaos expansion (PCE) using the adaptive
 * Smolyak pseudospectral technique.
 * This might be applicable is you have a ModPiece whose output is smooth, but is too expensive to evaluate repeatedly,
 * say, within MCMC.
 * Therefore, you might consider building a PCE that approximates it, and use the PCE instead, which is cheap to
 * evaluate.</p>
 *
 * <p>Recall that PCEs are orthogonal polynomials,
 * \$ f(x) = \sum_i f_i \Psi_i(x)\$, where the polynomials \$\Psi_i(x)\$ are chosen with respect to some input
 * distribution. We must provide the function,
 * of interest and a specification of that distribution, and we recieve back a replacement ModPiece. We will demonstrate
 * this process using a small test function.
 * </p>
 *
 *
 *  <h3>Line by line explanation</h3>
 *  <p>See example-1.cpp for finely documented code.</p>
 *
 *
 */
//Standard includes
#include <Eigen/Core>
#include <Eigen/QR>
#include <iostream>
#include <MUQ/Modelling/ModPieceTemplates.h>

//A tool from modelling to avoid unnecessary, repetitive calls to a ModPiece.
#include <MUQ/Modelling/CachedModPiece.h>

//Holds the actual representation of PCEs.
#include <MUQ/Approximation/pce/PolynomialChaosExpansion.h>

//The SmolyakPCEFactory does most of the work of creating the PCE.
#include <MUQ/Approximation/smolyak/SmolyakPCEFactory.h>

//Has a useful utility function for setting up PCE problems.
#include <MUQ/Approximation/utilities/PolychaosXMLHelpers.h>

//Logging utilities.
#include <MUQ/Utilities/LogConfig.h>


/* use the muq::modelling namespace
 */
using namespace muq::Modelling;
using namespace muq::Utilities;

//Pull in the namespace of the PolynomialChaos package.
using namespace muq::Approximation;

/* use the std namespace for cout and endl
 */
using namespace std;


//A small test function from Paul Constantine's thesis, 2009. See Modelling/example-1 for more details on creating
// ModPieces. This function is surprising challenging - it is simple, but highly coupled, so takes a fairly large number
// of terms to approximate.
class ExampleModPiece : public OneInputNoDerivModPiece {
public:

  /*
   * This function has two inputs and two outputs.
   */
  ExampleModPiece() : OneInputNoDerivModPiece(2, 2) {}

  /* We define a default virtual destructor for the model to eliminate issues when the destructor is called from
   * a pointer to a parent of this class (a common occurence in MUQ).
   */
  virtual ~ExampleModPiece() = default;

private:

  /*
   * The implementation of the model, which involves solving a small 2x2 inverse.
   */
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    //Create a 2x2 matrix
    Eigen::Matrix2d opMat;

    //Fill the matrix based on the values
    opMat << 4, input(1) + input(0) * input(1), input(0) + input(0) * input(1), 1;

    //The right hand side is constant.
    Eigen::Vector2d rhs;
    rhs << 1, 1;
    //Use Eigen's linear solver based on QR decompositions and return the result.
    return opMat.colPivHouseholderQr().solve(rhs);
  }
};


/* Now that we have our model, let's build a PCE of it.
 */
int main(int argc, char **argv)
{
  //Initialization boilerplate for logging. Works regardless of whether GLOG is installed.
  muq::Utilities::InitializeLogging(argc, argv);

  /*Create an instance of the ModPiece, getting a shared_ptr to it. Use C++11's fancy auto to avoid typing the return
   * type, since it's obvious.
   */
  auto trueModel = make_shared<ExampleModPiece>();

  /*
   * We need to tell the system what the distribution we want is. We have two basic options, uniform over [-1,1] and
   * standard normal. We assume that your function takes a combination of these two. If your function needs to shift or
   * scale the canonical distributions, do that inside the ModPiece. For the sake of the example, let's assume the first
   * input is uniform and the second is normal.
   *
   * We can use the function muq::PolynomialChaos::ConstructVariableCollection to help create the VariableCollection
   * object we need, using sensible defaults. It takes a boost::property_tree:ptree, indicating in a relatively simple
   * way what we want. In this case, we want the xml:
   *  <Variable>
   *    <index>1</index> <!-- Specifies the relative ordering of the variable block -->
   *    <type>Legendre</type> <!-- Type of variables, Gaussian or Legendre. -->
   *    <count>1</type> <!-- How many in this block, defaults to 1-->
   *  </Variable>
   *  <Variable>
   *    <index>2</index> <!-- Specifies the relative ordering of the variable block -->
   *    <type>Gaussian</type> <!-- Type of variables, Gaussian or Legendre. -->
   *    <count>1</type> <!-- How many in this block, defaults to 1-->
   *  </Variable>
   */

  //We have to create the two subtrees first, using the put command. This is needed because the name of the roots is the same, and
  // it uses the index field to figure out which is first.
  boost::property_tree::ptree subtree_legendre;

  subtree_legendre.put("index", 1);
  subtree_legendre.put("type", "Legendre");
  subtree_legendre.put("count", 1);
  boost::property_tree::ptree subtree_gaussian;
  subtree_gaussian.put("index", 2);
  subtree_gaussian.put("type", "Gaussian");
  subtree_gaussian.put("count", 1);

  //Now we can put them together. The order doesn't matter.
  boost::property_tree::ptree ptree;
  ptree.add_child("Variable", subtree_legendre);
  ptree.add_child("Variable", subtree_gaussian);

  //Now we can use the model to create the VariableCollection we need. This automatically constructs
  //the quadrature rules and polynomials that correspond to our choices.
  auto variableCollection = ConstructVariableCollection(ptree);

  //Wrap the model with a cache, so it won't call trueModel at the same point twice. We could omit this step, but
  //the (0,0) point is used repeatedly, and that might be expensive in real use.
  auto cachedTrueModel = make_shared<CachedModPiece>(trueModel);

  //Create the Smolyak object which can actually perform the pseudospectral construction and make a PCE for us.
  auto pceFactory = make_shared<SmolyakPCEFactory>(variableCollection, cachedTrueModel);

  //Now we can actually make the PCE. Start the factory, asking it to run until it reaches an error indicator of 1e-9.
  //Begin the construction with a simplex of order 2. The indicator is NOT the L_2 error, or anything that simple, but does
  //correlate with error. There are several other Start--- methods, and we must use one of them.
  std::shared_ptr<PolynomialChaosExpansion> pce = pceFactory->StartAdaptiveToTolerance(2, 1e-9);

  //Just for fun, let's have it refine for another fraction of a second, overwriting the pce we just got.
  //Note that we're not using the StartAdaptiveTimed method, since we've already started.
  //The non- Start--- methods can be called as many times as desired and may be combined in any way.
  pce = pceFactory->AdaptForTime(0.1, 0);

  //We can ask how many unique model runs are contained in the CachedModPiece. 
  cout << "Constructing the approximation used " << cachedTrueModel->GetNumOfEvals() <<
  " evaluations of the true model." << endl;

  //Now we have a ModPiece as usual. Let's see what it can do!
  
  //We can compute it's value at test points.
  Eigen::Vector2d testPoint { .2, -.3 };
  cout << "The pce approximation is " << pce->Evaluate(testPoint).transpose() << endl;
  
  //And it should be reasonably close to the actual value.
  cout << "The real model is " << trueModel->Evaluate(testPoint).transpose() << endl;
  cout << "The approximation has relative error: " <<
  ((trueModel->Evaluate(testPoint) -
    pce->Evaluate(testPoint)).array().abs() / trueModel->Evaluate(testPoint).array()).transpose() << endl;

  //We even have a cheap Jacobian of the pce, since taking derivatives of polynomials is easy!
  cout << "The pce's Jacobian is: " << endl <<  pce->Jacobian(testPoint) << endl;

  //As an added benefit, we can easily compute the mean and variance of the model, using cheap operations:
  cout << "The approximation has mean: " << pce->ComputeMean().transpose() << " and variance: " <<
  pce->ComputeVariance().transpose() << endl;

  //And the sensitivity indices are also cheap to compute:
  cout << "The main sensitivity indices (output x inputs) are: " << endl << pce->ComputeAllMainSensitivityIndices() << endl; 
  cout << "and the total sensitivity indices (output x inputs) are: " << endl << pce->ComputeAllSobolTotalSensitivityIndices() << endl;
  
  
  // get the multi-indices from the PCE.  Each row of the matrix corresponds to a term in the expansion, while each column corresponds to an input variable.
  Eigen::MatrixXu multis = pce->GetMultiIndices();
  
  // get the coefficients of the PCE.  Each row of the matrix corresponds to an output dimension of the expansion and each column corresponds to a coefficient in the multiindex matrix, i.e. columns of the coefficients matrix correspond to rows of the multiindex.  Notice that this function returns a matrix of unsigned integers.  If needed, the matrix can be cast to a different type with coeffs.cast<int>() or coeffs.cast<double>(), etc...
  Eigen::MatrixXd coeffs = pce->GetCoefficients();
  
  // print the size of the multiindex and coefficients
  cout << "The size of the multi-index matrix is (Number of Terms x Input Dimension):   " << multis.rows() << " x " << multis.cols() << endl;
  cout << "The size of the coefficient matrix is (Output Dimension x Number of Terms):  " << coeffs.rows() << " x " << coeffs.cols() << endl;
}

