#ifndef GENZFUNCTIONS_HPP_
#define GENZFUNCTIONS_HPP_

#include <Eigen/Core>
#include <MUQ/Modelling/ModPieceTemplates.h>

/**
 Virtual class of the Genz functions. Every Genz function is defined by its dimension, and a set of random parameters which characterize it.
 **/
class GenzFunction : public muq::Modelling::OneInputNoDerivModPiece {
  
public:
  
  /**
   \param dim input dimension
   \param w vector of parameters
   \param c vector of parameters
   **/
  GenzFunction(unsigned int           dim,
	             Eigen::VectorXd const& w,
	             Eigen::VectorXd const& c);
  
  virtual ~GenzFunction() = default;
  
protected:
  unsigned int dim;
  Eigen::VectorXd w;
  Eigen::VectorXd c;
};

/**
   Oscillatory Genz function:
   \f[ f({\bf x}) = cos\left( 2 \pi w_1 + \sum_{i=1}^d c_i x_i \right) \f]
 **/
class GenzOscillatory : public GenzFunction {
public:
  
  /**
   \param dim input dimension
   \param w vector of parameters
   \param c vector of parameters
   **/
  GenzOscillatory(unsigned int           dim,
                  Eigen::VectorXd const& w,
                  Eigen::VectorXd const& c);
private:
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const&) override;
};

/**
   Product Peak Genz function:
   \f[ f({\bf x}) = \prod_{i=1}^d \left( c_i^{-2} + (x_i + w_i)^2 \right)^{-1} \f]
 **/
class GenzProductPeak : public GenzFunction {
public:
  
  /**
   \param dim input dimension
   \param w vector of parameters
   \param c vector of parameters
   **/
  GenzProductPeak(unsigned int           dim,
                  Eigen::VectorXd const& w,
                  Eigen::VectorXd const& c);
private:
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const&) override;
};

/**
   Corner Peak Genz function:
   \f[ f({\bf x}) = \left( 1 + \sum_{i=1}^d c_i x_i \right)^{-(d+1)} \f]
 **/
class GenzCornerPeak : public GenzFunction {
public:
  
  /**
   \param dim input dimension
   \param w vector of parameters
   \param c vector of parameters
   **/
  GenzCornerPeak(unsigned int           dim,
                 Eigen::VectorXd const& w,
                 Eigen::VectorXd const& c);
private:
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const&) override;
};

/**
   Gaussian Genz function:
   \f[ f({\bf x}) = \exp \left( - \sum_{i=1}^d c_i^2 (x_i-w_i)^2 \right) \f]
 **/
class GenzGaussian : public GenzFunction {
public:
  
  /**
   \param dim input dimension
   \param w vector of parameters
   \param c vector of parameters
   **/
  GenzGaussian(unsigned int           dim,
               Eigen::VectorXd const& w,
               Eigen::VectorXd const& c);
private:
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const&) override;
};

/**
   Continuous Genz function:
   \f[ f({\bf x}) = \exp \left( - \sum_{i=1}^d c_i^2 \vert x_i-w_i \vert \right) \f]
 **/
class GenzContinuous : public GenzFunction {
public:
  
  /**
   \param dim input dimension
   \param w vector of parameters
   \param c vector of parameters
   **/
  GenzContinuous(unsigned int           dim,
                 Eigen::VectorXd const& w,
                 Eigen::VectorXd const& c);
private:
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const&) override;
};

/**
   Discontinuous Genz function:
   \f[ f({\bf x}) = 
   \begin{cases}
      0 & \text{if any } x_1 > w_1 \;\text{or}\; x_2 > w_2 \\
      \exp \left( \sum_{i=1}^d c_i x_i \right) & \text{otherwise}
   \end{cases} \f]
 **/
class GenzDiscontinuous : public GenzFunction {
public:
  
  /**
   \param dim input dimension
   \param w vector of parameters
   \param c vector of parameters
   **/
  GenzDiscontinuous(unsigned int           dim,
                    Eigen::VectorXd const& w,
                    Eigen::VectorXd const& c);
private:
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const&) override;
};


/**
 Draw random parameters which then define the returned Genz function
 \param ftype standard (0) or modified (1) Genz Functions
 \param fnum (1-6) function number
 \param dim number of dimensions
 \return the required Genz function with random parameters
 **/
std::shared_ptr<muq::Modelling::OneInputNoDerivModPiece>
GetRandomGenzFunction(unsigned int ftype,
                      unsigned int fnum,
                      unsigned int dim );

#endif
