This is the Polynomial Chaos benchmark on the Genz Functions.

Content:
1) References
2) Building
3) Running
4) Post-processing

==============================================================
REFERENCES

The standard Genz functions are defined in
Genz, A. (1984). Testing multidimensional integration routines. Proc. of International Conference on Tools, Methods and Languages for Scientific and Engineering Computation, 81–94.

The modified Genz functions are defined in 
Bigoni, D., Engsig-Karup, A. P., & Marzouk, Y. M. (). Spectral tensor-train decomposition, 28. Numerical Analysis. Retrieved from http://arxiv.org/abs/1405.5713

=============================================================
BUILDING

From this folder enter the following commands

     $ mkdir build
     $ cd build
     $ cmake -DCMAKE_BUILD_TYPE=Release ../
     $ make

=============================================================
RUNNING

     $ ./GenzMain <type> <fnum> <dim> <ntrials> <timescale>

type: standard (0) or modified (1) Genz Functions
fnum: (1-6) function number
dim: dimensions
ntrials: number of trials for each function
timescale: multiplier for the adapting times

This command will run the benchmark and store the output in the file GenzResultsAdaptive.h5

=============================================================
POST-PROCESSING

The post-processing scripts are written in Python and require the libraries: h5py, numpy, matplotlib.
In order to check the content of the file GenzResultsAdaptive.h5, run

   $ python ../H5Content.py GenzResultsAdaptive.h5

In order to obtain the convergence plot for the benchmark, run

   $ python ../ConvergenceGenzPlot.py GenzResultsAdaptive.h5 <type> <fnum> <dim> <timescale>

where the missing parameters are defined accordingly to the section "RUNNING".
