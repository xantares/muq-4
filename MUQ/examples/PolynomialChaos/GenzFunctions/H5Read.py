import numpy as np
import h5py


def read( fname, gtype, fnum, dim, timescale ):
    
    f = h5py.File( fname, 'r' )
    
    if gtype == 0:
        genzType = "Standard"
    elif gtype == 1:
        genzType = "Modified"
    else:
        print "Error: invalid type"
        exit()
    
    if fnum == 1:
        genzName = "Oscillatory"
    elif fnum == 2:
        genzName = "ProductPeak"
    elif fnum == 3:
        genzName = "CornerPeak"
    elif fnum == 4:
        genzName = "Gaussian"
    elif fnum == 5:
        genzName = "Continuous"
    elif fnum == 6:
        genzName = "Discontinuous"
    else:
        print "Error: invlaid fnum"
        exit()

    # Get group in h5 file
    groupName = "/" + genzType + "/" + genzName + "/dim=" + str(dim) + "/tscale=" + str(timescale)
    grp = f[groupName]
    
    # Read results
    fevals = np.array(grp['fevals'])
    LinfErr = np.array(grp['LinfErr'])
    L2Err = np.array(grp['L2Err'])

    f.close()

    return (fevals, LinfErr, L2Err)
