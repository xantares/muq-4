/** \example inference-example-3.cpp
 *  <h3> About this example </h3>
 *  <p> This example defines a Bayesian inference problem instead of a density. We will construct a graph
 *  which links multiple ModPiece instances. The ModPieces are used to define the inference parameters,
 *  the prior, the likelihood and the posterior. When the inference problem is constructed in this manner, 
 *  we can keep track of the forward model evaluations separately. In addition, the Jacobian information 
 *  is tracked and pass through the ModGraph which is used in our sampling algorithm.
 *  </p>
 *  <p>
 *  For this example, the inference problem is the following:
 *  \f{eqnarray*}{
 *  y &=& f(x) + \epsilon\\
 *  f_1(x_1,x_2) &=& x_1 \\
 *  f_2(x_1,x_2) &=& a \left( x_2 - x_1^2 \right) \\
 *  a &=& 5\\
 *  \f}
 *  where we observe
 *  \f[
 *  y = [1,\; 0]^T \;\;\;\; \epsilon \sim N(0,\sigma^2 I) \;\;\;\; \sigma = \sqrt{2}
 *  \f]
 *  with an improper constant prior. With this observation and prior, the posterior becomes
 *  \f[
 *  \pi(x|y) \propto \exp{\left(-(1-x_1)^2 - a^2(x_2 - x_1^2)^2\right)}
 *  \f]
 *
 *  We will be using pre-conditioned MALA to sample from the posterior. This example also introduces HDF5 
 *  logging which is an alternative method to report the final results.
 *  </p>
 *
 *  <h3> Line by line explanation </h3>
 *  See example-3.cpp for finely documented code.
 */

#include <iostream>
#include <Eigen/Core>
#include <boost/property_tree/ptree.hpp>

#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/DensityProduct.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/ModPieceTemplates.h"
#include "MUQ/Modelling/GaussianDensity.h"
#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/UniformSpecification.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"

#include "MUQ/Inference/MCMC/MCMCBase.h"
#include "MUQ/Inference/MCMC/MCMCProposal.h"
#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"

#include "MUQ/Utilities/HDF5Wrapper.h"

using namespace std;
using namespace Eigen;

using namespace muq::Modelling;
using namespace muq::Inference;
using namespace muq::Utilities;

// Unlike the previous example, we will NOT define a ModPiece for the density. Instead, we will 
// construct a ModPiece for the forward model of our inference problem. One helpful template is the
// OneInputJacobianModPiece which requires an implementation of the forward model and its Jacobian.
// Other useful templates for ModPieces can be found in "ModPieceTemplates.h"

class Rosenbrock : public OneInputJacobianModPiece {
public:
  Rosenbrock() : OneInputJacobianModPiece(2,    // input size
					  2) {} // output size
  virtual ~Rosenbrock() = default;

private:
  // implement the forward model evaluation using Eigen vectors
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    Eigen::VectorXd output(2);
    output(0) = input(0);
    output(1) = a*( input(1)-input(0)*input(0) );
    return output;
  }
  // implement the Jacobian as an Eigen matrix
  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) override
  {
    Eigen::MatrixXd output(2,2);
    output(0,0) = 1;
    output(0,1) = 0;
    output(1,0) = -2*a*input(0);
    output(1,1) = a*input(1);
    return output;
  }

  // constant in the Rosenbrock forward model
  double a = 4;
};

int main()
{
  // define some useful constants and vectors
  int paramDim = 2;                 // stochastic dimension
  int sigmaSq = 2;                  // variance of observational error 
  Eigen::VectorXd data(paramDim);   // observed data vector
  data << 1, 0;

  // The first step is to create all the ModPieces required for an inference problem and link them
  // together in a ModGraph. The first ModPiece at the top of the graph are the parameters of the
  // problem. This is created as a VectorPassthroughModel which is a ModPiece that 'passes through'
  // its input as output.
  auto inferenceTarget = make_shared<VectorPassthroughModel>(paramDim);
  // The prior is defined as a uniform density ranging from -1000 to 1000 for both parameters. It
  // is created as a UniformDensity which is a Density (non-negative ModPiece).
  auto prior = make_shared<UniformDensity>(make_shared<UniformBox>(VectorXd::Constant(2,-1000),
                                                                   VectorXd::Constant(2, 1000)));
  // The forward model is created from the custom ModPiece class we created above.
  auto forwardModel = make_shared<Rosenbrock>();
  // The likelihood is a multi-variate normal density that takes the output of the forward model 
  // and compares it with its mean (the data).
  auto likelihood = make_shared<GaussianDensity>(data,     // mean
					                             sigmaSq); // variance
  // Finally the posterior is the product of the likelihood and the prior.
  auto posterior = make_shared<DensityProduct>(2);

  // The second step is to name and link all the ModPieces. A valid ModGraph for inference must 
  // have the nodes named "forwardModel", "prior", and "likelihood".
  auto graph = std::make_shared<muq::Modelling::ModGraph>();
  graph->AddNode(inferenceTarget, "inferenceTarget");
  graph->AddNode(prior, "prior");
  graph->AddNode(forwardModel, "forwardModel");
  graph->AddNode(likelihood, "likelihood");
  graph->AddNode(posterior, "posterior");

  // The nodes of the graph are linked based upon name. The syntax to add an edge is 
  // graph->AddEdge("node1","node2",i). When evaluating the graph, the output of "node1" is fed
  // to the (i)th input of "node2". The input index for all ModPieces start at 0.
  graph->AddEdge("inferenceTarget", "forwardModel",0);
  graph->AddEdge("forwardModel", "likelihood",0);
  graph->AddEdge("inferenceTarget", "prior",0);
  graph->AddEdge("likelihood", "posterior",0);
  graph->AddEdge("prior", "posterior",1);

  // write the structure of the graph to a specified PDF file
  graph->writeGraphViz("InferenceModGraph.pdf");

  // define an inference problem using the information in the graph
  auto problem = make_shared<InferenceProblem>(graph);

  // define the properties and tuning parameters for preconditioned MALA
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Proposal", "PreMALA");
  params.put("MCMC.Steps", 1000);
  params.put("MCMC.BurnIn", 5);
  params.put("MCMC.MH.PropSize", 1);
  params.put("MCMC.PreMALA.StepLength", 1);
  params.put("MCMC.PreMALA.AdaptDelay", 5);
  params.put("HDF5.LogWriteFrequency", 1);
  params.put("Verbose", 2);
  params.put("MCMC.HDF5OutputGroup", "/PreMALA/");
  params.put("MCMC.HDF5OutputDetails", true);

  // Three extra parameters are included in order to use HDF5 logging. The following commands
  // initialize HDF5 logging to write to a specified file.
  HDF5Logging::Configure(params);
  HDF5Wrapper::OpenFile("mcmcLogging.h5");

  // construct a MCMC sampling task from the parameters and the inference problem.
  auto mcmc = MCMCBase::ConstructMCMC(problem, params);
  Eigen::VectorXd startingPoint(2);
  startingPoint << 0.5, 0.5;
  // sample from a given starting point. This procedure will also write to the HDF5 log file.
  EmpRvPtr mcmcChain = mcmc->Sample(startingPoint);
  // close HDF5 logging file
  HDF5Logging::WipeBuffers();
  HDF5Wrapper::CloseFile();
  
  // compute and display the mean, covariance and ess from the samples
  Eigen::VectorXd mean = mcmcChain->getMean();
  Eigen::MatrixXd cov  = mcmcChain->getCov();
  cout << endl << "Sample Mean:\n";
  cout << mean.transpose() << endl;
  cout << endl << "Sample Covariance:\n";
  cout << cov << endl << endl;

  Eigen::VectorXd ess = mcmcChain->getEss();
  cout << "Minimum ESS: " << ess.minCoeff() << endl;
  cout << "Maximum ESS: " << ess.maxCoeff() << endl << endl;

  // write out the chain in raw text
  mcmcChain->WriteRawText("mcmcChain.txt");
  cout << "Raw samples are written in mcmcChain.txt." << endl;

  // The HDF5 file contains datasets of the chain, forward model evaluations, acceptance,
  // log-posterior and step length (for MALA). The data of an HDF5 file can be extracted
  // using various ways. For example, we can use the following MATLAB command: 
  //      h5read('mcmcLogging.h5','/PreMALA/chain')
  cout << "Logged datasets written in mcmcLogging.h5:" << endl;
  cout << "  /PreMALA/forwardModel" << endl;
  cout << "  /PreMALA/chain" << endl;
  cout << "  /PreMALA/acceptance" << endl;
  cout << "  /PreMALA/logDensity" << endl;
  cout << "  /PreMALA/stepLength" << endl;
}

