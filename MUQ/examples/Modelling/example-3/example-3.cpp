/** \example modelling-example-3.cpp
 *  <h3> About this example </h3>
 *  <p>The first two examples developed approaches for evaluating the right hand side of the ODE:
 *  \f{eqnarray*}{
 *  \frac{dP}{dt} & = & rP\left(1-\frac{P}{K}\right) - s\frac{PQ}{a+P}\\
 *  \frac{dQ}{dt} & = & u\frac{PQ}{a+P} - vQ
 *  \f}
 *  This file uses the optional SUNDIALS wrapper in MUQ to model the entire time dependent system,
 *  not simply the right hand side.  Note that to compile this example, the SUNDIALS wrapper must
 *  have been compiled with MUQ.  As with all optional dependencies, SUNDIALS can be used by
 *  defining adding "-DMUQ_USE_SUNDIALS" to the cmake call.  </p>
 *
 *  <h3> Compiling example-3.cpp and linking to MUQ </h3>
 *  Same as example-1.cpp and example-2.cpp as long as SUNDIALS was used in MUQ compilation.
 */

// MUQ uses Eigen for defining all vectors and matrices.  This line includes the core components of Eigen that will be
// needed for defining our model in MUQ.
#include <Eigen/Core>

// At the end of this program, we will report a model evaluation so we will need to access std::cout
#include <iostream>
#include <iomanip>

#include <MUQ/Modelling/ModPiece.h>
#include <MUQ/Modelling/VectorPassThroughModel.h>
#include <MUQ/Modelling/OdeModPiece.h>

#include <boost/property_tree/ptree.hpp>

// use the muq::Modelling namespace
using namespace muq::Modelling;

// use the std namespace for cout and endl
using namespace std;

/** This class defines the RHS of the ODE predator-prey model.  An ODE model in MUQ is constructed by creating a ModPiece to evaluate
 * the RHS of the system, and then creating a muq::Modelling::OdeModPiece for solving the ODE.  The right hand side Modpiece should take
 * at least two inputs: (1) The current value of the independent ODE variable (i.e. the current time) and (2) The current state of the system.
 * Additional parameters can also be used in the right hand side ModPiece.  For example, in this predator-prey system, the various growth and
 * death rates are passed as a third input to the rhs.
 */
class PredPreyModel : public ModPiece {
public:

  PredPreyModel() : ModPiece(Eigen::Vector3i{1, 2, 6}, // input sizes (this examples uses the c++11 ability to construct vectors with a curly bracketed array like this.
                             2,        // the output dimension
                             false,    // does this class provide an efficient implementation of the GradientImpl function?
                             false,    // does this class provide an efficient implementation of the JacobianImpl function?
                             false,    // does this class provide an efficient implementation of the JacobianActionImpl function?
                             false,    // does this class provide an efficient implementation of the HessianImpl function?
                             false) {} // does this class return a random result?

  virtual ~PredPreyModel() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override
  {
    // grad the current time in the simulation
    double time = inputs.at(0) (0);

    // for clarity, grab the prey and predator populations from the input vector
    double preyPop = inputs.at(1) (0);
    double predPop = inputs.at(1) (1);

    // extract the model parameters from the input vector.  Note this requires an extra copy of the model parameters,
    // but results in more readable code
    double preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss, predatorGrowth;

    preyGrowth      = inputs.at(2) (0);
    preyCapacity    = inputs.at(2) (1);
    predationRate   = inputs.at(2) (2);
    predationHunger = inputs.at(2) (3);
    predatorLoss    = inputs.at(2) (4);
    predatorGrowth  = inputs.at(2) (5);

    // create a vector to hold the ModPiece output, this will hold the time derivatives of population
    Eigen::VectorXd output(2);

    // the first output of this function is the derivative of the predator population size with resepct to time
    output(0) = preyPop * (preyGrowth * (1 - preyPop / preyCapacity) - predationRate * predPop / (predationHunger + preyPop));
    output(1) = predPop * (predatorGrowth * preyPop / (predationHunger + preyPop) - predatorLoss);

    // return the output
    return output;
  }
};


int main()
{
  // create an instance of the modpiece defining the right hand side of the predator prey model
  auto predatorPreyRhs = make_shared<PredPreyModel>();

  /** create a parameter list to hold the ODE solver parameters.  Many of the more advanced numerical
   *  methods in MUQ use boost property trees to store parameters.  The ptree makes it easy for developers
   *  to specify default values, while also making it easy to override the defaults with user input.
   *  The parameters to the ODE solver include tolerances as well as which nonlinear and linear solvers
   *  should be used within the ODE integration.  In this example, a Newton method is used as the 
   *  nonlinear method and a BDF time integration scheme is used.  The default behavior for the linear
   *  solve in each Newton step is to call the RHS Jacobian function and use a dense linear solver.
   *  However, SUNDIALS (and the MUQ wrapper), can also employ an iterative linear solver that only
   *  uses calls to the RHS JacobianAction function.  Even the Newton iteration can be exchanged for 
   *  a fixed point iteration solver by changing "Newton" and "Iter".
   */
  boost::property_tree::ptree odeParams;

  odeParams.put("CVODES.ATOL", 1e-13);           // absolute tolerance
  odeParams.get("CVODES.RTOL", 1e-13);           // relative tolerance
  odeParams.put("CVODES.Method", "BDF");         // either BDF or Adams
  odeParams.put("CVODES.LinearSolver", "Dense"); // either Dense, SPGMR, SPBCG, or SPTFQMR
  odeParams.put("CVODES.SolveMethod", "Newton"); // either Newton or Iter
  odeParams.put("CVODES.MaxStepSize", 8e-4);     // maximum step size, should be small for accurate discrete adjoint

  /** The ODE solver needs to know when to stop integrating the system and while integrating, when to store the state.  
   *  All of this information is contained in the obsTimes vector.  This vector is an increasing list of times when the
   *  state should be stored.  The solver assumes the integration starts at 0 and goes until the last time in this vector.
   */
  Eigen::VectorXd obsTimes;
  obsTimes.setLinSpaced(50, 0.0, 300.0);

  // create the ODE model from the right hand side definition, the observation times, and the solver parameters
  auto predatorPreyModel = make_shared<OdeModPiece>(predatorPreyRhs, obsTimes, odeParams);

  /** Just like before, we need to create a vector of the input parameters.  This vector will contain the initial conditions
   *  of the state as in the first input, and the parameter values as the second.  Notice that even though the RHS modpiece
   *  takes time as the first input, the OdeModPiece has no time input.
   */
  vector<Eigen::VectorXd> modelParams(2);

  // set the initial population sizes
  modelParams.at(0).resize(2);
  modelParams.at(0) << 50, 5; // prey population, predator population

  // set the interaction parameters
  modelParams.at(1).resize(6);
  modelParams.at(1) << 0.8, 100, 4.0, 15, 0.6, 0.8; //preyGrowth, preyCapacity, predationRate, predationHunger, predatorLoss, predatorGrowth;

  /** Here we evaluate the full ODE system stored in the predatorPreyModel ModPiece.  Remember that the model is stored in a smart pointer.
   *  the result is a vector containing the states extracted at each time in the obsTimes vector.  The output from this call to the OdeModPiece
   *  is a vector containing each state variable at each time.  The states are stored contiguously, meaning that in this example populations(0)
   *  is the prey population at obsTimes(0), and populations(1) is the predator population at obsTimes(0).  Similarly, populations(2) is the 
   *  prey population at obsTimes(1) and populations(3) is the predator population at obsTimes(1).  An Eigen::Map could be used to unroll these
   *  quantities if desired.
   */
  Eigen::VectorXd populations = predatorPreyModel->Evaluate(modelParams);

  // print the prey population and predator population
  cout << "\nPrey Population  |  Predator Population\n";
  for (int i = 0; i < populations.size(); i += 2) {
    std::cout << setw(10) << setprecision(2) << fixed << populations(i) << "                " <<
    populations(i + 1) << endl;
  }
  cout << endl;
}

