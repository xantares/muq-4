#
# NAME
#   CreateExamplePage.py
#
# SYNOPSIS
#   This script converts an ipython notebook into a PHP webpage for the MUQ website
#
# DESCRIPTION
#   This script takes two input arguments: the ipynb input file and the PHP output file.
#   Calling this script will involve a command like:
#
#   CreateExamplePage.py -i <inputfile> -o <outputfile>
#

import os
import re
import json
from IPython.nbconvert.filters.markdown import markdown2html
import cgi
 
import sys, getopt

def PrintUsage():
    print '  CreateExamplePage.py -i <inputfile> -o <outputfile>\n'
    sys.exit(2)

inputfile = ''
outputfile = ''
   
try:
    opts, args = getopt.getopt(sys.argv[1:],"hi:o:",["ifile=","ofile="])
except getopt.GetoptError:
    PrintError()
    PrintUsage()
    
for opt, arg in opts:
    if opt == '-h':
	    PrintUsage()
    elif opt in ("-i", "--ifile"):
	    inputfile = os.path.abspath(arg)
    elif opt in ("-o", "--ofile"):
	    outputfile = os.path.abspath(arg)
	        
if(len(opts)!=2):
    print '\nInvalid number of options.  Correct usage is:'
    PrintUsage()
    
print '\nReading notebook from:\n    %s'%inputfile
print 'Writing php webpage to:\n    %s\n'%outputfile

isCpp = inputfile.split('/')[-1].lower().find('cpp')>0
    
# create a string to hold the html that we can include in the MUQ website
htmlStr = ''
allCode = ''

# loop through all the cells in the ipynb file
contents = json.load(file(inputfile))
for cell in contents['cells']:

	# CODE CELLS
	if(cell['cell_type']=='code'):
		cellLines = cell['source']
		
		# check to make sure this cell isn't empty
		if(len(cellLines)>0):
			
			# look for magic lines at the beginning of the cell
			skipOut = False
			skipCode = False
			if(cellLines[0][0]=='%'):
		
			    # if this cell is writing to a file, we will skip the output
			    if(len(cellLines[0])>10):
			        if(cellLines[0][0:11]=='%%writefile'):
			            skipOut = True
			            
			    if(len(cellLines[0])>5):
			        if(cellLines[0][0:6]=='%%bash'):
			            skipCode = True
			            
			    # remove the magic line
			    cellLines = cellLines[1::]
				
			
			htmlStr += '<pre class="prettyprint">\n'
			for line in cellLines:
			    safeStr = cgi.escape(line)
			    if(not skipCode):
			        allCode += safeStr
			    htmlStr += safeStr
			htmlStr += '\n</pre>\n\n'
			
			if(not skipCode):
			    allCode += '\n\n'
			
			if(not skipOut):
				for outCell in cell['outputs']:
					outLines = outCell['text']
					if(len(outLines)>0):
						htmlStr += '<pre class="prettyprint lang-bash" style="background-color:#D0D0D0">\n'
						for line in outLines:
							htmlStr += cgi.escape(line)
						htmlStr += '\n</pre>\n\n'
		
	# MARKDOWN CELLS      
	elif(cell['cell_type']=='markdown'):
		cellLines = cell['source']
		
		# check to make sure this cell isn't empty
		if(len(cellLines)>0):
			allLines = ''
			for line in cellLines:
				allLines += line
			htmlStr += re.sub('<a class="anchor-link" href="[^<]+</a>','',markdown2html(allLines))
	
	else:
		raise ValueError("ERROR: Invalid cell type, we can currently only handle 'code' and 'markdown'.")

# add the entire code to the html string
htmlStr += '<h2>Completed code:</h2>'
htmlStr += '<pre class="prettyprint" style="height:auto;max-height:400px;">\n'
htmlStr += allCode
htmlStr += '\n</pre>\n\n'

# now that we've converted the notebook to html, we need to create the webpage
pageName = inputfile.split('/')[-1].split('.')[0]
pageStr = "<?php\n$tpl['page_name'] = '" + pageName + "';\n$tpl['tab'] = 'Examples';\n?>\n"
pageStr += htmlStr

# create any necessary directories, open up a file, and write the webpage
fullDir = '/'.join(outputfile.split('/')[0:-1])

if(not os.path.exists(fullDir)):
	os.makedirs(fullDir)

with open(outputfile,'w') as fout:
	fout.write(pageStr)  